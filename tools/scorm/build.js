/**
 * Create SCORM-packages for selected Apps
 * @author Thomas Kippenberg
 */

import { sitemap } from '../../app/sitemap.js'
import fse from 'fs-extra'
import path from 'path'
import pkg from '../../package.json' with { type: 'json' }
import JSZip from 'jszip'

// Create output folder if necessary
const myDir = './tools/scorm' // location of this script
const downloadDir = './app/assets/download' // location for download files
const appDir = './app' // location of app dir
const weblateDir = './app/assets/weblate' // location of weblate repository

// Start
fse.ensureDirSync(downloadDir)
main()

/**
 * main function
 */
async function main() {
  // identify sites to build and generate list with all icons
  const sites = []
  const icons = []
  for (const topic of sitemap.topics) {
    if (!topic.subtopics) topic.subtopics = []
    for (const suptopic of topic.subtopics) {
      if (!suptopic.sites) subtopic.sites = []
      for (const site of suptopic.sites) {
        if (site.icon) icons.push(site.icon)
        if (site.download && site.id) sites.push(site)
      }
    }
  }
  // loop all sites and build SCORM if outdated or missing
  for (const site of sites) {
    console.log(`Building: ${site.id}`)
    // Filname of SCORM package
    const scormPackageFile = path.join(downloadDir, site.id + '.zip')
    // Filename of html site
    const htmlFile = path.join(appDir, 'html', site.id + '.html')
    // Create empty ZIP archive
    const zip = new JSZip()
    // add sitemap
    zip.file('sitemap.js', fse.readFileSync(path.join(appDir, 'sitemap.js')))
    // add website.js
    zip.file('website.js', fse.readFileSync(path.join(myDir, 'website.js')))
    // copy config file
    zip.file('config.js', fse.readFileSync(path.join(myDir, 'config.js')))
    // add app file
    const folderHtml = zip.folder('html')
    folderHtml.file('index.html', fse.readFileSync(htmlFile))
    // add components
    const folderComponents = zip.folder('components')
    fse.readdirSync(path.join(appDir, 'components')).forEach(file => {
      folderComponents.file(file, fse.readFileSync(path.join(appDir, 'components', file)))
    })
    // add modules
    const folderModules = zip.folder('modules')
    fse.readdirSync(path.join(appDir, 'modules')).forEach(file => {
      folderModules.file(file, fse.readFileSync(path.join(appDir, 'modules', file)))
    })
    // add styles
    const folderAssets = zip.folder('assets')
    const folderStyles = folderAssets.folder('styles')
    fse.readdirSync(path.join(appDir, 'assets', 'styles')).forEach(file => {
      folderStyles.file(file, fse.readFileSync(path.join(appDir, 'assets', 'styles', file)))
    })
    // add required icons
    const folderIcons = folderAssets.folder('icons')
    fse.readdirSync(path.join(appDir, 'assets', 'icons')).forEach(file => {
      const icon = file.substring(file.lastIndexOf('/') + 1)
      if (icon.endsWith('.svg') && !icons.indexOf(icon) >= 0) {
        folderIcons.file(file, fse.readFileSync(path.join(appDir, 'assets', 'icons', file)))
      }
    })
    // add further required assets (to avoid errors)required sound index
    const folderSounds = folderAssets.folder('sounds')
    folderSounds.file('index.js', fse.readFileSync(path.join(appDir, 'assets', 'sounds', 'index.js')))

    // add required libs
    const html = fse.readFileSync(path.join('./app/html/', site.id + '.html'), 'utf8')
    const threejs = html.indexOf('from \'three\'') > 0
    const folderLibs = folderAssets.folder('libs')
    fse.readdirSync(path.join(appDir, 'assets', 'libs')).forEach(file => {
      if (file.endsWith('.js') && (!file.startsWith('three') || threejs)) {
        folderLibs.file(file, fse.readFileSync(path.join(appDir, 'assets', 'libs', file)))
      }
    })
    // add required language files
    const folderWeblate = folderAssets.folder('weblate')
    fse.readdirSync(weblateDir).forEach(file => {
      if (file.indexOf('main-') >= 0) {
        folderWeblate.file(file, fse.readFileSync(path.join(weblateDir, file)))
      }
    })
    // add schemas
    fse.readdirSync(path.join(myDir, 'SCORM-schemas')).forEach(file => {
      zip.file(file, fse.readFileSync(path.join(myDir, 'SCORM-schemas', file)))
    })
    // Create manifest
    zip.file('imsmanifest.xml', createScormManifest(site))
    // write to disk
    const buffer = await zip.generateAsync({ type: 'nodebuffer' })
    fse.writeFileSync(scormPackageFile, buffer);
  }
}

/**
 * Build SCORM specific manifest
 * @param {String} site - id of site
 * @returns {String} text
 */
function createScormManifest(site) {
  const id = `org.mintapps.${site.id}.scorm.12`
  const title = site.title.split('.')[0]
  const organization = 'mintapps_org'
  const manifest =
  `<?xml version="1.0" standalone="no" ?>
  <manifest 
      identifier="${id}"
      version="1"
      xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2"
      xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd
        http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd
        http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd"
  >
     <metadata>
      <schema>ADL SCORM</schema>
      <schemaversion>1.2</schemaversion>
    </metadata>
    <organizations default="${organization}">
      <organization identifier="${organization}">
        <title>${title}</title>
        <item identifier="item_1" identifierref="resource_1">
          <title>${title}</title>
       </item>
     </organization>
    </organizations>
    <resources>
      <resource identifier="resource_1" type="webcontent" adlcp:scormtype="sco" href="html/index.html">
        <file href="html/index.html" />
      </resource>
    </resources>
  </manifest>`
  return manifest
}