export default {
  seoOptimization: false,
  website: false,

  // locale settings
  defaultLocale: 'de',
  fallbackLocale: 'en',

  // theme settings
  defaultTheme: 'light',

  // appeareance settings
  columnLimit: 2,

  // pathes, must end with trailing slash
  dataPath: '../assets/data/',
  weblatePath: '../assets/weblate/'
}
