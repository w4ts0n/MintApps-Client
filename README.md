# MintApps

## About

MintApps are a collection of various HTML5 apps to support digital learning and teaching in technical and scientific subjects.

![MintApps Logo](app/assets/icons/mintapps-logo-large.svg)

## What makes MintApps different from other apps?

- **Independence from the platform**: An up-to-date browser is sufficient, no installation is required, all common operating systems are supported.
- **Responsive layout**: The display automatically adapts to the dimensions of the device so that smartphones and tablets can also be used.
- **Data saving**: No tracking mechanisms are integrated and no data is passed to third parties.
- **Open source**: The source code is available on [codeberg.org](https://codeberg.org/MintApps/client) under the GPL 3.0 license.

## First steps

- **Online version**: You can use the current version of MintApps directly on the [mintapps.org](https://mintapps.org) website.
- **Own instance**: If you want to run your own instance of MintApps, you will find instructions in the [INSTALL.md](https://codeberg.org/MintApps/doc/src/branch/main/client/INSTALL.md) document.
- **Social media**: Don't miss out the latest simulations and tools and follow the project on [Mastodon](https://bildung.social/@mintapps).
- **Documentation**: You can find the source code documentation directly in MintApps in the “[Lab](https://mintapps.org/html/mint-index-lab.html)” section.

## Project team

- [Thomas Kippenberg](https://codeberg.org/tk100): development, app design
- [Christian Schiller](https://codeberg.org/schilchris): technical tests, app design
- [Lukas Schieren](https://lukas-schieren.de/): management of the Weblate repository
- [Michael Speckert](https://www.speckerts.de/): app design
- [Hans-Georg Unckell](https://codeberg.org/hgu): tests
- [Peter Weinzierl](https://weinzierl-it.de): development

## Contribute to the project

MintApps is a volunteer project. We invite you to join us and become part of the team!

- **Report a bug:** First make sure that the bug has not already been documented as an [issue](https://codeberg.org/MintApps/client/issues). If this is not the case, please create a new issue. Make sure to describe the bug clearly and comprehensibly, ideally with screenshot(s).
- **Contribute ideas:** If you have a new idea in the area of STEM subjects, please open a new issue in which describe exactly what the new app or the extension of an existing app should look like.
- **Translate:** Do you speak other languages besides English and German? We would be very happy to support you with [translating](https://translate.codeberg.org/projects/mintapps/).
- **Development:** Are you brave and want to create a new app yourself? This is relatively easy, as you don't have to learn any complex frameworks and there are numerous examples available. Just have a look at our [template-app](https://mintapps.org/html/mint-template.html) or get in touch with us!

## Many thanks to…

- [John Bartmann](https://johnbartmann.com), who makes his great music available under a CC license
- [Matthias Borchard](http://www.mabo-physik.de) for his professional advice and the exciting technical discussions
- [Donald Chan](https://github.com/Donaldcwl) for the library [Browser Image Compression](https://github.com/Donaldcwl/browser-image-compression) to compress images under the [MIT license](https://github.com/Donaldcwl/browser-image-compression/blob/master/LICENSE)
- [Codeberg e. V.](https://codeberg.org/) for its free alternative to manage git repositories
- [Cure 53](https://cure53.de/) for the library [DOMPurify](https://github.com/cure53/DOMPurify) to sanitize HTML under the [Apache License](https://github.com/cure53/DOMPurify/blob/main/LICENSE)
- the [Nimiq Foundation](https://github.com/nimiq) for the library [QR Code Encoder](https://github.com/nimiq/qr-creator) to create QR codes under the [MIT License](https://github.com/nimiq/qr-creator/blob/master/LICENSE)
- [Stefan Richtberg](https://www.richtberg.org/) for his technical feedback and the publication of some MintApps on [LEIFI-Physik](https://www.leifiphysik.de/)
- [Lina Staufer](https://codeberg.org/fabu) for the design of the MintApps logos
- the [STIX Fonts project](https://www.stixfonts.org/) for the free font [STIXTwoMath-Regular](https://github.com/stipub/stixfonts) to typeset mathematical formulas under the [SIL Open Font License](https://github.com/stipub/stixfonts/blob/master/OFL.txt)
- the [three.js project](https://threejs.org/) for the library [three.js](https://github.com/mrdoob/three.js) for 3D representation in the browser
- [Cosmo Wolfe](https://github.com/cozmo) for the library [jsQR](https://github.com/cozmo/jsQR) to read QR codes under the [Apache License](https://github.com/cozmo/jsQR/blob/master/LICENSE)
- and of course to all translators on Weblate, especially [mondstern](https://codeberg.org/mondstern), [SomeTr](https://translate.codeberg.org/user/SomeTr/) and [xpeng](https://xpengs.com)

## License of MintApps

MintApps is licensed under the “[GNU General Public License 3](https://www.gnu.org/licenses/gpl-3.0.html)”, please note that this excludes any liability or warranty.

**Disclaimer of Warranty.**

There is no warranty for the program, to the extent permitted by applicable law. Except when otherwise stated in writing the copyright holders and/or other parties provide the program “as is” without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with you. Should the program prove defective, you assume the cost of all necessary servicing, repair or correction.

**Limitation of Liability**

In no event unless required by applicable law or agreed to in writing will any copyright holder, or any other party who modifies and/or conveys the program as permitted above, be liable to you for damages, including any general, special, incidental or consequential damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by you or third parties or a failure of the program to operate with any other programs), even if such holder or other party has been advised of the possibility of such damages.