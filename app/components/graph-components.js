import * as plot2d from '../modules/plot-2d.js'
import * as color from '../modules/color.js'
import { translateChildren, translateElement } from '../modules/i18n.js'
import { renderChildren } from '../modules/asciimathml.js'
import { getBrowserName } from '../modules/browser.js'
import { enableEnterToClick } from '../modules/accessibility.js'
import './modal-dialog.js'
import { HTMLMintElement } from './abstract-components.js'
import { uid, hide, importCss, getMintAppsContainer } from '../modules/utils.js'
import './form-components.js'
import './alert-box.js'
importCss('graph-components.css')

/**
 * Web component: Display the coordinates in a small overlay when the user clicks on a point in the coordinate system
 */
class GraphPoint extends HTMLElement {
  span = null // reference to span element

  connectedCallback() {
    if (this.disconnected) return
    window.addEventListener('keydown', (e) => this.keyListener(e))
  }

  /** 
   * Open overlay
   * @param {Object} p - parameter object with pixel and relative coordinates
   */
  set point(p) {
    const fs = plot2d.getFontSizeText()
    if (this.span) this.removeChild(this.span)
    const textX = isNaN(p.x) ? '–' : plot2d.optimizeNumber(p.x, 3)
    const textY = isNaN(p.y) ? '–' : plot2d.optimizeNumber(p.y, 3)
    this.span = document.createElement('span')
    this.span.classList.add('graph-point', 'cursor-pointer')
    this.span.style.top = `${p.pixelY + 0.5 * fs}px`
    this.span.style.left = `${p.pixelX - 3 * fs}px`
    this.span.onclick = () => this.emitClose()
    this.span.innerText = `${textX} | ${textY}`
    this.append(this.span)
  }

  /**
   * Close overlay
   */
  emitClose() {
    if (this.span) this.removeChild(this.span)
    this.span = null
    this.dispatchEvent(new Event('close'))
  }

  /**
   * Listen for keyboard event and close on ESC
   * @param {Event} e - keyboard event
   */
  keyListener(e) {
    if (e.keyCode === 27) this.emitClose()
  }
}

/**
 * Legend in the graph that can be moved freely
 * @param {Boolean} [data-left] - position legend at the left side (right is default)
 */
class GraphLegend extends HTMLMintElement {
  lineStylesCss = ['', 'solid', 'dashed', 'dotted'] // predefined line styles
  position = { x: 0, y: 0 } // position of legend overlay
  dragging = false //  indicates whether the legend is currently being moved

  connectedCallback() {
    if (this.disconnected) return
    const side = this.hasAttribute('data-left') ? 'left' : 'right'
    this.style = `
      position: absolute;
      ${side}: 0;
      background-color: var(--background-color);
      padding: 5px;
      border: var(--dividor-color) 1px solid;
      z-index: 1000;
      cursor: grab;
    `
    this.onmousedown = (e) => this.mouseDown(e)
    this.onmousemove = (e) => this.mouseMove(e)
    this.onmouseup = (e) => this.mouseUp(e)
    this.onmouseleave = (e) => this.mouseUp(e)
    this.ontouchstart = (e) => this.mouseDown(e)
    this.ontouchmove = (e) => this.mouseMove(e)
    this.ontouchend = (e) => this.mouseUp(e)
    this.render()
  }

  /**
   * Render legend table, inlcluding translation and asciimath
   * @param {Array} legend - legend definition
   */
  render(legend) {
    this.innerHTML = ''
    if (!legend || legend.length === 0) {
      this.hide()
      return
    }
    this.show()
    const table = document.createElement('table')
    table.style = 'vertical-align: middle;'
    legend.forEach(entry => {
      const tr = document.createElement('tr')
      tr.style = `color: ${this.getColor(entry.color)}`
      // line or cross
      let td = document.createElement('td')
      let span = document.createElement('span')
      if (entry.lineWidth) { // line
        span.style = `
          vertical-align:middle;
          border-top-width: ${entry.lineWidth}px;
          border-top-style: ${this.lineStylesCss[entry.lineStyle]}; 
          border-top-color: ${this.getColor(entry.color)}; 
          width: 1rem;
          display:inline-block;
          height: 3px;
        `
        span.innerText = ' '
      } else { // cross
        span.style = `color:${this.getColor(entry.color)}; padding-left: 0.1rem;`
        span.innerText = '⨉'
      }
        td.append(span)
      tr.append(td)
      // text
      td = document.createElement('td')
      if (entry.text) {
        span = document.createElement('span')
        span.setAttribute('data-i18n-md-inline', entry.text)
        td.append(span)
      }
      if (entry.text && entry.math) {
        span.style.marginRight = '0.5em'
      }
      if (entry.math) {
        span = document.createElement('span')
        span.setAttribute('data-am-inline', entry.math)
        td.append(span)
      }
      tr.append(td)
      table.append(tr)
    })
    this.append(table)
    translateChildren(this)
    renderChildren(this)
  }

  /**
   * Resovle color
   * @param {String|Number} hex color code or color index
   * @returns {String} hex encoded color
   */
  getColor(col) {
    const colors = color.getCssColors()
    if (typeof col === 'string' && col.startsWith('#')) return col
    else if (isNaN(col)) return 'var(--primary-text-color)'
    else return colors[col]
  }

  /**
   * Start drag event
   * @param {Event} event 
   */
  mouseDown(event) {
    event.preventDefault()
    this.position = plot2d.getAbsoluteEventPosition(event, document.body)
    this.dragging = true
  }

  /**
   * React to drag
   * @param {Event} event 
   */
  mouseMove(event) {
    event.preventDefault()
    if (!this.dragging) return
    const newPosition = plot2d.getAbsoluteEventPosition(event, document.body)
    const movementX = this.position.x - newPosition.x
    const movementY = this.position.y - newPosition.y
    this.position = newPosition
    this.style.top = this.offsetTop - movementY + 'px'
    this.style.left = this.offsetLeft - movementX + 'px'
    this.style.right = 'auto'
  }

  /**
   * End drag event
   * @param {Event} event 
   */
  mouseUp(event) {
    event.preventDefault()
    this.dragging = false
  }

  /**
   * Set legend object
   */
  set legend(l) {
    this.render(l)
  }
}

/**
 * Web component: Optional dialog with graph settings for coordinate system
 * @property {Boolean} [data-fixed-border=false] hide options to alter border
 */
class GraphSettings extends HTMLMintElement {
  #gridx = null // reference to x.grid input
  #gridy = null // reference to y.grid input
  #border = null // reference to border select
  #legend = null // reference to show legend switch
  #alert = null // reference to alert box
  #coords = {} // local coords object
  #coordsBackup = '' // backup of local coords object
  #canvas = null // canvas object
  #modalDownloadFile = null // reference to modal-download-file component
  #modalSettings = null // reference to modal-dialog with settings

  connectedCallback() {
    if (this.disconnected) return
    // icon
    const img = document.createElement('img')
    img.tabIndex = 0
    img.src = `../assets/icons/menu.svg`
    img.setAttribute('data-i18n-alt', 'General.settings')
    img.setAttribute('data-i18n-title', 'General.settings')
    img.classList.add('graph-settings', 'hide-print', 'invert-dark', 'cursor-pointer', 'enter-click')
    img.onclick = () => this.openSettings()
    this.append(img)
    enableEnterToClick(this)
  }

  /**
   * Render modal dialog with settings
   */
  renderModalSettingsDialog() {
    // settings dialog
    this.#modalSettings = document.createElement('modal-dialog')
    getMintAppsContainer().append(this.#modalSettings)
    this.#modalSettings.footer.removeAttribute('data-hide')
    // set title
    this.#modalSettings.setAttribute('data-title', 'GraphSettings.title')
    // alter border (select and label)
    this.#border = document.createElement('input-select')
    this.#border.setAttribute('data-options', 'GraphSettings.border-0, General.diagramm')
    this.#border.id = uid()
    const borderLabel = document.createElement('mint-label')
    borderLabel.setAttribute('data-text', 'General.presentation')
    borderLabel.setAttribute('for', this.#border.id)
    // grid x (input and label)
    this.#gridx = document.createElement('input-number')
    this.#gridx.setAttribute('data-min', 0)
    this.#gridx.setAttribute('data-max', 10)
    this.#gridx.setAttribute('data-integer', true)
    this.#gridx.id = uid()
    const gridxLabel = document.createElement('mint-label')
    gridxLabel.setAttribute('data-text', 'GraphSettings.grid-x')
    gridxLabel.setAttribute('for', this.#gridx.id)
    // grid y (input and label)
    this.#gridy = document.createElement('input-number')
    this.#gridy.setAttribute('data-min', 0)
    this.#gridy.setAttribute('data-max', 10)
    this.#gridy.setAttribute('data-integer', true)
    this.#gridy.id = uid()
    const gridyLabel = document.createElement('mint-label')
    gridyLabel.setAttribute('data-text', 'GraphSettings.grid-y')
    gridyLabel.setAttribute('for', this.#gridy.id)
    // show legend (check box and label)
    this.#legend = document.createElement('input-switch')
    this.#legend.setAttribute('data-text', 'GraphSettings.show-legend')
    this.#legend.id = uid()
    const legendLabel = document.createElement('mint-label')
    legendLabel.classList.add('wide-label')
    legendLabel.setAttribute('data-text', 'GraphSettings.show-legend')
    legendLabel.setAttribute('for', this.#legend.id)
    // form
    const form = document.createElement('form')
    form.classList.add('form-container')
    form.append(borderLabel, this.#border, gridxLabel, this.#gridx, gridyLabel, this.#gridy, legendLabel, this.#legend)
    if (this.hasAttribute('data-fixed-border')) hide(this.#border, borderLabel)
    // alert box
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-type', 'warning')
    this.#alert.hide()
    // append to body
    this.#modalSettings.body.append(form, this.#alert)
    // buttons
    const browser = getBrowserName()
    const buttonSave = document.createElement('btn')
    buttonSave.classList.add('btn')
    buttonSave.setAttribute('data-i18n', 'General.btn-save-img')
    buttonSave.onclick = () => this.saveImage(document.title)
    const buttonCopy = document.createElement('btn')
    buttonCopy.classList.add('btn')
    buttonCopy.setAttribute('data-i18n', 'General.btn-copy-img')
    buttonCopy.onclick = () => this.copyImage()
    if (browser === 'firefox' || browser === 'safari') buttonCopy.classList.add('d-none')
    const buttonClose = document.createElement('btn')
    buttonClose.classList.add('btn', 'btn-secondary')
    buttonClose.setAttribute('data-i18n', 'General.btn-cancel')
    buttonClose.onclick = () => this.#modalSettings.hide()
    const buttonAccept = document.createElement('btn')
    buttonAccept.classList.add('btn', 'btn-primary')
    buttonAccept.setAttribute('data-i18n', 'General.btn-accept')
    buttonAccept.onclick = () => this.accept()
    // append to footer
    this.#modalSettings.footer.append(buttonSave, buttonCopy, buttonClose, buttonAccept)
  }

  /**
   * set coords and merge with current settings
   * @param {Object} newCoords - new coordinate system
   */
  set coords(newCoords) {
    // first, complete properties if missing
    if (isNaN(newCoords.border)) newCoords.border = 0
    if (newCoords.legend !== false) newCoords.legend = true
    if (isNaN(newCoords.x.grid)) newCoords.x.grid = 0
    if (isNaN(newCoords.y.grid)) newCoords.y.grid = 0
    // second, match merge with existing coords object
    if (this.#coords.x?.grid >= 0) newCoords.x.grid = this.#coords.x.grid
    if (this.#coords.y?.grid >= 0) newCoords.y.grid = this.#coords.y.grid
    if (this.#coords.border >= 0) newCoords.border = this.#coords.border
    if (this.#coords.legend === false) newCoords.legend = false
    // store coords
    this.#coords = newCoords
  }

  /**
   * get current coords
   * @returns {Object} coordinate system
   */
  get coords() {
    return structuredClone(this.#coords)
  }

  /**
   * Set canvas to paint on
   * @param {HTMLCanvasElement} c
   */
  set canvas(c) {
    this.#canvas = c
  }

  /**
   * show modal settings dialog
   */
  openSettings() {
    if (!this.#modalSettings) this.renderModalSettingsDialog()
    this.#coordsBackup = JSON.stringify(this.#coords)
    this.#alert.hide()
    this.#gridx.value = this.#coords.x.grid
    this.#gridy.value = this.#coords.y.grid
    this.#legend.value = this.#coords.legend
    this.#border.value = this.#coords.border
    this.#gridx.showIf(this.#coords.x.grid >= 0)
    this.#gridy.showIf(this.#coords.y.grid >= 0)
    this.#modalSettings.show()
  }

  /**
   * use user specified settings and close modal dialog
   */
  accept() {
    if (this.#modalSettings.querySelector('.invalid')) {
      this.#alert.setAttribute('data-text', 'General.warning-validate')
      this.#alert.show()
      translateElement(this.#alert)
    } else {
      this.#coords.border = this.#border.value
      this.#coords.x.grid = this.#gridx.value
      this.#coords.y.grid = this.#gridy.value
      this.#coords.legend = this.#legend.value
      this.#modalSettings.hide()
      this.dispatchEvent(new Event('change'))
    }
  }

  /**
   * close dialog (without applying the changes)
   */
  close() {
    if (!this.#coordsBackup) return
    this.#coords = JSON.parse(this.#coordsBackup)
    this.hide()
  }

  /**
   * copy image to clipboard
   */
  copyImage() {
    plot2d.copyImage(this.#canvas)
      .then(() => this.emitClose())
      .catch((e) => this.error = e.message)
  }

  /**
   * Save image to file
   * @param {String} name - file name
   */
  async saveImage(name) {
    this.#modalSettings.emitClose()
    if (!this.#modalDownloadFile) {
      await import('./modal-download-file.js')
      this.#modalDownloadFile = document.createElement('modal-download-file')
      getMintAppsContainer().append(this.#modalDownloadFile)
    }
    this.#modalDownloadFile.show()
    this.#modalDownloadFile.open({ name, type: 'canvas', data: this.#canvas })
  }

}

// Register elements
customElements.define('graph-point', GraphPoint)
customElements.define('graph-legend', GraphLegend)
customElements.define('graph-settings', GraphSettings)
