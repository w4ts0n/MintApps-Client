import { translateChildren } from '../modules/i18n.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * Show rotating clock to indicate that the user should wait
 * @property {String} msg - optional message
 * @property {Boolean} data-container - Embed icon in a div.container element
 * @property {Boolean} data-hide - hide wait-icon
 */
class WaitIcon extends HTMLMintElement {
  static observedAttributes = ['data-hide']

  connectedCallback() {
    this.render()
  }

  attributeChangedCallback() {
    this.render()
  }

  render() {
    if (this.hasAttribute('data-hide')) {
      this.innerHTML = ''
    } else {
      // draw icon and text
      const img = document.createElement('img')
      img.src = `../assets/icons/wait.svg`
      img.classList.add('invert-dark', 'rotating')
      img.style.width = '3rem'
      const center = document.createElement('div')
      center.classList.add('text-center')
      center.append(img)
      const span = document.createElement('span')
      span.setAttribute('data-i18n', this.dataset.msg ?? 'WaitIcon.please-wait')
      const alert = document.createElement('div')
      alert.classList.add('alert-primary', 'alert')
      alert.append(span, center)
      if (this.hasAttribute('data-container')) {
        const container = document.createElement('div')
        container.classList.add('container')
        container.append(alert)
        this.append(container)
      } else {
        this.append(alert)
      }
      // translate
      translateChildren(this)
    }
  }
}

customElements.define('wait-icon', WaitIcon)