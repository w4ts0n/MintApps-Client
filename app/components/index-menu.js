import { enableEnterToClick } from '../modules/accessibility.js'
import { getBgVarStyle } from '../modules/color.js'
import { translateChildren } from '../modules/i18n.js'
import { renderBlock, renderInline } from '../modules/markdown.js'
import { importCss } from '../modules/utils.js'
importCss(`index-menu.css`)

/**
 * Component that renders the menus used on the index, topic and search pages.
 * @property {String} [data-type = 'huge'] - type of menu ('huge' or 'large')
 * @property {Boolean} [data-translate = false] - wether to translate the contents
 * @property {Boolean} [data-markdown = false] - wehter to use markdown
 * @see MainMenu
 * @see TopicMenu
 */
class IndexMenu extends HTMLElement {
  /**
   * Set new menu
   * @param {Object} menuDefiniton - menu definition
   */
  set menu(menuDefiniton) {
    this.buildMenu(menuDefiniton)
    translateChildren(this)
    enableEnterToClick(this) 
  }

  /**
   * Build menu
   * @param {Object} menu - definition of menu (array of entries {title, text, icon, to})
   */
  buildMenu (menu) {
    const type = this.dataset.type ?? 'huge'
    const markdown = this.hasAttribute('data-markdown')
    const translate = this.hasAttribute('data-translate')
    this.innerHTML = ''
    if (!menu) return

    menu.forEach((entry, index) => {
      // image
      const img = document.createElement('img')
      img.src = entry.icon
      if (translate) img.setAttribute('data-i18n-alt', entry.title)
      else img.alt = entry.title
      const entryImage = document.createElement('div')
      entryImage.classList.add('entry-image')
      if (entry.dataIcon) img.classList.add('data-icon')
      else entryImage.style = getBgVarStyle(index, menu.length)
      entryImage.append(img)
      // title & text
      const title = document.createElement('p')
      const text = document.createElement('p')
      title.classList.add('title')
      if (translate && markdown) {
        title.setAttribute('data-i18n-md-inline', entry.title)
        text.setAttribute('data-i18n-md-block', entry.text)
      } else if (translate) {
        title.setAttribute('data-i18n', entry.title)
        text.setAttribute('data-i18n', entry.text)
      } else if (markdown) {
        title.innerHTML = renderInline(entry.title)
        text.innerHTML = renderBlock(entry.text)
      } else {
        title.innerText = entry.title
        text.innerText = entry.text
      }
      const entryText = document.createElement('div')
      entryText.classList.add('entry-text')
      entryText.append(title, text)
      // link
      const a = document.createElement('a')
      a.classList.add('entry', 'shadow', 'enter-click')
      a.href = entry.href
      if (type === 'huge') a.append(entryImage, entryText)
      else a.append(entryText, entryImage)
      // entry

      this.append(a)
    })
    this.classList.add(`menu-${type}`)
  }
}

customElements.define('index-menu', IndexMenu)