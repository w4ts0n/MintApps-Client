
import config from '../config.js'
import { emit, hide, showIf } from '../modules/utils.js'
import * as http from '../modules/http.js'
import './wait-icon.js'
import './alert-box.js'
import './item-list.js'
import './modal-share.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * List of examples for given type
 * @property {String} data-type - type of application, for example 'price'
 * @property {String} [data-title] - optional title / heading
 * @property {Boolean} [data-auto-load] - automatically load examples when element is connected
 * @fires load - user selected item
 * @see https://codeberg.org/MintApps/data
 */
class ExampleList extends HTMLMintElement {

  #visibleItems = [] // list of visible items
  #selectedItem = null // selected item
  #modalConfirm = null // reference to modal confirm
  #modalShare = null // reference to share dialog
  #allItems = [] // list of all items
  #title = null // reference to title
  #input = null // reference to input
  #alert = null // reference to alert box
  #list = null // reference to item list
  #wait = null// referenct to wait icon

  connectedCallback() {
    if (this.disconnected) return
    // title
    this.#title = document.createElement('h2')
    if (this.dataset.title) this.#title.setAttribute('data-i18n', this.dataset.title)
    else this.#title.classList.add('d-none')
    // input text
    this.#input = document.createElement('input')
    this.#input.style = "width:100%; box-sizing: border-box;"
    this.#input.setAttribute('data-i18n-placeholder', 'ExampleList.search-info')
    this.#input.id = Math.random()
    this.#input.oninput = () => this.search()
    // error
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-text', 'Errors.no-examples-available')
    this.#alert.hide()
    // wait icon
    this.#wait = document.createElement('wait-icon')
    // item list
    this.#list = document.createElement('item-list')
    this.#list.addEventListener('use', e => this.use(e))
    this.#list.addEventListener('share', e => this.share(e))
    // modal confirm
    this.#modalConfirm = document.createElement('modal-dialog')
    this.#modalConfirm.setAttribute('data-title', 'General.confirmation')
    this.#modalConfirm.setAttribute('data-buttons', 'accept, cancel')
    this.#modalConfirm.addEventListener('accept', () => this.loadExample())
    this.#modalConfirm.innerHTML = '<p data-i18n="General.confirm-local-load"></p>'
    // modal share
    this.#modalShare = document.createElement('modal-share')
    // root
    this.append(this.#title, this.#input, this.#alert, this.#wait, this.#list, this.#modalConfirm, this.#modalShare)
    this.classList.add('gap-stack')
    // auto load data
    if (this.hasAttribute('data-auto-load')) {
      this.loadData()
      this.search()
    }
  }

  /**
   * Load examples from server
   */
  loadData() {
    this.#wait.show()
    http.get('data:index.json').then((response) => {
      this.#wait.hide()
      this.#allItems = response.filter(item => item.type === this.dataset.type)
      this.search()
      showIf(this.#input, this.#allItems.length > 4)
    }).catch((error) => {
      this.#allItems = []
      this.#wait.hide()
      this.#alert.show()
      hide(this.#input)
      console.debug('could not load examples, consider installing the data repository,', error.message)
    })
  }

  /**
   * Open activity (and show modal to confirm)
   * @param {Event} event 
   */
  use(event) {
    this.#selectedItem = event.detail
    if (window.mintapps?.modified) {
      this.#modalConfirm.show()
    } else {
      this.loadExample()
    }
  }

  /**
   * Load example
   */
  loadExample() {
    this.#modalConfirm.hide()
    emit(this, 'load', this.#selectedItem)
  }

  /**
   * Search all items and display result
   */
  search() {
    this.#visibleItems = []
    const searchText = this.#input.value
    for (const item of this.#allItems) {
      if (
        !searchText ||
        (item.title + item.desc + item.subject).toLowerCase().indexOf(searchText.toLowerCase()) >= 0
      ) {
        const icon = item.icon ? `${config.dataPath}icons/${item.icon} ` : ''
        let copyright = ''
        if (item.author) copyright += item.author
        if (item.author && item.license) copyright += ', '
        if (item.license) copyright += item.license
        this.#visibleItems.push({ id: item.id, title: item.title, type: item.type, desc: item.desc, copyright, icon })
      }
    }
    this.#list.renderList(this.#visibleItems)
  }

  /**
   * Show hyperlink and QR code to share item
   */
  share(event) {
    const item = event.detail
    this.#modalShare.setAttribute('data-url', `${window.location.origin}${window.location.pathname}?play=${item.id}`)
    this.#modalShare.show()
  }

}

customElements.define('example-list', ExampleList)
