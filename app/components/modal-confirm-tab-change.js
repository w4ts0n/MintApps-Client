import { translateChildren } from '../modules/i18n.js'
import { ModalDialog } from './modal-dialog.js'
import './qr-code.js'
import './alert-box.js'

/**
 * Confirm tab change, used by PageTitle
 * @see PageTitle
 */
class ModalConfirmTabChange extends ModalDialog {

  connectedCallback() {
    if (this.disconnected) return
    super.connectedCallback()
    // title
    this.title.setAttribute('data-i18n', 'General.confirmation')
    // body
    const p = document.createElement('p')
    p.setAttribute('data-i18n', 'General.confirm-tab-change')
    this.body.append(p)
    // footer
    const buttonAccept = document.createElement('button')
    buttonAccept.classList.add('btn', 'btn-primary')
    buttonAccept.onclick = () => this.emitAccept()
    buttonAccept.setAttribute('data-i18n', 'General.btn-next')
    this.footer.append(buttonAccept)
    const buttonCancel = document.createElement('button')
    buttonCancel.classList.add('btn', 'btn-secondary')
    buttonCancel.onclick = () => this.emitClose()
    buttonCancel.setAttribute('data-i18n', 'General.btn-cancel')
    this.footer.append(buttonCancel)
    this.footer.removeAttribute('data-hide')
    // root
    translateChildren(this)
  }
}

customElements.define('modal-confirm-tab-change', ModalConfirmTabChange)

