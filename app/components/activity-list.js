
import { emit, hide, show, showIf } from '../modules/utils.js'
import * as http from '../modules/http.js'
import * as storage from '../modules/storage.js'
import * as session from '../modules/session.js'
import './wait-icon.js'
import './alert-box.js'
import './item-list.js'
import './modal-share.js'
import './modal-dialog.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * List of own activities for given type
 * @property {String} data-type - type of application, for example 'price'
 * @property {String} [data-title] - optional title / heading
 * @property {Boolean} [data-hide-empty] - hide empty list
 * @property {Boolean} [data-auto-load] - automatically load activities when element is connected
 * @fires load - user selected item
 * @fires num - number of items in list
 * @see https://codeberg.org/MintApps/server
 */
class ActivityList extends HTMLMintElement {
  #visibleItems = [] // list of visible items
  #selectedItem = null // selected item
  #modalConfirm = null // reference to modal confirm
  #modalShare = null // reference to share dialog
  #modalDelete = null // reference to delete dialog
  #allItems = [] // list of all items
  #title = null // reference to tmodalDeleteitle
  #input = null // reference to input
  #alert = null // reference to alert box
  #list = null // reference to item list
  #wait = null// referenct to wait icon

  constructor() {
    super()
    storage.watch('session', () => this.loadData())
  }

  async connectedCallback() {
    if (this.disconnected) return
    // title
    this.#title = document.createElement('h2')
    if (this.dataset.title) this.#title.setAttribute('data-i18n', this.dataset.title)
    else this.#title.classList.add('d-none')
    // input text
    this.#input = document.createElement('input')
    this.#input.style = "width:100%; box-sizing: border-box;"
    this.#input.setAttribute('data-i18n-placeholder', 'ExampleList.search-info')
    this.#input.id = Math.random()
    this.#input.oninput = () => this.search()
    // error
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-type', 'warning')
    this.#alert.hide()
    // wait icon
    this.#wait = document.createElement('wait-icon')
    // item list
    this.#list = document.createElement('item-list')
    this.#list.setAttribute('data-del', '')
    this.#list.addEventListener('use', e => this.use(e))
    this.#list.addEventListener('share', e => this.share(e))
    this.#list.addEventListener('delete', e => this.showModalDelete(e))
    // modal confirm
    this.#modalConfirm = document.createElement('modal-dialog')
    this.#modalConfirm.setAttribute('data-buttons', 'accept, cancel')
    this.#modalConfirm.setAttribute('data-title', 'General.confirmation')
    this.#modalConfirm.addEventListener('accept', () => this.loadActivity())
    this.#modalConfirm.innerHTML = '<div data-i18n="General.confirm-local-load"></div>'
    // modal delete
    this.#modalDelete = document.createElement('modal-dialog')
    this.#modalDelete.setAttribute('data-buttons', 'delete, cancel')
    this.#modalDelete.setAttribute('data-title', 'General.confirmation')
    this.#modalDelete.addEventListener('delete', () => this.deleteActivity())
    this.#modalDelete.innerHTML = '<div data-i18n="ActivityList.confirm-delete"></div>'
    // modal share
    this.#modalShare = document.createElement('modal-share')
    // root
    this.append(this.#title, this.#input, this.#alert, this.#wait, this.#list, this.#modalConfirm, this.#modalShare, this.#modalDelete)
    this.classList.add('gap-stack')
    // auto load data
    if (this.hasAttribute('data-auto-load')) {
      this.loadData()
      this.search()
    }
  }

  /**
   * load activities from server
   */
  async loadData() {
    this.#allItems = []
    const s = await session.isValidSession()
    if (s) {
      try {
        show(this.#wait, this.#input, this.#list)
        const response = await http.post('sync:get-activity-list', { user: s.user, type: this.dataset.type })
        this.#wait.hide()
        this.#alert.hide()
        show(this.#title, this.#list)
        response.forEach(item => this.#allItems.push(item))
        if (this.#allItems.length > 0) {
          this.search()
          showIf(this.#input, this.#allItems.length > 4)
        } else {
          hide(this.#input)
          if (this.hasAttribute('data-hide-empty')) {
            // hide empty if required (for mint-user-data)
            hide(this.#title, this.#list)
          } else {
            // show info on missing activities
            this.#alert.setAttribute('data-type', 'info')
            this.#alert.show('ActivityList.no-activities-available')
          }
        }
        emit(this, 'num', this.#allItems.length)
      } catch (e) {
        this.#wait.hide()
        this.#alert.setAttribute('data-type', 'warning')
        this.#alert.show(`Errors.${e.message}`)
        hide(this.#input)
        console.debug('could not load activites, check server configuration', e.message)
      }
    } else {
      hide(this.#wait, this.#input, this.#list)
      this.#alert.setAttribute('data-type', 'info')
      this.#alert.show('ActivityList.login')
    }
  }

  /**
   * Open activity (and show modal to confirm)
   * @param {Event} event 
   */
  use(event) {
    this.#selectedItem = event.detail
    if (window.mintapps?.modified) {
      this.#modalConfirm.show()
    } else {
      this.loadActivity()
    }
  }

  /**
   * Load activity
   */
  loadActivity() {
    this.#modalConfirm.hide()
    emit(this, 'load', this.#selectedItem)
  }

  /**
   * Search all items and display result
   */
  search() {
    this.#visibleItems = []
    const searchText = this.#input.value
    for (const item of this.#allItems) {
      if (
        !searchText ||
        (item.title + item.desc + item.subject).toLowerCase().indexOf(searchText.toLowerCase()) >= 0
      ) {
        const icon = item.icon ?? ''
        this.#visibleItems.push({ id: item.id, title: item.title, type: item.type, desc: item.desc, icon })
      }
    }
    this.#list.renderList(this.#visibleItems)
  }

  /**
   * Search all items and display result
   */
  share(event) {
    const item = event.detail
    this.#modalShare.setAttribute('data-url', `${window.location.origin}${window.location.pathname}?play=${item.id}`)
    this.#modalShare.show()
  }

  /**
   * Show modal dialog to confirm delete
   */
  showModalDelete(event) {
    this.#selectedItem = event.detail
    this.#modalDelete.show()
  }

  /**
   * Delete activity from server
   */
  async deleteActivity() {
    const s = await session.isValidSession()
    if (s) {
      try {
        this.#modalDelete.hide()
        await http.post('sync:delete-activity', { id: this.#selectedItem.id, user: s.user })
        await this.loadData()
      } catch (e) {
        this.#alert.setAttribute('data-text', `Errors.${e.message}`)
        this.#alert.show()
      }
    } else {
      storage.value.session = {}
    }
  }

}

customElements.define('activity-list', ActivityList)
