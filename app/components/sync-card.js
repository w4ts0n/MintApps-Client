import { MintCard } from './mint-card-components.js'
import { gid, show, hide, showIf, emit, importCss } from '../modules/utils.js'
import * as sync from '../modules/sync.js'
import * as http from '../modules/http.js'
import * as storage from '../modules/storage.js'
import './alert-box.js'
import './qr-code.js'
import './progress-bar.js'
import { isValidSession } from '../modules/session.js'
import { translateElement } from '../modules/i18n.js'
import * as soundPlayer from '../modules/sound-player.js'
importCss('sync-card.css')

/**
 * MintCard that forms the link to the sync module in multi-user applications
 * @property {String} data-base-url - base url of link for participants
 * @property {String} data-type - type of app (pairs, quiz, poll, plot ...)
 * @property {Boolean} [data-show-names] - display participants names
 * @property {Boolean} [data-allow-close] - display close button
 * @porperty {Boolean} [data-sound] - play sound for new members
 * @property {String} [data-name=''] - name of room
 * @property {String} [data-warn] - show warn message when syncing, key gets translated
 * @property {Boolean} [data-info] - show optional invite info
 * @fires syncing - syncing started
 * @fires close - card closed
 */
class SyncCard extends MintCard {
  #state = 'start' // state of this card
  #url = '' // entry point for participants
  #error = '' // general error message text
  #user = '' // user for authentification (only password)
  #secret = '' // secret for authentification (captcha, solved challenge or password)
  #members = [] // list of room members
  #worker // worker for solving cryptographic challenges
  #captchaImg // reference, captcha image
  #captchaInput  // reference, captcha input field
  #qrContainer // reference to qr container
  #qrCode // reference to qr code
  #blockStart // reference, block for state start
  #blockPassword // reference, block for state password
  #blockCaptcha  // reference, block for captcha dialog
  #blockChallenge // reference, block for challenge dialog
  #blockShare // reference, block for share dialog
  #blockError // reference, block to display general errors
  #btnInvite // reference, invite button
  #btnNext // reference, next button
  #btnClose // reference, close button
  #progress // reference to progress bar
  #alertError // reference, box to display general error message
  #alertChallengeInvalid // reference to error message
  #alertCaptchaInvalid // reference to error message
  #countMembers // reference counter for members
  #showNames // reference to name list
  #inviteLink // reference to hyperlink
  #inviteInfo // optional info text
  #alertWarn // optional warning text when syncing
  #onsyncing // listener to syncing event
  #onclose // listener to close event

  connectedCallback() {
    if (this.disconnected) return
    this.setAttribute('data-title', 'SyncCard.title')
    super.connectedCallback()
    // body
    let html = `
    <div id="sc-start" class="gap-stack">
      <img class="floating-logo invert-dark" src="../assets/icons/sync-server.svg">
      <p id="sc-info-invite" data-i18n="SyncCard.invite-info-text"></p>
      <p data-i18n="SyncCard.info-privacy"></p>
    </div>
    <div id="sc-password">
      <alert-box data-text="SyncCard.password-required"></alert-box>
    </div>
    <div id="sc-captcha">
      <div class="gap-stack">
        <p data-i18n="SyncCard.captcha-required"></p>
        <div class="text-center"><img id="sc-captcha-img"></div>
        <input id="sc-captcha-input" class="w-100 form-border" @keyup.enter="this.clickCreate">
        <alert-box id="sc-alert-captcha-invalid" data-type="warning" data-text="SyncCard.captcha-wrong"></alert-box>
      </div>
    </div>
    <div id="sc-challenge" class="gap-stack">
      <p data-i18n-md="SyncCard.challenge-info"></p>
      <alert-box id="sc-alert-challenge-wait" data-text="SyncCard.challenge-wait"></alert-box>
      <alert-box id="sc-alert-challenge-invalid" data-type="warning" data-text="SyncCard.challenge-wrong"></alert-box>
      <progress-bar id="sc-progress-bar"></progess-bar>
    </div>
    <div id="sc-error" class="gap-stack">
      <p data-i18n="SyncCard.error-text"></p>
      <alert-box id="sc-alert-error" data-type="warning"></alert-box>
    </div>
    <div id="sc-share" class="sc-share-container">
      <div class="flex-grow">
        <p data-i18n="SyncCard.invite-text"></p>
        <p id="sc-count-members" data-i18n="SyncCard.count-members"></p>
        <p id="sc-show-names"></p>
      </div>
      <div id="sc-qr-container" class="sc-qr-container">
        <a id="sc-invite-link" target="_blank" class="w-100 d-block">
          <qr-code id="sc-qr-code" data-radius="0.5" data-size="200"><qr-code>
        </a>
      </div>
    </div>
    <alert-box id="sc-alert-warn" data-hide></alert-box>
    `
    this.body.innerHTML = html
    // footer
    html = `
      <button id="sc-btn-invite" class="btn btn-secondary" data-i18n="General.btn-invite"></button>
      <button id="sc-btn-next" class="btn btn-primary" data-i18n="General.btn-next"></button>
      <button id="sc-btn-close" class="btn" data-i18n="General.btn-close"></button>
    `
    this.footer.innerHTML = html
    // create references
    this.#captchaImg = gid('sc-captcha-img')
    this.#captchaInput = gid('sc-captcha-input')
    this.#qrCode = gid('sc-qr-code')
    this.#qrContainer = gid('sc-qr-container')
    this.#blockStart = gid('sc-start')
    this.#blockPassword = gid('sc-password')
    this.#blockCaptcha = gid('sc-captcha')
    this.#blockChallenge = gid('sc-challenge')
    this.#blockShare = gid('sc-share')
    this.#blockError = gid('sc-error')
    this.#btnInvite = gid('sc-btn-invite')
    this.#btnNext = gid('sc-btn-next')
    this.#btnClose = gid('sc-btn-close')
    this.#alertError = gid('sc-alert-error')
    this.#progress = gid('sc-progress-bar')
    this.#alertChallengeInvalid = gid('sc-alert-challenge-invalid')
    this.#alertCaptchaInvalid = gid('sc-alert-captcha-invalid')
    this.#countMembers = gid('sc-count-members')
    this.#showNames = gid('sc-show-names')
    this.#inviteLink = gid('sc-invite-link')
    this.#inviteInfo = gid('sc-info-invite')
    this.#alertWarn = gid('sc-alert-warn')

    // events
    this.#btnInvite.onclick = () => this.clickInvite()
    this.#btnNext.onclick = () => this.createRoom()
    this.#btnClose.onclick = () => { 
      if (this.#onclose) this.#onclose()
      emit(this, 'close')
      this.hide()
    }
    // start
    storage.watch('session', () => this.updateSession())
    sync.setOwnerListener(info => this.ownerListener(info))
    this.updateGui()
  }

  /**
   * Start syncing (corresponds to pressing the "Invite" button)
   */
  start() {
    if (sync.isSyncing()) {
      this.#state = 'share'
      this.updateGui()
    }
    else this.clickInvite()
  }

  /**
   * react to session change
   */
  updateSession() {
    isValidSession().then(valid => {
      if (valid && this.#state === 'password') this.clickInvite()
    })
  }

  /**
   * Update user interface
   */
  updateGui() {
    // show or hide blocks
    showIf(this.#blockStart, this.#state === 'start')
    showIf(this.#blockPassword, this.#state === 'password')
    showIf(this.#blockCaptcha, this.#state.startsWith('captcha'))
    showIf(this.#blockChallenge, this.#state.startsWith('challenge'))
    showIf(this.#blockShare, this.#state === 'share')
    showIf(this.#blockError, this.#state === 'error')
    // show or hide buttons
    showIf(this.#btnInvite, this.#state === 'start')
    showIf(this.#btnNext, this.#state.startsWith('captcha'))
    showIf(this.#btnClose, this.dataset.allowClose !== undefined)
    showIf(this.footer, this.#state === 'start' || this.#state.startsWith('captcha') || this.dataset.allowClose !== undefined)
    // state specific updates
    switch (this.#state) {
      case 'start':
        showIf(this.#inviteInfo, this.dataset.info !== undefined)
        break
      case 'error':
        this.#alertError.show(this.#error)
        break
      case 'challenge':
        hide(this.#alertChallengeInvalid)
        break
      case 'challenge-invalid':
        show(this.#alertChallengeInvalid)
        break
      case 'captcha':
        hide(this.#alertCaptchaInvalid)
        this.#captchaInput.classList.remove('invalid')
        break
      case 'captcha-invalid':
        show(this.#alertCaptchaInvalid)
        this.#captchaInput.classList.add('invalid')
        break
      case 'share':
        this.#countMembers.setAttribute('data-count', this.#members.length)
        translateElement(this.#countMembers)
        showIf(this.#showNames, this.hasAttribute('data-show-names'))
        if (this.dataset.warn) this.#alertWarn.show(this.dataset.warn)
        if (this.dataset.showNames !== undefined) {
          let html = ''
          this.#members.forEach(member => html += `<span class="sc-member">${member.name}</span>`)
          this.#showNames.innerHTML = html
        }
        break
    }
  }

  /**
   * React to invite button
   */
  async clickInvite() {
    if (this.#url && sync.isSyncing()) {
      this.#state = 'share'
    } else {
      this.#members= []
      this.#error = ''
      try {
        const authType = (await http.get('sync:get-auth-type')).type
        if (await isValidSession()) {
          this.#state = 'password'
          this.#user = storage.get('session').user
          await this.createRoom()
        } else if (authType === 'captcha') {
          this.#state = 'captcha'
          this.#captchaImg.src = (await http.get('sync:get-captcha')).captcha
        } else if (authType === 'challenge') {
          this.#state = 'challenge'
          this.updateGui()
          try {
            const solvedChallenge = await this.getChallenge()
            if (this.#state === 'challenge') {
              this.#secret = solvedChallenge
              await this.createRoom()
            }
          } catch {
            this.#state = 'challenge-invalid'
          }
        } else if (authType === 'password') {
          this.#state = 'password'
        } else {
          await this.createRoom()
        }
      } catch (e) {
        this.#state = 'error'
        this.#error = `Errors.${e.message}`
      }
    }
    this.updateGui()
  }

  /**
   * Create room
   */
  async createRoom() {
    try {
      if (this.#state.endsWith('-invalid')) { this.#state = this.#state.split('-')[0] }
      if (this.#state.startsWith('captcha')) this.#secret = this.#captchaInput.value
      const room = await sync.createRoom(
        this.dataset.name,
        this.dataset.type,
        this.#user,
        this.#secret
      )
      const url = new URL(document.URL)
      url.pathname = `${url.pathname.substring(0, url.pathname.lastIndexOf('/'))}/${this.dataset.baseUrl}.html`
      url.searchParams.delete('room')
      url.searchParams.append('room', room)
      url.searchParams.delete('play')
      if (this.dataset.play) url.searchParams.append('play', this.dataset.play)
      url.searchParams.delete('lang')
      this.#url = url.href
      this.#state = 'share'
      this.#qrCode.setAttribute('data-text', this.#url)
      this.#inviteLink.href = this.#url
      this.updateGui()
      this.repaint()
      if (this.#onsyncing) this.#onsyncing()
      emit(this, 'syncing')
    } catch (e) {
      console.log(e)
      if (e.message === 'invalid-secret' || e.message === 'secret-invalid') {
        if (this.#state === 'captcha') this.#state = 'captcha-invalid'
        if (this.#state === 'captcha-invalid') this.#captchaImg.src = (await http.get('sync:get-captcha')).captcha
        if (this.#state === 'password') storage.set('session', {})
        if (this.#state === 'challenge') this.#state = 'challenge-invalid'
      } else {
        this.#state = 'error'
        this.#error = `Errors.${e.message}`
      }
    }
    this.updateGui()
  }

  /**
   * Send "end" message to all participants and delete room,
   * doesn't throw any errors
   */
  async deleteRoom() {
    this.#state = 'start' // update gui without delay, so code isn't visibile any more
    this.updateGui()
    try {
      this.#btnInvite.disabled = true
      // inform participants
      sync.sendMessage('', { action: 'end' })
      // delay so, the message will be sent to all participants
      await sync.delay(1000)
      // stop sync
      await sync.stop()
      this.#btnInvite.disabled = false
    } catch (e) {
      console.debug(e)
      this.#btnInvite.disabled = false
    }
    this.updateGui()
  }

  /**
   * Listen to info messages from the sync module
   * @param {Object} info - internal info object from sync module
   */
  ownerListener(info) {
    if (info.members) {
      if (this.dataset.sound !== undefined) {
        if (info.members.length > this.#members.length) soundPlayer.play('pushSound')
        if (info.members.length < this.#members.length) soundPlayer.play('popSound')
      }
      this.#members.splice(0, this.#members.length)
      for (const member of info.members) this.#members.push(member)
      this.updateGui()
    }
  }

  /**
   * Repaint (qr code) after resize or theme change
   */
  repaint() {
    if (!this.#qrContainer) return
    this.#qrCode.setAttribute('data-size', this.#qrContainer.clientWidth)
  }

  /**
   * Get and solve challenge (for creating a new room)
   */
  getChallenge() {
    return new Promise((resolve, reject) => {
      http
        .get('sync:get-challenge')
        .then((response) => {
          this.#worker = new Worker('../modules/sync-challenge-worker.js')
          this.#worker.postMessage({ challenge: response.challenge })
          this.#worker.onmessage = (message) => {
            if (message.data?.solution) resolve(message.data.solution)
            else if (message.data?.progress) { this.#progress.value = message.data.progress }
          }
        })
        .catch(reject)
    })
  }

  /**
   * set syncing listener
   */
  set onsyncing (value) {
    this.#onsyncing = value
  }

  /**
   * set close listener
   */
  set onclose (value) {
    this.#onclose = value
  }
}

customElements.define('sync-card', SyncCard)