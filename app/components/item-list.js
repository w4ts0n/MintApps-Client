import { importCss } from '../modules/utils.js'
import { renderInline } from '../modules/markdown.js'
import { shortenText } from '../modules/text.js'
import { translateChildren } from '../modules/i18n.js'
import { emit } from '../modules/utils.js'
importCss('item-list.css')

class ItemList extends HTMLElement {
  #list = [] // list of items

  renderList(list) {
    this.#list = list
    this.classList.add('item-list')
    this.innerHTML = ''
    this.#list.forEach(item => {
      // image
      const img = document.createElement('img')
      img.src = item.icon ? item.icon : `../assets/icons/${item.type}.svg`
      if (!item.icon) img.classList.add('invert-dark')
      // text
      const text = document.createElement('div')
      text.classList.add('text-column')
      const p1 = document.createElement('div')
      p1.style.fontWeight = 'bold'
      p1.innerHTML = renderInline(item.title)
      text.append(p1)
      if (item.subject) {
        const p2 = document.createElement('div')
        p2.style.fontStyle = 'italic'
        p2.setAttribute('data-i18n', `Menus.${item.subject}`)
        text.append(p2)
      }
      const p3 = document.createElement('div')
      p3.innerHTML = renderInline(shortenText(item.desc, 100))
      p3.classList.add('item-list-desc')
      text.append(p3)
      if (item.copyright) {
        const p4 = document.createElement('div')
        p4.innerText =  item.copyright
        p4.classList.add('item-list-copyright')
        text.append(p4)
      }
      // actions
      const actions = document.createElement('div')
      actions.classList.add('action-column')
      const btnLoad = document.createElement('button')
      btnLoad.className = 'btn'
      btnLoad.setAttribute('data-i18n-title', 'General.btn-load')
      btnLoad.onclick = () => emit(this, 'use', item)
      btnLoad.innerHTML = `<img class="item-list-button-img invert-dark" src="../assets/icons/run.svg">`
      actions.append(btnLoad)
      const btnShare = document.createElement('button')
      btnShare.className = 'btn'
      btnShare.setAttribute('data-i18n-title', 'General.share')
      btnShare.onclick = () =>  emit(this, 'share', item)
      btnShare.innerHTML = `<img class="item-list-button-img invert-dark" src="../assets/icons/share.svg">`
      actions.append(btnShare)
      if (this.hasAttribute('data-del')) {
        const btnDelete = document.createElement('button')
        btnDelete.className = 'btn'
        btnDelete.setAttribute('data-i18n-title', 'General.btn-delete')
        btnDelete.onclick = () => emit(this, 'delete', item)
        btnDelete.innerHTML = `<img class="item-list-button-img invert-dark" src="../assets/icons/delete.svg">`
        actions.append(btnDelete)
      }
      // container
      const div = document.createElement('div')
      div.append(img, text, actions)
      div.classList.add('item', 'shadow', 'actions')
      this.append(div)
    })
    translateChildren(this)
  }

}

customElements.define('item-list', ItemList)