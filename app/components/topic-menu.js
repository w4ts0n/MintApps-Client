import { buildHref } from '../modules/utils.js'
import { sitemap } from '../sitemap.js'
import './index-menu.js'

/**
 * Component that displays the topic menus and uses the information from the sitmap
 * @property {String} data-topic - id of topic, for example physics
 * @see IndexMenu
 */
class TopicMenu extends HTMLElement {
  connectedCallback() {
    const topic = sitemap.topics.find((x) => x.id === this.dataset.topic)
    if (!topic || !topic.subtopics) {
      window.location = './mint-not-found'
      return
    }
    const title = topic.title
    const subtitle = topic.text
    for (const subtopic of topic.subtopics) {
      const menu = new Array()
      for (const entry of subtopic.sites) {
        if (entry.id) {
          menu.push({
            title: entry.title,
            text: entry.text,
            href: buildHref(entry.id),
            icon: `../assets/icons/${entry.icon}`
          })
        }
      }
      if (menu.length > 0) {
        // update view
        const h2 = document.createElement('h2')
        h2.setAttribute('data-i18n', subtopic.title)
        const indexMenu = document.createElement('index-menu')
        indexMenu.setAttribute('data-type', 'large')
        indexMenu.setAttribute('data-translate', true)
        indexMenu.menu = menu
        this.append(h2, indexMenu)
      }
    }
    document.getElementById('page-title').setAttribute('data-title', title)
    document.getElementById('page-title').setAttribute('data-subtitle', subtitle)
  }
}

customElements.define('topic-menu', TopicMenu)