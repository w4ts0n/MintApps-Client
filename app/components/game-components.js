import config from '../config.js';
import { allLocales, translateChildren } from '../modules/i18n.js';
import { renderBlock } from '../modules/markdown.js';
import { MintCard } from './mint-card-components.js'
import './alert-box.js'
import { isSupportedLocale } from '../modules/i18n.js'
import { emit, showIfDetails } from '../modules/utils.js';
import { credits } from '../modules/music-player.js'

/**
 * Card showing meta information about a game; used in the lobby of games
 * To display it, the setter for the attribute "game" must be called.
 * @property {String} data-music - display additional info on music playing (not part of game)
 * @fires start - user pressed start button
 * @fires invite - user pressed invite button
 */
class GameInfo extends MintCard {
  static observedAttributes = ['data-music']

  #game // current game

  attributeChangedCallback(attr) {
    if (attr === 'data-music' && this.#game) this.render()
  }

  /**
   * Render current game
   */
  render() {
    // clear and set title
    this.title.setAttribute('data-i18n', 'General.information')
    this.body.innerHTML = ''
    // body
    const div = document.createElement('div')
    //div.style = 'display: flex; gap: 1rem; align-items: flex-start;'
    // body → icon
    const img = document.createElement('img')
    img.style = 'width: 50%; min-width:8rem; float:right'
    if (this.#game.icon) {
      img.src = this.#game.icon
    } else {
      img.classList.add('invert-dark', 'opa-75')
      img.src = `../assets/icons/games.svg`
    }
    // body → infos
    const infos = document.createElement('div')
    infos.classList.add('info-item')
    // body → infos → description
    if (this.#game.desc) {
      const p = document.createElement('p')
      p.innerHTML = renderBlock(this.#game.desc)
      infos.append(p)
    }
    // body → infos → subject
    if (this.#game.subject) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Games.subject"></b>: <span data-i18n="Menus.${this.#game.subject}"></span>`
      infos.append(p)
    }
    // body → infos → author
    if (this.#game.author) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Games.author"></b>: <span>${this.#game.author}</span>`
      infos.append(p)
    }
    // body → infos → license
    if (this.#game.license) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Games.license"></b>: <span>${this.#game.license}</span>`
      infos.append(p)
    }
    // body → infos → language
    if (this.#game.locale && isSupportedLocale(this.#game.locale)) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Menus.locale"></b>: <span>${allLocales[this.#game.locale].text}</span>`
      infos.append(p)
    }
    // number of cards (for example price or pairs)
    if (this.#game.cards) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Games.number-cards"></b>: <span>${this.#game.cards.length}</span>`
      infos.append(p)
    }
    // number of question (for example poll, quiz)
    if (this.#game.questions) {
      const p = document.createElement('p')
      p.innerHTML = `<b data-i18n="Games.number-questions"></b>: <span>${this.#game.questions.length}</span>`
      infos.append(p)
    }
    // music
    if (this.dataset.music) {
      const p = document.createElement('p')
      const playList = this.dataset.music.split(',').map(m => m.trim())
      p.innerHTML = `<b data-i18n="Games.music"></b>: <span>${credits(playList)}</span>`
      infos.append(p)
    }
    div.append(img, infos)
    this.body.append(div)
    translateChildren(this)
  }

  /**
   * Set current game
   * @param {Object} newGame - game definition, for example price, pairs or quiz object from data-repository
   */
  set game(newGame) {
    this.#game = newGame
    this.render()
  }
}

/**
 * Card showing patameters of a game; used in the lobby of games
 * @property {Boolean} [data-allow-invite=false] - wether game supports multi user mode
 * @property {Boolean} [data-allow-start=false] - whether the game can be started (reactive)
 */
class GameParams extends MintCard {
  static observedAttributes = ['data-allow-start', 'data-allow-invite']

  #allowInvite = false // wether game supports multi user mode
  #allowStart = true // whether the game can be started
  #btnStart = null // reference to start button
  #btnInvite = null // reference to invite button
  #oninvite = null // listener to invite events
  #onstart = null // listener to start event

  connectedCallback() {
    if (this.disconnected) return
    this.#allowInvite = config.syncUrl && this.hasAttribute('data-allow-invite')
    this.#allowStart = this.hasAttribute('data-allow-start')
    // title
    this.setAttribute('data-title', 'General.card-params')
    super.connectedCallback()
    // body → alert
    const alert = document.createElement('alert-box')
    alert.setAttribute('data-text', 'SyncCard.invite-info-text')
    this.body.append(alert)
    if (!this.#allowInvite) alert.hide()
    // footer → start button
    this.#btnStart = document.createElement('button')
    if (!this.#allowStart) this.#btnStart.setAttribute('disabled', '')
    this.#btnStart.classList.add('btn', 'btn-primary')
    this.#btnStart.setAttribute('data-i18n', 'General.btn-start')
    this.#btnStart.onclick = () => {
      if (this.#onstart) this.#onstart()
      emit(this, 'start')
    }
    this.footer.append(this.#btnStart)
    // footer → invite button
    this.#btnInvite = document.createElement('button')
    this.#btnInvite.classList.add('btn', 'btn-secondary')
    this.#btnInvite.setAttribute('data-i18n', 'General.btn-invite')
    this.#btnInvite.onclick = () => {
      alert.hide()
      if (this.#oninvite) this.#oninvite()
      emit(this, 'invite')
    }
    if (!this.#allowInvite) this.#btnInvite.setAttribute('data-hide', '')
    this.footer.append(this.#btnInvite)
    this.footer.removeAttribute('data-hide')
  }

  // eslint-disable-next-line no-unused-vars
  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-allow-start':
        if (this.#btnStart) {
          this.hasAttribute('data-allow-start') ? this.#btnStart.removeAttribute('disabled') : this.#btnStart.setAttribute('disabled', '')
        }
        break
      case 'data-allow-invite':
        if (this.#btnInvite) {
          config.syncUrl && this.hasAttribute('data-allow-invite') ? this.#btnInvite.removeAttribute('data-hide') : this.#btnInvite.setAttribute('data-hide', '')
        }
    }
  }

  /**
   * Set listener to invite events
   * @param {Function} listener 
   */
  set oninvite(listener) {
    this.#oninvite = listener
  }

  /**
   * Set listener to start events
   * @param {Function} listener 
   */
  set onstart(listener) {
    this.#onstart = listener
  }

}

/**
 * Card giving general information on editing a game; used in editor
 */
class GameEditInfo extends MintCard {
  connectedCallback() {
    if (this.disconnected) return
    this.setAttribute('data-title', 'Games.info')
    super.connectedCallback()
    const ul = document.createElement('ul')
    for (let i = 1; i <= 4; i++) {
      const li = document.createElement('li')
      li.setAttribute('data-i18n-md-inline', `Games.info${i}`)
      ul.append(li)
    }
    this.body.append(ul)
  }
}

/**
 * Card to edit the meta information of a game 
 * @param {String} data-type - game type (e.g. pairs, poll, ...)
 * @fires change - information has changed
 */
class GameEditSettings extends MintCard {
  #game = {} // reference to game
  #properties = ['title', 'subject', 'desc', 'license', 'author', 'locale']
  connectedCallback() {
    if (this.disconnected) return
    this.setAttribute('data-title', 'General.settings')
    super.connectedCallback()
    const html = `
      <form class="form-container">
        <mint-label data-text="General.title" for="game-edit-title"></mint-label>
        <input-text id="game-edit-title" data-markdown data-min="1" data-max="300"></input-text>
        <mint-label data-detail data-text="Games.subject" for="game-edit-subject"></mint-label>
        <input-text data-detail id="game-edit-subject" data-min="0" data-max="300"></input-text>
        <mint-label data-detail data-text="General.description" for="game-edit-desc"></mint-label>
        <input-text data-detail id="game-edit-desc" data-markdown data-multi-line data-min="0" data-max="500"></input-text>
        <mint-label data-detail data-text="Games.author" for="game-edit-author"></mint-label>
        <input-text data-detail id="game-edit-author" data-min="0" data-max="100"></input-text>
        <mint-label data-detail data-text="Games.license" for="game-edit-license"></mint-label>
        <input-text data-detail id="game-edit-license" data-min="0" data-max="100"></input-text>
        <mint-label data-detail data-text="Menus.locale" for="game-edit-locale"></mint-label>
        <input-text data-detail id="game-edit-locale" data-min="0" data-max="8"></input-text>
        <mint-label data-detail data-text="General.img-optional" for="game-edit-icon"></mint-label>
        <input-file data-detail id="game-edit-icon" data-file-type="image" data-max-size="0.2" data-max-dim="300"></input-file>
        <mint-label class="wide-label" data-text="General.show-details" for="game-edit-details"></mint-label>
        <input-switch id="game-edit-details"></input-switch>
      </form>
    `
    this.body.innerHTML = html
    // hide details
    showIfDetails(this, false)
    document.getElementById('game-edit-details').oninput = () => showIfDetails(this, document.getElementById('game-edit-details').value)
    // event listeners for normal input fields
    this.#properties.forEach(prop => {
      const input = document.getElementById(`game-edit-${prop}`)
      input.oninput = () => {
        if (this.#game[prop] !== undefined && this.#game[prop] !== input.value) {
          this.#game[prop] = input.value
          emit(this, 'change')
          window.mintapps.modified = this.dataset.type
        }
      }
    })
    // event listener for image
    document.getElementById('game-edit-icon').addEventListener('input', () => {
      const file = document.getElementById('game-edit-icon').value
      if (file) {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = e => { this.#game.icon = e.target.result }
      } else {
        this.#game.icon = ''
      }
      emit(this, 'change')
      window.mintapps.modified = this.dataset.type
    })
  }

  set game(newGame) {
    this.#game = newGame
    this.#properties.forEach(prop => {
      const input = document.getElementById(`game-edit-${prop}`)
      input.value = newGame[prop]
    })
    document.getElementById('game-edit-icon').value = this.#game.icon ? this.#game.icon : ''
  }
}

customElements.define('game-edit-info', GameEditInfo)
customElements.define('game-info', GameInfo)
customElements.define('game-params', GameParams)
customElements.define('game-edit-settings', GameEditSettings)

