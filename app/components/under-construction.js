/**
 * Message that the site is currently under development
 */
class UnderConstruction extends HTMLElement {
  connectedCallback() {
    const text = document.createElement('div')
    text.setAttribute('data-i18n', 'UnderConstruction.text')
    const img = document.createElement('img')
    img.src = `../assets/icons/construction.svg`
    img.style = 'width: 3rem; vertical-align: middle;'
    this.classList.add('alert-primary', 'text-center', 'd-block')
    this.append(text, img)
  }
}

// Register element
customElements.define('under-construction', UnderConstruction)