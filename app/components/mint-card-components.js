import config from '../config.js'
import { ref } from '../modules/utils.js'
import * as color from '../modules/color.js'
import { watch as watchTheme } from '../modules/themes.js'
import { watch as watchLocale } from '../modules/i18n.js'
import { watch as watchFontsize } from '../modules/fontsizes.js'
import { emit } from '../modules/utils.js'
import { HTMLMintElement } from './abstract-components.js'
import { translateElement } from '../modules/i18n.js'

/**
 * Container for the MintApps cards, usually displayed in two columns, on small screens also in one column
 * @property {Number} 'data-num-cols' - initial number of columns
 */
class MintCardContainer extends HTMLMintElement {
  static observedAttributes = ['data-num-cols', 'data-max-cols']

  /** current number of columns */
  #numCols = new ref(Math.min(config.columnLimit, 2))
  /** maximum number of columns */
  #maxCols = new ref(Math.min(config.columnLimit, 2))
  /** current width in px */
  #width = 0
  /** column containers */
  #colContainers = null
  /** list of cards */
  #cards = []
  /** listener to repaint events */
  #onrepaint = null
  /** locale already loaded, used to buffer initial repaint events */
  #localeLoaded = false

  constructor() {
    super()
    // watch number of columns
    this.#numCols.watch(() => {
      if (this.#numCols.value === 1) this.classList.add('mint-card-container-wide')
      else this.classList.remove('mint-card-container-wide')
      this.repaint()
    })
    // watch size of container
    const resizeObserver = new ResizeObserver(() => this.resize())
    resizeObserver.observe(this)
    // watch theme, locale and fontSize
    watchLocale(() => { this.#localeLoaded = true; this.repaint() })
    watchTheme(() => this.repaint())
    watchFontsize(() => this.repaint())
  }

  connectedCallback() {
    if (this.disconnected) return
    this.classList.add('mint-card-container')
    if (this.dataset.numCols === "1") {
      this.classList.add('mint-card-container-wide')
      this.#numCols.value = 1
    }
    // create two columns
    this.#colContainers = [document.createElement('div'), document.createElement('div')]
    //this.#colContainers.forEach(div => div.style.width="100%")
    this.append(...this.#colContainers)
    // get cards
    // TODO find better solution than complete list (css class .mint-card can't be used here, as not already set)
    this.#cards = this.querySelectorAll('mint-card, game-edit-info, game-edit-settings, game-params, game-info, local-load, qr-code-card, video-recorder, sync-card')
    this.renderColumns()
  }

  attributeChangedCallback(attr) {
    if (attr === 'data-num-cols') {
      this.#numCols.value = Math.min(config.columnLimit, this.#maxCols, this.dataset.numCols)
    }
    else if (attr === 'data-max-cols') {
      this.#maxCols.value = Math.min(config.columnLimit, this.dataset.maxCols)
      this.#numCols.value = Math.min(this.#numCols.value, this.#maxCols.value)
    }
  }

  /**
   * React to resize events and emit repaint if width has changed
   */
  resize() {
    if (this.#width !== this.clientWidth) {
      this.#width = this.clientWidth
      this.repaint()
    }
  }

  /**
   * Inform all cards about repaint event and emit repaint event
   * Might be triggered by size or theme change
   */
  repaint() {
    if (!this.#localeLoaded) return // don't repaint, when still loading locale files as this process will trigger a repaint event
    if (this.#onrepaint) this.#onrepaint()
    emit(this, 'repaint')
    this.#cards.forEach(card => {
      if (card.repaint) card.repaint()
    })
  }

  /**
   * Internal: Sort the individual cards into the two columns
   */
  renderColumns() {
    // get all cards and append to columns
    let index = 0
    let cardsPerCol = Math.ceil(this.#cards.length / 2)
    this.#cards.forEach(card => {
      // inform card about container
      card.container = this
      // determine col index (fill first column, then second column)
      const colIndex = Math.floor(index / cardsPerCol)
      this.#colContainers[colIndex].append(card)
      index++
    })
  }

  /**
   * Get maximum number of columns
   * @returns {Number}
  */
  get maxCols() {
    return this.#maxCols
  }

  /**
   * Get current number of columns
   * @returns {Number}
   */
  get numCols() {
    return this.#numCols
  }

  /**
   * Set listener to repaint events
   * @param {Function} listener
   */
  set onrepaint(listener) {
    this.#onrepaint = listener
  }
}

/**
 * Single card of the MintApps
 * @property {String} data-title - title of card
 * @property {String} [data-color] - backgroundcolor of card title
 */
export class MintCard extends HTMLMintElement {
  static observedAttributes = ['data-title']

  body = null // reference to body
  title = null // reference to title
  footer = null // reference to footer
  #hidden = false // wether card is hidden
  #numCols // reference to mintapp-container.numCols
  #maxCols // reference to mintapp-container.maxCols
  #container // reference to mintapp-container

  connectedCallback() {
    if (this.disconnected) return
    this.classList.add('mint-card')
    // get container
    this.#container = this.parentElement.parentElement
    this.#numCols = this.#container.numCols
    this.#maxCols = this.#container.maxCols
    // header - title
    this.title = document.createElement('span')
    this.title.setAttribute('data-i18n-md-inline', this.dataset.title ?? '')
    // header → icons
    const iconContainer = document.createElement('span')
    iconContainer.classList.add('mint-card-icon-container')
    // header → icons → expand
    let img = document.createElement('img')
    img.classList.add('mint-card-icon', 'mint-card-expand-icon', 'enter-click')
    img.src = `../assets/icons/expand-width.svg`
    img.role = 'button'
    img.setAttribute('data-i18n-alt', 'MintCard.toggle-expand')
    img.setAttribute('data-i18n-title', 'MintCard.toggle-expand')
    img.onclick = () => this.toggleExpanded()
    iconContainer.append(img)
    // header → icons → hide
    img = document.createElement('img')
    img.classList.add('mint-card-icon', 'mint-card-icon', 'enter-click')
    img.src = `../assets/icons/expand-height.svg`
    img.role = 'button'
    img.setAttribute('data-i18n-alt', 'MintCard.toggle-hidden')
    img.setAttribute('data-i18n-title', 'MintCard.toggle-hidden')
    img.onclick = () => this.toggleHidden()
    iconContainer.append(img)
    // header
    const header = document.createElement('div')
    header.classList.add('mint-card-header')
    if (this.dataset.color) header.style = this.getColorStyle(this.dataset.color)
    header.append(this.title, iconContainer)
    // body
    this.body = document.createElement('div')
    this.body.classList.add('mint-card-body')
    this.body.append(...this.children)
    this.append(header, this.body)
    // footer
    const footerSlot = this.querySelector('[slot=footer]')
    this.footer = document.createElement('div')
    this.footer.classList.add('mint-card-footer')
    if (footerSlot) this.footer.append(...footerSlot.children)
    else this.footer.setAttribute('data-hide', '')
    this.append(this.footer)
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'data-title' && this.title) {
      this.title.setAttribute('data-i18n-md-inline', newValue)
      translateElement(this.title)
    }
  }

  /**
   * Toggle visibility of card
   */
  toggleHidden() {
    this.#hidden = !this.#hidden
    if (this.#hidden) this.classList.add('mint-card-hidden')
    else this.classList.remove('mint-card-hidden')
  }

  /**
   * Change the number of columns in the container (affects all cards)
   */
  toggleExpanded() {
    this.#numCols.value = this.#numCols.value + 1 > this.#maxCols.value ? 1 : this.#numCols.value + 1
  }

  /**
   * Internal: Create style information for color and backgroundcolor
   * @param {String} backgroundColor 
   * @returns {String} css style information
   */
  getColorStyle(backgroundColor) {
    let style = `background-color:${backgroundColor};color:`
    if (color.isDarkColor(backgroundColor)) style += 'white'
    else style += 'black'
    return style
  }

  /**
   * Specify the container in which the card is located
   */
  set container(mintAppContainer) {
    this.#numCols = mintAppContainer.numCols // reactive value
    this.#maxCols = mintAppContainer.maxCols // reactive value
  }

  /**
   * React to repaint events
   */
  repaint() {
    if (this.clientWidth < 400) this.classList.add('mint-card-narrow')
    else this.classList.remove('mint-card-narrow')
  }
}

// Register elements
customElements.define('mint-card-container', MintCardContainer)
customElements.define('mint-card', MintCard)
