import config from '../config.js'
import { translateChildren } from '../modules/i18n.js'
import { importCss } from '../modules/utils.js'
importCss('navbars.css')

/**
 * Bottom navigation bar, including submenus for settings, actions and users
 */
class NavbarBottom extends HTMLElement {
  connectedCallback() {
    if (this.disconnected) return
    this.renderBottomPanel()
  }

  /**
   * Generate bottom navbar panel
   */
  renderBottomPanel () {
    // Version info
    const entries = []
    const span = document.createElement('span')
    span.classList.add('hide-tiny')
    span.innerText = `${config.name} ${config.version}`
    entries.push(span)
    // About and privacy info
    for (const key of ['about', 'privacy']) {
      const url = config[`${key}Url`]
      if (url) {
        const a = document.createElement('a')
        a.href = url
        a.setAttribute('data-i18n', `Menus.${key}`)
        const span = document.createElement('span')
        span.append(a)
        entries.push(span)
      }
    }
    this.classList.add('nav-bottom')
    this.append(...entries)
    translateChildren(this)
  }
}

customElements.define('navbar-bottom', NavbarBottom)
