import * as plot2d from '../modules/plot-2d.js'
import { HTMLMintElement } from './abstract-components.js'
import './graph-components.js'
import { watch as watchTheme } from '../modules/themes.js'
import { watch as watchLocale } from '../modules/i18n.js'
/**
 * Element that represents a simulated scope with one to four inputs
 * @property {Number} [data-ratio=0.5] - aspect ratio y:x
 * @property {Boolean} [data-auto-scale=false] - wether to autoscale axis
 * @property {Boolean} [data-show-point=false] - wether to display the coordinates on a click or touch event
 */
class CanvasScope extends HTMLMintElement {
  #div = null // main div (container for elements)
  #graphLegend = null // reference for graph legend
  #graphSettings = null // reference for graph settings dialog
  #graphPoint = null // reference for point info (after click on canvas)
  #canvas = null // reference to canvas
  #width = 0 // width of canvas in px
  #height = 0 // height of canvas in px
  #coords = {} // coordinate system definition
  #legend = [] // entries of legend
  #resizeObserver = null // watch for resize events
  #point = {} // user selected point
  #historySize = 0 // number of values in history
  #historyLength = 0 // displayed time interval (x.max - x.min of coords)
  #historyPos = 0 // current position in history
  #histories = [] // array of histories (for all connected inputs)
  #inputs = [] // definition of connected inputs
  #dt = 0.1 // time step between to input signals
  #timeOffset = 0 // time offset (when scrolling)
  #fs = 15 // font-size in px


  connectedCallback() {
    if (this.disconnected) return
    // Graph settings
    this.#graphSettings = document.createElement('graph-settings')
    this.#graphSettings.setAttribute('data-fixed-border', '')
    this.#graphSettings.onchange = () => this.settingsChanged()
    // Graph legend
    this.#graphLegend = document.createElement('graph-legend')
    // Graph point
    this.#graphPoint = document.createElement('graph-point')
    // root
    this.#div = document.createElement('div')
    this.#div.append(this.#graphSettings, this.#graphLegend, this.#graphPoint)
    // watch for resize events and theme changes
    this.#resizeObserver= new ResizeObserver(() => this.resize())
    this.#resizeObserver.observe(this.#div)
    watchTheme(() => this.repaint())
    watchLocale(() => this.repaint())
    // initialize
    plot2d.init()
    this.fs = plot2d.getFontSizeText()
    this.append(this.#div)
    this.classList.add('d-block')
    this.repaint()
  }

  /**
   * Set coordinate system
   * @param {object} newCoords - see ../modules/plot-2d.md#module_plot-2d.drawCoordinates
   * @public
   */
  set coords(newCoords) {
    newCoords.border = 1
    this.#graphSettings.coords = newCoords
    this.#coords = this.#graphSettings.coords
  }

  /**
   * Get coordinate system
   * @returns {object} see ../modules/plot-2d.md#module_plot-2d.drawCoordinates
   * @public
   */
  get coords() {
    return this.#coords
  }

 /**
  * Define inputs of scope
  * @param {Object[]} inputs - Definition of inputs
  */
 set inputs (inputs) {
  if (!this.#coords.x) throw new Error('must set coords first')
  this.#inputs = inputs
  this.#legend = []
  this.#inputs.forEach(input => {
    if (input.math || input.text) {
      this.#legend.push(input)
      this.#graphLegend.legend = this.#legend
      this.#graphLegend.showIf(this.#coords.legend && this.#legend.length >0)
    }
  })
  this.initHistories()
  this.plot()
}

/**
 * Set time step between to input signals
 * @param {Number} ts - time step in seconds
 */
 set timeStep (dt) {
  this.#dt = dt
  this.clear()
 }

  /**
  * Create a canvas object on which the different elements of the scope will be displayed.
  */
  createCanvas() {
    const ratio = this.dataset.ratio ?? 0.5
    const showPoint = this.hasAttribute('data-show-point')
    this.#width = this.#div.clientWidth
    this.#height = this.#width * ratio
    this.#div.style.height = this.#height + 'px'
    this.#canvas = plot2d.createCanvas(this.#div, this.#width, this.#height, 'scope', true)
    if (showPoint) {
      this.#canvas.classList.add('cursor-crosshair')
      this.#canvas.addEventListener('click', (e) => this.showCoordinates(e))
    }
    this.#graphSettings.canvas = this.#canvas
  }

  /**
   * initialize history of all input channels
   */
 initHistories () {
  this.#historyPos = 0
  this.#histories = []
  if (!this.#coords.x) return
  this.#historyLength = this.#coords.x.max - this.#coords.x.min // displayed time interval
  this.#historySize = Math.round(this.#historyLength / this.#dt) // number of values in history
  this.#inputs.forEach(() => this.#histories.push(new Array(this.#historySize)))
}


/**
 * clear values
 */
clear () {
  this.#timeOffset = 0
  this.initHistories()
  this.#point = {}
  this.plot()
}

/**
 * proceed one time step and add value for each input channel
 * @param {Number[]} - Array of values for each input
 */
addValues (values) {
  let shiftCoords = false
  for (let i = 0; i < this.#inputs.length; i++) {
    this.#histories[i][this.#historyPos] = values[i]
    if (this.#historyPos + 1 === this.#historySize) {
      this.#histories[i] = this.#histories[i].slice(1)
      this.#histories[this.#historySize - 1] = NaN
      shiftCoords = true
    }
    // autoscale
    if (this.hasAttribute('data-auto-scale')) {
      let rescaled = false
      while (values[i] > this.#coords.y.max && this.#coords.y.max > 0) {
        this.#coords.y.max *= 1.1
        this.#coords.y.step = null
        rescaled = true
      }

      while (values[i] < this.#coords.y.min && this.#coords.y.min < 0) {
        this.#coords.y.min *= 1.1
        this.#coords.y.step = null
        rescaled = true
      }
      if (rescaled) this.#graphSettings.coords = this.#coords
    }
  }
  if (++this.#historyPos === this.#historySize) this.#historyPos--
  if (shiftCoords) this.#timeOffset += this.#dt
  this.plot()
}


// plot everything (coords and lines for connected inputs)
 plot () {
  if (!this.#coords.x || !this.#coords.y) return
  this.#coords.width = this.#width
  this.#coords.height = this.#height
  const ctx = this.#canvas.getContext('2d')
  plot2d.clearCanvas(this.#canvas)
  // draw coordinate system (taking into account timeOffset when scrolling)
  plot2d.drawCoordinates(ctx, {
    y: this.#coords.y,
    x: {
      min: this.#coords.x.min + this.#timeOffset,
      max: this.#coords.x.max + this.#timeOffset,
      text: this.#coords.x.text,
      grid: this.#coords.x.grid
    },
    width: this.#coords.width,
    height: this.#coords.height,
    border: this.#coords.border
  })
  // set extra margin when necessary
  if (this.#height - plot2d.calcPixelY(this.#coords, 0) < this.#fs ) this.#div.style.paddingBottom = '1rem'
  else this.#div.style.paddingBottom = ''
  // draw lines for connected inputs
  for (let i = 0; i < this.#inputs.length; i++) {
    plot2d.setStyle(ctx, this.#inputs[i])
    ctx.save()
    const scaleX = plot2d.getScaleX(this.#coords)
    const scaleY = plot2d.getScaleY(this.#coords)
    const offsetX = plot2d.calcPixelX(this.#coords, 0)
    const offsetY = plot2d.calcPixelY(this.#coords, 0)
    ctx.translate(offsetX, offsetY)
    ctx.scale(scaleX, scaleY)
    ctx.beginPath()
    const values = this.#histories[i]
    ctx.moveTo(this.#coords.x.min, values[0])
    let x = this.#coords.x.min
    for (let k = 1; k < this.#historySize; k++) {
      x += this.#historyLength / this.#historySize
      ctx.lineTo(x, values[k])
    }
    ctx.restore()
    ctx.stroke()
  }
    // point
    if (this.#point.show) {
      this.#point.pixelX = plot2d.calcPixelX(this.#coords, this.#point.x)
      this.#point.pixelY = plot2d.calcPixelY(this.#coords, this.#point.y)
      plot2d.drawCrossHairCoords(ctx, this.#coords, {
        x: this.#point.x,
        y: this.#point.y,
        lineWidth: 1
      })
    }

}

  /**
   * Get the canvas element
   * @returns {HTMLCanvasObject}
   * @public
   */
  getCanvas() {
    return this.#canvas
  }

  /**
   * The graph settings have been changed by the user, triggered from component #graphSettings
   * @see #graphSettings.md
   */
  settingsChanged() {
    this.#coords = this.#graphSettings.coords
    this.#graphLegend.showIf(this.#coords.legend)
    this.plot()
  }

  /**
   * Read coordinates from an user event and display them in a small modal dialog
   * @param {event} e - mouse or touch event
   */
  showCoordinates(e) {
    const pos = plot2d.getEventPosition(e, this.#canvas)
    this.#graphPoint.point = {
      show: true,
      x: plot2d.calcX(this.#coords, pos.x) + this.#timeOffset,
      y: plot2d.calcY(this.#coords, pos.y),
      pixelX: pos.x,
      pixelY: pos.y
    }
    this.plot()
  }

  /**
   * Clear the canvas and redraw all elements
   * @public
   */
  repaint() {
    if (this.#canvas) this.#canvas.remove()
    this.createCanvas()
    plot2d.updateColors()
    this.plot()
  }

  /**
   * Listen to resize events and repaint if necessary
   */
  resize() {
    if (this.parentElement.clientWidth === 0) return // css not loaded
    if (this.#div.clientWidth == this.#width) return // width not changed
    this.repaint()
  }

}

// Register element
customElements.define('canvas-scope', CanvasScope)