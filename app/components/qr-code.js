import { getColors } from '../modules/color.js'
import '../assets/libs/qr-creator.es6.min.js'
import { HTMLMintElement } from './abstract-components.js'

/**
 * Display text as QR codes, the design can be customized via attributes
 * @property {String} data-text - text
 * @property {Number} [data-size=256] - size in px
 * @property {Number} [data-radius=0] - corner radius (0 ... 1)
 * @property {String} [color=var(--text-color)] - color (valid html definition)
 */
class QrCode extends HTMLMintElement {
  #text = '' // text
  #size = 256 // size in px
  #radius = 0.1 // corner radius
  #color = null // color
  
  static observedAttributes = ['data-text', 'data-size', 'data-radius', 'data-color']

  connectedCallback() {
    this.classList.add('d-inline')
  }

   
  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-text': this.#text = newValue; break
      case 'data-size': this.#size = newValue; break
      case 'data-radius': this.#radius = newValue; break
      case 'data-color': this.#color = newValue; break
    }
    if (this.#text) this.render()
  }

  /**
   * Render QR code
   */
  render() {
    const colors = getColors()
    const params = { text: this.#text, radius: this.#radius, ecLevel: 'H', fill: this.#color ?? colors[4], background: null, size: this.#size }
    this.innerHTML = ''
    try {
      // eslint-disable-next-line no-undef
      QrCreator.render(params, this)
    } catch {
      console.debug('QrCode: invalid values')
    }
  }
}

// Register element
customElements.define('qr-code', QrCode)