import * as plot2d from '../modules/plot-2d.js'
import { HTMLMintElement } from './abstract-components.js'
import './graph-components.js'
import { watch as watchTheme } from '../modules/themes.js'
import { watch as watchLocale } from '../modules/i18n.js'
/**
 * Element that represents a complete mathematical graph with a coordinate system and various elements (such as functions, points, lines, etc.).
 * @property {Number} [data-ratio=0.5] - aspect ratio y:x
 * @property {Boolean} [data-auto-scale=false] - wether to autoscale axis
 * @property {Boolean} [data-show-point=false] - wether to display the coordinates on a click or touch event
 */
class CanvasGraph extends HTMLMintElement {
  static observedAttributes = ['data-name']

  #div = null // main div (container for elements)
  #graphLegend = null // reference for graph legend
  #graphSettings = null // reference for graph settings dialog
  #graphPoint = null // reference for point info (after click on canvas)
  #canvas = null // reference to canvas
  #width = 0 // width of canvas in px
  #height = 0 // height of canvas in px
  #functions = [] // array of functions
  #points = [] // array of points
  #lines = [] // array of lines
  #vectors = [] // array of vectors
  #traces = [] // array of traces
  #circles = [] // array of circles
  #histograms = [] // array of histogramms
  #coords = {} // coordinate system definition
  #legend = [] // entries of legend
  #resizeObserver = null // watch for resize events
  #point = {} // user selected point
  #fs = 15 // font-size in px

  connectedCallback() {
    if (this.disconnected) return
    // Graph settings
    this.#graphSettings = document.createElement('graph-settings')
    this.#graphSettings.onchange = () => this.settingsChanged()
    // Graph legend
    this.#graphLegend = document.createElement('graph-legend')
    // Graph point
    this.#graphPoint = document.createElement('graph-point')
    // root
    this.#div = document.createElement('div')
    this.#div.append(this.#graphSettings, this.#graphLegend, this.#graphPoint)
    // initialize
    plot2d.init()
    this.append(this.#div)
    this.classList.add('d-block')
    this.resize() // depends on timing (css load)
    // watch for resize events and theme changes
    this.#resizeObserver= new ResizeObserver(() => this.resize())
    this.#resizeObserver.observe(this.#div)
    watchTheme(() => this.repaint())
    watchLocale(() => this.repaint())
  }

  /**
  * Create a canvas object on which the different elements of the graph will be displayed.
  */
  createCanvas() {
    const ratio = this.dataset.ratio ?? 0.5
    const showPoint = this.hasAttribute('data-show-point')
    this.#width = this.#div.clientWidth
    this.#height = this.#width * ratio
    this.#div.style.height = this.#height + 'px'
    this.#canvas = plot2d.createCanvas(this.#div, this.#width, this.#height, 'graph', true)
    if (showPoint) {
      this.#canvas.classList.add('cursor-crosshair')
      this.#canvas.addEventListener('click', (e) => this.showCoordinates(e))
    }
    this.#graphSettings.canvas = this.#canvas
  }

  /**
   * Set coordinate system
   * @param {object} newCoords - see ../modules/plot-2d.md#module_plot-2d.drawCoordinates
   * @public
   */
  set coords(newCoords) {
    this.#graphSettings.coords = newCoords
    this.#coords = this.#graphSettings.coords
  }

  /**
   * Get coordinate system
   * @returns {object} see ../modules/plot-2d.md#module_plot-2d.drawCoordinates
   * @public
   */
  get coords() {
    return this.#coords
  }

  /**
   Add new empty trace (without points to graph)
   @param {object} props - configuraiton,  see ../modules/plot-2d.md#module_plot-2d.drawTraceCoords
   @public
   */
  addTrace(props) {
    const trace = { ...props, points: [] }
    this.#traces.push(trace)
    this.addToLegend(props)
  }

  /**
   Add point to a specific trace
   @param {number} index - Index of the trace (according to the order in which the traces were added)
   @param {object} point - coordinates {x, y}
  */
  addToTrace(index, point) {
    if (!this.#traces[index]) return
    const trace = this.#traces[index]
    trace.points.push(point)
    if (this.autoscaleAxis(point.x, point.y)) this.plot()
    const traceLength = trace.points.length
    if (traceLength > 1) {
      const ctx = this.#canvas.getContext('2d')
      const lastPoint = trace.points[traceLength - 2]
      const line = {
        x0: lastPoint.x,
        y0: lastPoint.y,
        x1: point.x,
        y1: point.y,
        color: trace.color,
        lineWidth: trace.lineWidth,
        lineStyle: trace.lineStyle
      }
      plot2d.drawLineCoords(ctx, this.#coords, line)
    }
  }

  /**
   * Add function, the color is set automatically
   * @param {function} f - Mathematical function, e.g. x => x+3
   * @public
   */
  addFunction(f) {
    this.addToLegend(f)
    this.#functions.push(f)
    if (this.hasAttribute('data-auto-scale')) {
      const xmin = this.#coords.x.min
      const xmax = this.#coords.x.max
      const xstep = (xmax - xmin) / 100
      for (let x = xmin; x <= xmax; x += xstep) {
        this.autoscaleAxis(x, f.f(x))
      }
    }
  }

  /**
   * Add a histogram
   * @param {object[]} h.values - configuration of the bars in the histogram, each entry is an object of the form  {x, y, width, height, color, fillColor }
   * @public
   */
  addHistogram(h) {
    this.#histograms.push(h)
  }

  /**
   * Add a point
   * @param {object} point - see ../modules/plot-2d.md#module_plot-2d.drawPointCoords
   * @public
   */
  addPoint(point) {
    this.addToLegend(point)
    this.#points.push(point)
  }

  /**
   * Add a line
   * @param {object} line - see ../modules/plot-2d.md#module_plot-2d.drawLineCoords
   * @public
   */
  addLine(line) {
    this.addToLegend(line)
    this.#lines.push(line)
    this.autoscaleAxis(line.x0, line.y0)
    this.autoscaleAxis(line.x1, line.y1)
  }

  /**
   * Add a circle
   * @param {object} circle - see ../modules/plot-2d.md#module_plot-2d.drawDiscCoords
   * @public
   */
  addCircle(circle) {
    this.#circles.push(circle)
  }

  /**
   * Add a vector
   * @param {object} vector - see ../modules/plot-2d.md#module_plot-2d.drawArrowCoords
   * @public
   */
  addVector(vector) {
    this.addToLegend(vector)
    this.#vectors.push(vector)
    this.autoscaleAxis(vector.x0, vector.y0)
    this.autoscaleAxis(vector.x1, vector.y1)
  }

  /**
   * Get coords from pixel position (to eval mouse clicks)
   * @param {number} pixelX - x coordinate in px
   * @param {number} pixelY - y coordinate in px
   * @returns {object} Object {x, y} in relative coordinates
   */
  getPoint(pixelX, pixelY) {
    return {
      x: plot2d.calcX(this.#coords, pixelX),
      y: plot2d.calcY(this.#coords, pixelY)
    }
  }

  /**
   * Draw all elements of the graphic (necessary if they were only added)
   * @public
   */
  plot() {
    // skip, when coords undefined
    if (!this.#canvas || !this.#coords || !this.#coords.x || !this.#coords.y) return
    this.#coords.width = this.#width
    this.#coords.height = this.#height
    const ctx = this.#canvas.getContext('2d')
    ctx.clearRect(0, 0, this.#width, this.#height)
    // coordinate system
    plot2d.drawCoordinates(ctx, this.#coords)
    if (this.#height - plot2d.calcPixelY(this.#coords, 0) < this.#fs ) this.#div.style.paddingBottom = '1rem'
    else this.#div.style.paddingBottom = ''
    // histograms
    for (const entry of this.#histograms) {
      for (const value of entry.values) {
        plot2d.drawRectCoords(ctx, this.#coords, {
          x: value.x - 0.5 * entry.width,
          y: 0,
          width: entry.width,
          height: value.y,
          color: value.color,
          fillColor: value.fillColor
        })
      }
    }
    // functions
    for (const func of this.#functions) {
      plot2d.drawFunction(ctx, this.#coords, func)
    }
    // points
    for (const point of this.#points) {
      plot2d.drawPointCoords(ctx, this.#coords, point)
    }
    // lines
    for (const line of this.#lines) {
      plot2d.drawLineCoords(ctx, this.#coords, line)
    }
    // vectors
    for (const vector of this.#vectors) {
      plot2d.drawArrowCoords(ctx, this.#coords, vector)
    }
    // traces
    for (const trace of this.#traces) {
      plot2d.drawTraceCoords(ctx, this.#coords, trace)
    }
    // cricles
    for (const circle of this.#circles) {
      plot2d.drawDiscCoords(ctx, this.#coords, circle)
    }
    // point
    if (this.#point.show) {
      this.#point.pixelX = plot2d.calcPixelX(this.#coords, this.#point.x)
      this.#point.pixelY = plot2d.calcPixelY(this.#coords, this.#point.y)
      plot2d.drawCrossHairCoords(ctx, this.#coords, {
        x: this.#point.x,
        y: this.#point.y,
        lineWidth: 1
      })
    }
  }

  /**
   * Remove all elements from the graph but keep settings (usefull for redrawing same graph)
   * @public
   */
  clear() {
    this.#functions = []
    this.#points = []
    this.#vectors = []
    this.#traces = []
    this.#points = []
    this.#circles = []
    this.#histograms = []
    this.#legend = []
    this.#lines = []
    this.#graphLegend.legend = this.#legend
  }

  /**
   * Automatically scale the axes so that the specified point can be displayed
   * @param {number} x - x coordinate
   * @param {number} y - y coordinate
   */
  autoscaleAxis(x, y) {
    let rescaled = false
    if (this.hasAttribute('data-auto-scale')) {
      if (x > this.#coords.x.max) {
        if (x > 0) this.#coords.x.max = 1.1 * x
        else this.#coords.x.max = 0.9 * x
        this.#coords.x.step = null
        rescaled = true
      }
      if (x < this.#coords.x.min) {
        if (x < 0) this.#coords.x.min = 1.1 * x
        else this.#coords.x.min = 0.9 * x
        this.#coords.x.step = null
        rescaled = true
      }
      if (y > this.#coords.y.max) {
        if (y > 0) this.#coords.y.max = 1.1 * y
        else this.#coords.y.max = 0.9 * y
        this.#coords.y.step = null
        rescaled = true
      }
      if (y < this.#coords.y.min) {
        if (y < 0) this.#coords.y.min = 1.1 * y
        else this.#coords.y.min = 0.9 * y
        this.#coords.y.step = null
        rescaled = true
      }
      if (rescaled) this.#graphSettings.coords = this.#coords
    }
    return rescaled
  }

  /**
   * Get the canvas element
   * @returns {HTMLCanvasObject}
   * @public
   */
  getCanvas() {
    return this.#canvas
  }

  /**
   * The graph settings have been changed by the user, triggered from component #graphSettings
   * @see #graphSettings.md
   */
  settingsChanged() {
    this.#coords = this.#graphSettings.coords
    this.#graphLegend.showIf(this.#coords.legend && this.#legend.length > 0)
    this.plot()
  }

  /**
   * Read coordinates from an user event and display them in a small modal dialog
   * @param {event} e - mouse or touch event
   */
  showCoordinates(e) {
    const pos = plot2d.getEventPosition(e, this.#canvas)
    this.#graphPoint.point = {
      show: true,
      x: this.#coords.x.grid >= 0 ? plot2d.calcX(this.#coords, pos.x) : NaN,
      y: this.#coords.y.grid >= 0 ? plot2d.calcY(this.#coords, pos.y) : NaN,
      pixelX: pos.x,
      pixelY: pos.y
    }
    this.plot()
  }

  /**
   * Clear the canvas and redraw all elements
   * @public
   */
  repaint() {
    if (this.#canvas) this.#canvas.remove()
    this.createCanvas()
    plot2d.updateColors()
    this.plot()
  }

  /**
   * Listen to resize events and repaint if necessary
   */
  resize() {
    if (this.parentElement.clientWidth === 0) return // css not loaded
    if (this.#div.clientWidth == this.#width) return // width not changed
    this.repaint()
  }

  /**
   * Add the properties of an element to the legend
   * @param {object} element - drawing object, such as line, circle, etc.
   */
  addToLegend(element) {
    if (element.text || element.math) {
      this.#legend.push(element)
      this.#graphLegend.legend = this.#legend
      this.#graphLegend.showIf(this.#coords.legend && this.#legend.length >0)
    }
  }
}

// Register element
customElements.define('canvas-graph', CanvasGraph)