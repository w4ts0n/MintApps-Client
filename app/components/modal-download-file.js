import { ModalDialog } from './modal-dialog.js'
import { translateChildren } from '../modules/i18n.js'
import { downloadImage, downloadCsv, downloadText, downloadVcard, downloadJson, downloadCanvas } from '../modules/download.js'
import './form-components.js'
import './alert-box.js'
import './wait-icon.js'
import * as http from '../modules/http.js'
import { uid, showIf, hide, show } from '../modules/utils.js'
import * as storage from '../modules/storage.js'
import * as session from '../modules/session.js'

/**
 * Modal dialog for saving files (as local download or server upload)
 * @see ModalDialog
 */
class ModalDownloadFile extends ModalDialog {
  #alert = null // reference to alert box
  #info1 = null // reference to info text 1
  #info2 = null // reference to info text 2
  #input = null // reference to input field (for file name)
  #wait = null // reference to wait icon
  #btnClose = null // reference to close button
  #btnDownload = null // reference to download button
  #btnOverwrite = null // reference to upload button
  #btnUpload = null // reference to upload (with overwrite) button
  #params = {} // download parameters

  connectedCallback() {
    if (this.disconnected) return
    super.connectedCallback()
    // set title
    this.title.setAttribute('data-i18n', 'General.btn-save')
    // info texts
    this.#info1 = document.createElement('alert-box')
    this.#info1.setAttribute('data-text', 'ModalDownloadFile.info1')
    this.#info2 = document.createElement('alert-box')
    this.#info2.setAttribute('data-text', 'ModalDownloadFile.info2')
    this.#info2.classList.add('scorm-hide')
    // alert box
    this.#alert = document.createElement('alert-box')
    this.#alert.setAttribute('data-hide', true)
    // form
    const form = document.createElement('form')
    form.classList.add('form-container')
    this.#input = document.createElement('input-text')
    this.#input.setAttribute('data-min', 1)
    this.#input.setAttribute('data-max', 100)
    this.#input.id = uid()
    const inputLabel = document.createElement('mint-label')
    inputLabel.setAttribute('data-text', 'ModalDownloadFile.file-name')
    inputLabel.setAttribute('for', this.#input.id)
    form.append(inputLabel, this.#input)
    // wait icon
    this.#wait = document.createElement('wait-icon')
    this.#wait.hide()
    // body
    this.body.append(this.#info1, this.#info2, this.#alert, this.#wait, form)
    // set footer
    this.#btnDownload = document.createElement('button')
    this.#btnDownload.classList.add('btn')
    this.#btnDownload.onclick = () => this.download()
    this.#btnDownload.setAttribute('data-i18n', 'ModalDownloadFile.btn-local')
    this.#btnUpload = document.createElement('button')
    this.#btnUpload.classList.add('btn')
    this.#btnUpload.onclick = () => this.upload(false)
    this.#btnUpload.setAttribute('data-i18n', 'ModalDownloadFile.btn-server')
    this.#btnUpload.setAttribute('data-hide', '')
    this.#btnOverwrite = document.createElement('button')
    this.#btnOverwrite.classList.add('btn')
    this.#btnOverwrite.onclick = () => this.upload(true)
    this.#btnOverwrite.setAttribute('data-i18n', 'ModalDownloadFile.btn-server-overwrite')
    this.#btnClose = document.createElement('button')
    this.#btnClose.classList.add('btn', 'btn-secondary')
    this.#btnClose.onclick = () => this.emitClose()
    this.#btnClose.setAttribute('data-i18n', 'General.btn-cancel')
    this.footer.append(this.#btnDownload, this.#btnUpload, this.#btnOverwrite, this.#btnClose)
    this.footer.removeAttribute('data-hide')
    // translate
    translateChildren(this)
  }

  /**
   * Open download dialog
   * @param {Object} downloadParams - parameter object
   * @param {String} [downloadParams.name] - default file name (if empty use document title)
   * @param {String} downloadParams.type - type of download (canvas, image, csv, json, text)
   * @param {Object} downloadParams.data - data source (for example canvas, text, etc.)
   */
  open(downloadParams) {
    if (!downloadParams.name) downloadParams.name = document.title
    this.#params = downloadParams
    this.#input.value = this.#params.name
    this.#alert.hide()
    hide(this.#btnOverwrite)
    this.hasValidationErrors()
    this.removeAttribute('data-hide')
    // offer upload for json files and authentificated user
    if (this.#params.type === 'json') {
      this.#info2.show()
      session.isValidSession().then(s => {
        showIf(this.#btnUpload, !!s)
      })
    } else {
      this.#info2.hide()
    }
  }

  /**
   * Get number of validation errors and display error message
   * @return {Boolean} true on errors
   */
  hasValidationErrors() {
    const numValidationErrors = this.getElementsByClassName('invalid').length
    if (numValidationErrors) {
      this.#alert.setAttribute('data-text', 'General.warning-validate')
      this.#alert.setAttribute('data-type', 'warning')
      this.#alert.removeAttribute('data-hide')
    }
    return numValidationErrors > 0
  }

  /**
   * Start local download
   */
  download() {
    if (this.hasValidationErrors()) return
    if (!this.#params || !this.#params.type) return
    if (this.#params.type === 'canvas') { downloadCanvas(this.#input.value, this.#params.data) }
    if (this.#params.type === 'image') { downloadImage(this.#input.value, this.#params.data) }
    if (this.#params.type === 'csv') { downloadCsv(this.#input.value, this.#params.data) }
    if (this.#params.type === 'text') { downloadText(this.#input.value, this.#params.data) }
    if (this.#params.type === 'vcard') { downloadVcard(this.#input.value, this.#params.data) }
    if (this.#params.type === 'json') { downloadJson(this.#input.value, this.#params.data) }
    this.emitClose()
    this.dispatchEvent(new Event('save'))
  }

  /**
   * Upload file to server
   * @param {Boolean} overwrite - force overwrite on server
   */
  async upload(overwrite) {
    if (this.hasValidationErrors()) return
    try {
      this.#wait.show()
      const data = structuredClone(this.#params.data)
      data.title = this.#input.value
      const res = await http.post('sync:save-activity', {
        activity: JSON.stringify(data),
        overwrite,
        user: storage.get('session').user
      })
      this.#params.data.id = res.id
      this.emitClose()
      this.dispatchEvent(new Event('save'))
    } catch (e) {
      if (e.message === 'db-duplicate-entry') {
        show(this.#btnOverwrite)
        this.#alert.setAttribute('data-type', 'info')
        this.#alert.setAttribute('data-text', 'ModalDownloadFile.upload-duplicate')
      } else {
        if (e.message === 'session-invalid' && storage.get('session')?.user) storage.set('session', {})
        this.#alert.setAttribute('data-type', 'warning')
        this.#alert.setAttribute('data-text', `Errors.${e.message}`)
      }
      this.#alert.show()
    } finally {
      this.#wait.hide()
    }
  }


}

// Register element
customElements.define('modal-download-file', ModalDownloadFile)