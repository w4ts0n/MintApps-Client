import config from '../config.js'
import { getCurrentLocale, translateChildren, watch } from '../modules/i18n.js'
import { importCss } from '../modules/utils.js'
import { renderBlock } from '../modules/markdown.js'
import { HTMLMintElement } from './abstract-components.js'
importCss('text-block.css')
import './wait-icon.js'

const cache = {}

/**
 * Text block, dispalying one or several markdown files
 * @property {String} data-text - names of markdownfiles to be included, separated by comma
 * @property {Boolean} [data-container=false] - embed in div.container
 */
class TextBlock extends HTMLMintElement {
  static observedAttributes = ['data-hide']

  #waitIcon = null // reference to wait icon
  #container = null // text container

  connectedCallback () {
    if (this.disconnected) return
    // wait icon
    this.#waitIcon = document.createElement('wait-icon')
    if (this.hasAttribute('data-container')) this.#waitIcon.setAttribute('container', true)
    this.append(this.#waitIcon)
    // text container
    this.#container = document.createElement('div')
    this.#container.classList.add('text-block')
    if (this.hasAttribute('data-container')) this.#container.classList.add('container')
    this.append(this.#container)
    // watch for locale changes
    watch(() => this.render())
  }

  attributeChangedCallback() {
    if (!this.hasAttribute('data-hide')) this.render()
  }

  /**
   * Render contents, using markdown and asiimathml
   */
  async render() {
    if (this.hasAttribute('data-hide')) return
    if (!this.hasAttribute('data-text')) {
      console.debug('text-block: attribute "data-text" missing')
      return
    }
    this.#waitIcon.show()
    const locale = getCurrentLocale()
    let fileList = this.dataset.text.split(',').map(e => e.trim())
    // load markdown files
    const warnings = {}
    let completeText = ''
    for (const file of fileList) {
      let text
      const [prefix, id] = file.trim().split('/')
      if (!cache[prefix]) cache.prefix = {}
      // try current language
      text = (await this.loadLocale(prefix, locale))[id]
      // try fallback language
      if (!text) {
        text = (await this.loadLocale(prefix, config.fallbackLocale))[id]
        if (text) warnings.fallback = true
      }
      // try default language
      if (!text) {
        text = (await this.loadLocale(prefix, config.defaultLocale))[id]
        if (text) warnings.default = true
      }
      // put together
      if (text) {
        if (text) completeText += `\n${text}\n`
      } else {
        warnings.missing = true
      }
    }
    // display warnings and converted markdown
    this.#container.innerHTML = ''
    // warnings
    if (warnings.default || warnings.missing || warnings.fallback) {
      const p = document.createElement('p')
      p.setAttribute('data-i18n', 'TextBlock.translation-incomplete')
      p.setAttribute('data-locale', getCurrentLocale())
      const ul = document.createElement('ul')
      if (warnings.fallback) {
        const li = document.createElement('li')
        li.setAttribute('data-i18n', 'TextBlock.text-using-other-language')
        li.setAttribute('data-lang', config.fallbackLocale)
        ul.append(li)
      }
      if (warnings.default) {
        const li = document.createElement('li')
        li.setAttribute('data-i18n', 'TextBlock.text-using-other-language')
        li.setAttribute('data-lang', config.defaultLocale)
        ul.append(li)
      }
      if (warnings.missing) {
        const li = document.createElement('li')
        li.setAttribute('data-i18n', 'TextBlock.text-not-found')
        li.setAttribute('data-lang', config.defaultLocale)
        ul.append(li)
      }
      const div = document.createElement('div')
      div.classList.add('alert-primary')
      div.append(p, ul)
      this.#container.append(div)
    }
    // text container
    this.#container.innerHTML += renderBlock(completeText)
    translateChildren(this.#container)
    this.#waitIcon.hide()
  }

  /**
   * Load (and cache) markdown messages from server
   * @param {String} prefix - namespace (for examples 'infos')
   * @param {String} locale - language (two letter code)
   * @returns {Object} - empty when to available
   */
  async loadLocale(prefix, locale) {
    if (!cache[prefix]) cache[prefix] = {}
    if (!cache[prefix][locale]) {
      const response = await fetch(`${config.weblatePath}${prefix}-${locale}.json`)
      if (response.ok) {
        cache[prefix][locale] = await response.json()
        console.debug(`text-block: successfully loaded ${prefix}-messages for locale ${locale}`)
      } else {
        cache[prefix][locale] = {}
        console.debug(`text-block: missing ${prefix}-messages for locale ${locale}`)
      }
    }
    return cache[prefix][locale]
  }
}

customElements.define('text-block', TextBlock)
