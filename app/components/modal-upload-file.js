import { ModalDialog } from './modal-dialog.js'
import { translateChildren } from '../modules/i18n.js'
import './input-file.js'
import { emit } from '../modules/utils.js'

/**
 * Modal dialog with information about downloading apps as HTML or SCORM packages
 * @property {String} data-file-type - allowed file types (csv, image, json, video)
 * @see ModalDialog
 */
class ModalUploadFile extends ModalDialog {
  #input = null // reference to input element

  connectedCallback() {
    if (this.disconnected) return
    super.connectedCallback()
    // title
    this.title.setAttribute('data-i18n', 'General.load-local-file')
    // body
    this.#input = document.createElement('input-file')
    this.#input.setAttribute('data-file-type', this.dataset.fileType)
    this.#input.setAttribute('data-max-size', this.dataset.maxSize)
    this.#input.style.gridColumn = '1/4'
    this.#input.oninput = () => this.load()
    const form = document.createElement('form')
    form.classList.add('form-container')
    form.append(this.#input)
    this.body.append(form)
    // footer
    const buttonClose = document.createElement('button')
    buttonClose.classList.add('btn', 'btn-secondary')
    buttonClose.setAttribute('data-i18n', 'General.btn-cancel')
    buttonClose.onclick = () => this.emitClose()
    this.footer.append(buttonClose)
    this.footer.removeAttribute('data-hide')
    // translate
    translateChildren(this)
  }

  /**
   * emit load event and close dialog
   */
  load() {
    emit(this, 'load', this.#input.value)
    this.emitClose()
  }
}

// Register element
customElements.define('modal-upload-file', ModalUploadFile)