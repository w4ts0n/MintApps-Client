import { emit } from '../modules/utils.js'
import { translateChildren } from '../modules/i18n.js'
import { importCss } from '../modules/utils.js'
import { HTMLMintElement } from './abstract-components.js'
importCss('modal-dialog.css')

/**
 * Class defining an general modal dialog
 * @property {String} data-title - title of dialog window (reactive)
 * @property {String} [data-buttons] - list of buttons in footer ('close', 'accept', 'delete', 'close')
 * @property {Boolean} [data-wide=false] - extra wide dialog
 * @property {Boolean} [data-keep-open=false] - keep modal open for specific event
 */
export class ModalDialog extends HTMLMintElement {
  card = null // reference to card element (not private, used in child classes)
  body = null // reference to body element (not private, used in child classes)
  title = null // reference to title element (not private, used in child classes)
  footer = null // reference to footer element (not private, used in child classes)
  #onaccept = null // reference to optional accept listener
  #onclose = null // reference to optional close / cancel listener
  #ondelete = null // reference to optional delete listener

  static observedAttributes = ['data-title', 'data-buttons']

  connectedCallback() {
    if (this.disconnected) return
    // set listeners
    window.addEventListener('keydown', (e) => this.keyListener(e))
    // render
    this.hide()
    this.renderModal()
    translateChildren(this)
  }

  /**
   * Render HTML contents of modal dialog
   */
  renderModal() {
    // modal header
    const header = document.createElement('div')
    header.classList.add('header')
    this.title = document.createElement('span')
    this.title.setAttribute('data-i18n', this.dataset.title)
    const img = document.createElement('img')
    img.src = `../assets/icons/close.svg`
    img.classList.add('mint-card-icon')
    img.role = 'button'
    img.setAttribute('data-i18n-alt', 'General.btn-close')
    img.setAttribute('data-i18n-title', 'General.btn-close')
    img.onclick = () => this.emitClose()
    header.append(this.title, img)
    // modal footer
    this.footer = document.createElement('div')
    this.footer.classList.add('footer')
    this.renderFooter()
    // modal body
    this.body = document.createElement('div')
    this.body.classList.add('body')
    const slot = this.childNodes
    if (slot) this.body.append(...slot)
    // modal card
    const card = document.createElement('div')
    this.card = card
    card.classList.add('card')
    card.append(header, this.body, this.footer)
    if (this.dataset.buttons) card.append(this.footer)
    // modal mask in wide view
    const modalWrapper = document.createElement('div')
    modalWrapper.classList.add('modal-wrapper')
    modalWrapper.append(card)
    // root
    this.classList.add('modal-mask')
    this.append(modalWrapper)
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-title':
        if (this.title) {
          this.title.setAttribute('data-i18n', newValue)
          translateChildren(this.title)
        }
        break;
      case 'data-buttons': {
        if (this.footer) this.renderFooter()
        break;
      }
    }
    if (attr === 'data-title' && this.title) {
      this.title.setAttribute('data-i18n', newValue)
      translateChildren(this.title)
    }
  }

  /**
   * Render predefined footer, either 'close' or 'accept'
   */
  renderFooter() {
    this.footer.innerHTML = ''
    const buttons = this.dataset.buttons
    if (!buttons) {
      this.footer.setAttribute('data-hide', '')
    } else {
      this.footer.removeAttribute('data-hide')
      // create buttons
      if (buttons.indexOf('accept') >= 0) {
        const buttonAccept = document.createElement('button')
        buttonAccept.classList.add('btn', 'btn-primary')
        buttonAccept.onclick = () => this.emitAccept()
        buttonAccept.setAttribute('data-i18n', 'General.btn-accept')
        this.footer.append(buttonAccept)
      }
      if (buttons.indexOf('delete') >= 0) {
        const buttonDelete = document.createElement('button')
        buttonDelete.classList.add('btn', 'btn-primary')
        buttonDelete.onclick = () => this.emitDelete()
        buttonDelete.setAttribute('data-i18n', 'General.btn-delete')
        this.footer.append(buttonDelete)
      }
      if (buttons.indexOf('cancel') >= 0) {
        const buttonCancel = document.createElement('button')
        buttonCancel.classList.add('btn', 'btn-secondary')
        buttonCancel.onclick = () => this.emitClose()
        buttonCancel.setAttribute('data-i18n', 'General.btn-cancel')
        this.footer.append(buttonCancel)
      }
      if (buttons.indexOf('close') >= 0) {
        const buttonClose = document.createElement('button')
        buttonClose.classList.add('btn', 'btn-secondary')
        buttonClose.onclick = () => this.emitClose()
        buttonClose.setAttribute('data-i18n', 'General.btn-close')
        this.footer.append(buttonClose)
      }
      translateChildren(this.footer)
    }
  }

  /**
   * Close / Cancel modal dialog
   */
  emitClose() {
    if (!this.dataset.keepOpen || this.dataset.keepOpen.indexOf('close') < 0) this.hide()
    if (this.#onclose) this.#onclose()
    emit(this, 'close')
  }

  /**
   * Accept
   */
  emitAccept() {
    if (!this.dataset.keepOpen || this.dataset.keepOpen.indexOf('accept') < 0) this.hide()
    if (this.#onaccept) this.#onaccept()
    emit(this, 'accept')
  }

  /**
   * Delete
   */
  emitDelete() {
    if (!this.dataset.keepOpen || this.dataset.keepOpen.indexOf('accept') < 0) this.hide()
    if (this.#ondelete) this.#ondelete()
    emit(this, 'delete')
  }

  /**
   * Listen for keyboard event and close on ESC
   * @param {Event} e - keyboard event
   */
  keyListener(e) {
    if (e.keyCode === 27) this.emitClose()
  }


  /**
   * Show dialog (and retranslate contents)
   */
  show() {
    translateChildren(this)
    super.show()
  }

  /**
   * Set single listener for accept
   */
  set onaccept(listener) {
    this.#onaccept = listener
  }

  /**
   * Set single listener for close (cancel)
   */
  set onclose(listener) {
    this.#onclose = listener
  }

  /**
   * Set single listener for delete
   */
  set ondelete(listener) {
    this.#ondelete = listener
  }
}

customElements.define('modal-dialog', ModalDialog)


