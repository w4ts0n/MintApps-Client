import config from '../config.js'
import { listToReadableList } from '../modules/contributors.js'
import { translateChildren } from '../modules/i18n.js'

/**
 * Description of MyComponent
 * @property {String} 'data-xxx' - description of attribut 'data-xxx'
 */
class AppInfo extends HTMLElement {
  connectedCallback() {
    if (this.disconnected) return
    if (!window.mintapps.authors && !window.mintapps.thanks) return
    // container
    const container = document.createElement('div')
    container.classList.add('container')
    // Link to online Version, only for exported apps
    if (config.onlineVersion) {
      const a = document.createElement('a')
      a.href = config.onlineVersion
      a.target = '_blank'
      a.setAttribute('data-i18n', 'AppContributors.online-version')
      container.append(a)
    }
    // Attribution to authors
    const divAuthors = document.createElement('div')
    if (window.mintapps?.authors) {
      divAuthors.setAttribute('data-i18n-md-inline', 'AppContributors.developed-by')
      divAuthors.setAttribute('data-authors', listToReadableList(window.mintapps.authors))
    } else {
      divAuthors.setAttribute('data-i18n-md-inline', 'AppContributors.developed')
    }
    container.append(divAuthors)
    // E-Mail adress for feedback
    if (config.mailto) {
      const divEmail = document.createElement('div')
      divEmail.setAttribute('data-i18n-md-inline','AppContributors.info-mail' )
      divEmail.setAttribute('data-mail', `mailto:${config.mailto}` )
      container.append(divEmail)
    }
    // Thanks to contributors and translators
    const divThanks = document.createElement('div')
    if (window.mintapps?.thanks) {
      divThanks.setAttribute('data-i18n-md-inline', 'AppContributors.thanks-to-extended')
      divThanks.setAttribute('data-thanks', listToReadableList(window.mintapps.thanks))
    } else {
      divThanks.setAttribute('data-i18n-md-inline', 'AppContributors.thanks-to-simple')
    }
    container.append(divThanks)
    // root
    this.classList.add('app-info')
    this.append(container)
    translateChildren(this)
  }

}

// Register element
customElements.define('app-info', AppInfo)