import { translateElement, translateChildren } from '../modules/i18n.js';
import { importCss } from '../modules/utils.js';
import { HTMLMintElement } from './abstract-components.js';
importCss('alert-box.css')

/**
 * Colored information box containing text.
 * @property {String} [type='info'] type of box ('info' or 'warning', reactive)
 * @property {String} data-text - text to display, with markdown support in block mode (reactive)
 * @property {Boolean} [data-container=false] wether to use the class 'container' for the parent node, default is 'false'
 */
class AlertBox extends HTMLMintElement {
  #text // reference to text element
  #alert // reference to alert container
  #icon // reference to close icon

  static observedAttributes = ['data-text', 'data-type']

  connectedCallback() {
    if (this.disconnected) return
    // attributes
    const type = this.dataset.type ?? 'info'
    const container = this.hasAttribute('data-container')
    // render
    this.#alert = document.createElement('div')
    this.#alert.classList.add(type === 'info' ? 'alert-primary' : 'alert-warning')
    // icon
    this.#icon = document.createElement('span')
    this.#icon.classList.add('close-icon', 'cursor-pointer')
    this.#icon.innerText = '⨉'
    this.#icon.onclick = () => this.hide()
    this.#alert.append(this.#icon)
    // text
    this.#text = document.createElement('span')
    this.#text.setAttribute('data-i18n-md-block', this.dataset.text)
    this.#alert.append(this.#text)
    // container
    this.classList.add('alert-box', 'hide-print')
    if (container) this.classList.add('container', 'extra-margin')
    this.append(this.#alert)
  }


  attributeChangedCallback(attr, oldValue, newValue) {
    switch (attr) {
      case 'data-text':
        if (this.#text) {
          this.#text.setAttribute('data-i18n-md-block', newValue)
          translateChildren(this)
        }
        break
      case 'data-type':
        if (this.#alert) {
          this.#alert.classList.remove('alert-warning')
          this.#alert.classList.remove('alert-primary')
          this.#alert.classList.add(newValue === 'warning' ? 'alert-warning' : 'alert-primary')
        }
        break;
    }
  }

  /**
   * Show AlertBox and update text if given
   * @param {String} text 
   * @param {Object} params 
   */
  show(text = '', params = {}) {
    if (params) {
      Object.keys(params).forEach(key => this.#text.setAttribute(`data-${key}`, params[key]))
    }
    if (text) this.setAttribute('data-text', text)
    translateElement(this)
    super.show()
  }
}

customElements.define('alert-box', AlertBox)