import { emit, importCss } from '../modules/utils.js'
import { translateChildren } from '../modules/i18n.js'
import { HTMLMintElement } from './abstract-components.js'
importCss('page-title.css')

/**
 * Display of the page title (with main and subheading) and optionally different tabs.
 * @property {String} data-title - title (translated, reactive)
 * @property {String} data-subtitle - subtitle (translated, reactive)
 * @property {String} [data-tabs=''] - definition of page tabs, for example 'General.simulation, General.information, General.tab-exe'
 * @fires tab - changed by user
 */
class PageTitle extends HTMLMintElement {
  #tabIndex = 0 // active tab
  #container = null // referenct to container element
  #tabGroup = null // reference to tab group element
  #title = null // reference to title element
  #subtitle = null // reference to subtitle element
  #modalConfirm = null // modal dialog to confirm tab change
  #newTabIndex = 0 // new tabindex (when changing tabs)
  #ontab = null // listener to tab events

  static observedAttributes = ['data-title', 'data-subtitle', 'data-tabs']

  connectedCallback() {
    if (this.disconnected) return
    // extra content
    const extra = this.childNodes
    // title
    this.#title = document.createElement('h1')
    this.#title.classList.add('title')
    this.#title.setAttribute('data-i18n-md-inline', this.dataset.title)
    // subtitle
    this.#subtitle = document.createElement('p')
    this.#subtitle.classList.add('subtitle')
    if (this.dataset.subtitle) this.#subtitle.setAttribute('data-i18n-md-block', this.dataset.subtitle)
    // container
    this.#container = document.createElement('div')
    this.#container.classList.add('container', 'page-title-container')
    // tabs and corresponding events
    this.#tabGroup = document.createElement('div')
    this.#tabGroup.classList.add('scorm-hide', 'tab-group')
    this.renderTabs()
    document.querySelectorAll('[data-tab-index]').forEach(e => {
      if (e.getAttribute('data-tab-index') > 0) e.setAttribute('data-hide', '')
    })
    // root
    this.classList.add('page-title')
    this.#container.append(this.#title, this.#subtitle, ...extra, this.#tabGroup)
    this.append(this.#container)
    translateChildren(this)
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (attr === 'data-title' && this.#title) {
      this.#title.setAttribute('data-i18n-md-inline', newValue)
    } else if (attr === 'data-subtitle' && this.#subtitle) {
      this.#subtitle.setAttribute('data-i18n-md-block', newValue)
    } else if (attr === 'data-tabs' && this.#tabGroup) {
      this.renderTabs()
    }
    translateChildren(this)
  }

  /**
   * Render page tabs according to attribute 'data-set-tabs'
   */
  renderTabs() {
    this.#tabGroup.innerHTML = ''
    if (this.dataset.tabs) {
      const tabs = this.dataset.tabs.split(',').map(e => e.trim())
      tabs.forEach((tab, i) => {
        const button = document.createElement('button')
        button.className = this.#tabIndex === i ? 'tab-active' : 'tab-inactive'
        button.onclick = () => this.tabClicked(i)
        button.setAttribute('data-i18n', tab)
        this.#tabGroup.append(button)
      })
    }
  }

  /**
   * Tab clicked by user, change visibility of regions with attribute 'data-tab-index'
   * @param {Number} i - index of active tab
  */
  tabClicked(i) {
    if (i === this.#tabIndex) return
    this.#newTabIndex = i
    // confirm tab change (according to data-tab-confirm attribute)?
    if (this.dataset.tabConfirm?.indexOf(this.#tabIndex) >= 0) {
      this.confirmTabChange()
    } else {
      this.tabIndex = this.#newTabIndex
    }
  }

  /**
   * Set tab (allow other scripts to change tab automatically)
   * Also emit event 'tab' with detail
   */
  set tabIndex(i) {
    this.#tabIndex = i
    // highlight active tab
    this.#tabGroup.childNodes.forEach((button, j) => {
      button.className = j === i ? 'tab-active' : 'tab-inactive'
    })
    // hide / show tab contents
    document.querySelectorAll('[data-tab-index]').forEach(e => {
      const visible = Number(e.getAttribute('data-tab-index')) === this.#tabIndex
      if (visible) e.removeAttribute('data-hide')
      else e.setAttribute('data-hide', '')
    })
    if (this.#ontab) this.#ontab({ detail: i })
    emit(this, 'tab', i)
  }

  /**
   * Get index of active tab
   * @returns {Number}
   */
  get tabIndex() {
    return this.#tabIndex
  }

  /**
   * Show modal confirm dialog
   */
  confirmTabChange() {
    import('./modal-confirm-tab-change.js').then(() => {
      if (!this.#modalConfirm) {
        this.#modalConfirm = document.createElement('modal-confirm-tab-change')
        this.append(this.#modalConfirm)
      }
      this.#modalConfirm.showIf()
      this.#modalConfirm.addEventListener('accept', () => this.tabIndex = this.#newTabIndex)
    })
  }

  /**
   * Set title (without translation)
   */
  set title(newTitle) {
    this.#title.innerText = newTitle
  }

  /**
   * Set subtitle (without translation)
   */
  set subtitle(newSubtitle) {
    this.#subtitle.innerText = newSubtitle
  }

  /**
   * Set listener to tab events
   */
  set ontab(listener) {
    this.#ontab = listener
  }
}

customElements.define('page-title', PageTitle)
