const sitemap = {
  title: 'Menus.title',
  text: 'Menus.index',
  id: 'mint-index',
  topics: [
    {
      id: 'mint-index-math',
      title: 'Menus.mathematics',
      text: 'Menus.index-math',
      icon: 'math.svg',
      subtopics: [
        {
          title: 'Menus.math-section-analysis',
          sites: [
            { title: 'Linear.title', text: 'Linear.menu', id: 'mint-linear', icon: 'linear.svg', authors: 'tk100', thanks: 'hgu', download: true },
            { title: 'Quadratic.title', text: 'Quadratic.menu', id: 'mint-quadratic', icon: 'quadratic.svg', download: true },
            { title: 'Polynom.title', text: 'Polynom.menu', id: 'mint-polynom', icon: 'polynom.svg', authors: 'tk100', thanks: 'hgu', download: true }
          ]
        },
        {
          title: 'Menus.math-section-linalg',
          sites: [
            { title: 'Gauss.title', text: 'Gauss.menu', id: 'mint-gauss', icon: 'gauss.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.math-section-stochastics',
          sites: [
            { title: 'BinomDist.title', text: 'BinomDist.menu', id: 'mint-binom-dist', icon: 'binom-dist.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.math-section-sets',
          sites: [
            { title: 'Complex.title', text: 'Complex.menu', id: 'mint-complex', icon: 'complex.svg', authors: 'tk100', thanks: 'hgu', download: true },
            { title: 'Pi.title', text: 'Pi.menu', id: 'mint-pi', icon: 'pi.svg', authors: 'tk100', thanks: 'hgu', download: true },
            { title: 'Prim.title', text: 'Prim.menu', id: 'mint-prim', icon: 'prim.svg', authors: 'tk100', thanks: 'w4ts0n, hgu', download: true }
          ]
        }
      ]
    },
    {
      id: 'mint-index-info',
      title: 'Menus.informatics',
      text: 'Menus.index-info',
      icon: 'informatics.svg',
      subtopics: [
        {
          title: 'Menus.info-section-1',
          sites: [
            { title: 'Koch.title', text: 'Koch.menu', id: 'mint-koch', icon: 'koch.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Pythagoras.title', text: 'Pythagoras.menu', id: 'mint-pythagoras', icon: 'pythagoras.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Sierpinski.title', text: 'Sierpinski.menu', id: 'mint-sierpinski', icon: 'sierpinski.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Mandelbrot.title', text: 'Mandelbrot.menu', id: 'mint-mandelbrot', icon: 'mandelbrot.svg', download: false, authors: 'tk100', thanks: 'hgu' },
            { title: 'Julia.title', text: 'Julia.menu', id: 'mint-julia', icon: 'julia.svg', download: false, authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.info-section-3',
          sites: [
            { title: 'Perceptron.title', text: 'Perceptron.menu', id: 'mint-perceptron', icon: 'perceptron.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.info-section-2',
          sites: [
            { title: 'Hash.title', text: 'Hash.menu', id: 'mint-hash', icon: 'hash.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        }
      ]
    },
    {
      id: 'mint-index-physics',
      title: 'Menus.physics',
      text: 'Menus.index-physics',
      icon: 'physics.svg',
      subtopics: [
        {
          title: 'Menus.physics-section-mechanics',
          sites: [
            { title: 'HorizontalPlane.title', text: 'HorizontalPlane.menu', id: 'mint-horizontal-plane', icon: 'horizontal-plane.svg', download: true, authors: 'tk100', thanks: 'misp, hgu' },
            { title: 'TiltedPlane.title', text: 'TiltedPlane.menu', id: 'mint-tilted-plane', icon: 'tilted-plane.svg', download: true, authors: 'tk100', thanks: 'misp, hgu' },
            { title: 'Throw.title', text: 'Throw.menu', id: 'mint-throw', icon: 'throw.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Atwood.title', text: 'Atwood.menu', id: 'mint-atwood', icon: 'atwood.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' },
            { title: 'Rocket.title', text: 'Rocket.menu', id: 'mint-rocket', icon: 'rocket.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Collision.title', text: 'Collision.menu', id: 'mint-collision', icon: 'collision.svg', download: true, authors: 'tk100', thanks: 'misp, chsc hgu' },
            { title: 'Ballistic.title', text: 'Ballistic.menu', id: 'mint-ballistic', icon: 'ballistic.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Skydive.title', text: 'Skydive.menu', id: 'mint-skydive', icon: 'skydive.svg', download: true, authors: 'tk100', thanks: 'misp, hgu' },
            { title: 'CentralForce.title', text: 'CentralForce.menu', id: 'mint-central-force', icon: 'central-force.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.physics-section-oszi-waves',
          sites: [
            { title: 'OszillationEquation.title', text: 'OszillationEquation.menu', id: 'mint-oszillation-equation', icon: 'oszillation-equation.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'SpringPendulum.title', text: 'SpringPendulum.menu', id: 'mint-spring-pendulum', icon: 'spring-pendulum.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Pendulum.title', text: 'Pendulum.menu', id: 'mint-pendulum', icon: 'pendulum.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' },
            { title: 'Resonance.title', text: 'Resonance.menu', id: 'mint-resonance', icon: 'resonance.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Waves.title', text: 'Waves.menu', id: 'mint-waves', icon: 'waves.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'WaveEquation.title', text: 'WaveEquation.menu', id: 'mint-wave-equation', icon: 'wave-equation.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'StringWave.title', text: 'StringWave.menu', id: 'mint-stringwave', icon: 'stringwave.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' }
          ]
        },
        {
          title: 'Menus.physics-section-optics',
          sites: [
            {
              title: 'Spectrum.title', text: 'Spectrum.menu', id: 'mint-spectrum', icon: 'spectrum.svg', download: true, authors: 'tk100',
              thanks: 'hgu'
            },
            {
              title: 'Diffraction.title', text: 'Diffraction.menu', id: 'mint-diffraction', icon: 'diffraction.svg', download: true, authors: 'tk100',
              thanks: 'chsc, hgu'
            }
          ]
        },
        {
          title: 'Menus.physics-section-em-field',
          sites: [
            { title: 'Rcalc.title', text: 'Rcalc.menu', id: 'mint-rcalc', icon: 'rcalc.svg', download: true, authors: 'tk100', thanks: 'w4ts0n, rode, hgu' },
            { title: 'Wheatstone.title', text: 'Wheatstone.menu', id: 'mint-wheatstone', icon: 'wheatstone.svg', download: true, authors: 'tk100', thanks: 'w4ts0n, rode, hgu' },
            { title: 'Fluxline.title', text: 'Fluxline.menu', id: 'mint-fluxline', icon: 'fluxline.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Capacity.title', text: 'Capacity.menu', id: 'mint-capacity', icon: 'capacity.svg', authors: 'tk100', thanks: 'hgu', download: true },
            { title: 'Millikan.title', text: 'Millikan.menu', id: 'mint-millikan', icon: 'millikan.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Induction.title', text: 'Induction.menu', id: 'mint-induction', icon: 'induction.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Inductance.title', text: 'Inductance.menu', id: 'mint-inductance', icon: 'inductance.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'LC.title', text: 'LC.menu', id: 'mint-lc', icon: 'lc.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Linacc.title', text: 'Linacc.menu', id: 'mint-linacc', icon: 'linacc.svg', download: true, authors: 'tk100', thanks: 'bech hgu' },
            { title: 'Teltron.title', text: 'Teltron.menu', id: 'mint-teltron', icon: 'teltron.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Zyklotron.title', text: 'Zyklotron.menu', id: 'mint-zyklotron', icon: 'zyklotron.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'AMS.title', text: 'AMS.menu', id: 'mint-ams', icon: 'ams.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Dipole.title', text: 'Dipole.menu', id: 'mint-dipole', icon: 'dipole.svg', download: true, authors: 'tk100', thanks: 'hgu, chsc' },
            { title: 'Microwave.title', text: 'Microwave.menu', id: 'mint-microwave', icon: 'microwave.svg', download: true, authors: 'tk100', thanks: 'hgu, chsc' }
          ]
        },
        {
          title: 'Menus.physics-section-qm-atom',
          sites: [
            { title: 'Photoeffect.title', text: 'Photoeffect.menu', id: 'mint-photoeffect', icon: 'photoeffect.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Compton.title', text: 'Compton.menu', id: 'mint-compton', icon: 'compton.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' },
            { title: 'Dualism.title', text: 'Dualism.menu', id: 'mint-dualism', icon: 'dualism.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'MachZehnder.title', text: 'MachZehnder.menu', id: 'mint-machzehnder', icon: 'machzehnder.svg', download: true, authors: 'tk100', thanks: 'misp, hgu' },
            { title: 'ElectronDiffraction.title', text: 'ElectronDiffraction.menu', id: 'mint-electron-diffraction', icon: 'electron-diffraction.svg', download: true, authors: 'tk100', thanks: 'mr, hgu' },
            { title: 'Wavepacket.title', text: 'Wavepacket.menu', id: 'mint-wavepacket', icon: 'wavepacket.svg', download: true, authors: 'tk100', thanks: 'hgu, chsc' },
            { title: 'Schroedinger.title', text: 'Schroedinger.menu', id: 'mint-schroedinger', icon: 'schroedinger.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Hydrogen.title', text: 'Hydrogen.menu', id: 'mint-hydrogen', icon: 'hydrogen.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'FranckHertz.title', text: 'FranckHertz.menu', id: 'mint-franckhertz', icon: 'franckhertz.svg', download: true, authors: 'tk100', thanks: 'mabo, hgu' },
            { title: 'Xray.title', text: 'Xray.menu', id: 'mint-xray', icon: 'xray.svg', download: true, authors: 'tk100', thanks: 'mabo, hgu' }
          ]
        },
        {
          title: 'Menus.physics-section-thermodynamics',
          sites: [
            { title: 'TDChange.title', text: 'TDChange.menu', id: 'mint-tdchange', icon: 'tdchange.svg', authors: 'tk100, misp', thanks: 'hgu', download: true }
          ]
        }, {
          title: 'Menus.tools',
          sites: [
            { title: 'Vidana.title', text: 'Vidana.menu', id: 'mint-vidana', icon: 'vidana.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' },
            { title: 'Plot.title', text: 'Plot.menu', id: 'mint-plot', icon: 'plot.svg', authors: 'tk100', thanks: 'chsc, hgu', download: true }
          ]
        }
      ]
    },
    {
      id: 'mint-index-games',
      title: 'Menus.games',
      text: 'Menus.index-games',
      icon: 'games.svg',
      subtopics: [
        {
          title: 'Menus.game-section-1',
          sites: [
            { title: 'Pairs.title', text: 'Pairs.menu', id: 'mint-pairs', icon: 'pairs.svg', authors: 'tk100', thanks: 'hgu' },
            { title: 'Quiz.title', text: 'Quiz.menu', id: 'mint-quiz', icon: 'quiz.svg', authors: 'tk100', thanks: 'hgu' },
            { title: 'Price.title', text: 'Price.menu', id: 'mint-price', icon: 'price.svg', authors: 'tk100', thanks: 'hgu' }
          ]
        },
        {
          title: 'Menus.game-section-2',
          sites: [
            { title: 'Life.title', text: 'Life.menu', id: 'mint-life', icon: 'life.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Tictac.title', text: 'Tictac.menu', id: 'mint-tictac', icon: 'tictac.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'FourWins.title', text: 'FourWins.menu', id: 'mint-fourwins', icon: 'fourwins.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'FourColors.title', text: 'FourColors.menu', id: 'mint-fourcolors', icon: 'fourcolors.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Sudoku.title', text: 'Sudoku.menu', id: 'mint-sudoku', icon: 'sudoku.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Mathefix.title', text: 'Mathefix.menu', id: 'mint-mathefix', icon: 'mathefix.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        }
      ]
    },
    {
      id: 'mint-index-tools',
      title: 'Menus.tools',
      text: 'Menus.index-tools',
      icon: 'tools.svg',
      subtopics: [
        {
          title: 'Menus.tools-section-1',
          sites: [
            { title: 'Vidana.title', text: 'Vidana.menu', id: 'mint-vidana', icon: 'vidana.svg', download: true, authors: 'tk100', thanks: 'chsc, hgu' },
            { title: 'Plot.title', text: 'Plot.menu', id: 'mint-plot', icon: 'plot.svg', authors: 'tk100', thanks: 'chsc, hgu', download: true },
            { title: 'Poll.title', text: 'Poll.menu', id: 'mint-poll', icon: 'poll.svg', authors: 'tk100', thanks: 'chsc, hgu', download: false },
            { title: 'QrGenerator.title', text: 'QrGenerator.menu', id: 'mint-qr', icon: 'qr.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'QrReader.title', text: 'QrReader.menu', id: 'mint-qr-reader', icon: 'qr-reader.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Vcard.title', text: 'Vcard.menu', id: 'mint-vcard', icon: 'vcard.svg', download: true, authors: 'tk100', thanks: 'hgu' },
          ]
        },
        {
          title: 'Menus.tools-section-2',
          sites: [
            { title: 'AsciiMath.title', text: 'AsciiMath.menu', id: 'mint-ascii-math', icon: 'ascii-math.svg', download: true, authors: 'tk100', thanks: 'hgu' },
            { title: 'Markdown.title', text: 'Markdown.menu', id: 'mint-markdown', icon: 'markdown.svg', download: true, authors: 'tk100', thanks: 'hgu' }
          ]
        }
      ]
    },
    // ------------------------------------------------------------------------------------ user
    { title: 'UserProfile.title', text: 'UserProfile.subtitle', id: 'mint-user-profile', authors: 'tk100', thanks: 'hgu' },
    { title: 'UserAdmin.title', text: 'UserAdmin.subtitle', id: 'mint-user-admin', authors: 'tk100', thanks: 'hgu' },
    { title: 'General.activities', text: 'UserData.subtitle', id: 'mint-user-data', authors: 'tk100', thanks: 'hgu' },
    // ------------------------------------------------------------------------------------ lab
    {
      title: 'Menus.lab',
      text: 'Menus.index-lab',
      icon: 'lab.svg',
      id: 'mint-index-lab',
      subtopics: [
        {
          title: 'Menus.lab-section-3',
          sites: [
            { title: 'Documentation.title', text: 'Documentation.menu', id: 'mint-doc-client', icon: 'documentation.svg', authors: 'tk100' },
            { title: 'Template.title', text: 'Template.menu', id: 'mint-template', icon: 'template.svg', authors: 'tk100', thanks: 'w4ts0n' }
          ]
        },
        {
          title: 'Menus.lab-section-0',
          sites: [
            { title: 'ImgEncoder.title', text: 'ImgEncoder.menu', id: 'mint-img-encoder', icon: 'img-encoder.svg', download: true, authors: 'tk100', thanks: 'hgu' },
          ]
        },
        {
          title: 'Menus.lab-section-1',
          sites: [
          ]
        }
      ]
    },
    {
      title: 'Search.title',
      text: 'Menus.index-search',
      icon: 'search.svg',
      id: 'mint-search'
    },
    {
      title: 'News.title',
      text: 'Menus.index-news',
      icon: 'news.svg',
      id: 'mint-news'
    },
    {
      title: 'Info.title',
      text: 'Info.menu',
      icon: 'info.svg',
      id: 'mint-info'
    },
    {
      title: 'Menus.about',
      id: 'about'
    },
    {
      title: 'Menus.privacy',
      id: 'privacy'
    }
  ]
}
export { sitemap }
