export default {
  // Basic settings
  name: 'MintApps',
  version: '2.0.0',
  aboutUrl: '',
  privacyUrl: '',
  syncUrl: '',
  seoOptimization: true,
  website: true,
  mailto: '',

  // locale settings
  defaultLocale: 'de',
  fallbackLocale: 'en',

  // theme settings
  defaultTheme: 'light',

  // appeareance settings
  columnLimit: 2,

  // pathes, must end with trailing slash
  dataPath: '../assets/data/',
  weblatePath: '../assets/weblate/',
  downloadPath: '../assets/download/'
}
