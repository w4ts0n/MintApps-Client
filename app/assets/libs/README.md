# External linbraries

This folder contains external libraries used by the MintApps. Please read the corresponding license terms in the list below.

## browser-image-compression

- Repository: https://github.com/Donaldcwl/browser-image-compression
- License: MIT License
- File: browser-image-compression.js

## jsQR

- Repository: https://github.com/cozmo/jsQR
- License: Apache License 2.0
- File: jsqr.js

## qr-creator

- Repository: https://github.com/nimiq/qr-creator
- License: MIT
- File: qr-creator.es6.min.js

## DOMPurify

- Repository: https://github.com/cure53/DOMPurify
- License: Apache
- File: purify.min.js

## Three.js

- Repository: https://github.com/mrdoob/three.js
- License: MIT License
- Files: CSS2DRenderer.js, OrbitControls.js, three.module.min.js, three.core.min.js

## three-fatline

- Repository: https://github.com/vasturiano/three-fatline
- License: MIT License
- File: three-fatline.js (ending changed from mjs to js to avoid browser errors regarding wrong mimetype)
