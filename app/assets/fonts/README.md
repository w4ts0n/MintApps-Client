# Fonts

## STIXTwoMath-Regular: 

- Font used for math expressions
- Repository [github.com/stipub/stixfonts](https://github.com/stipub/stixfonts)
- License [SIL OPEN FONT LICENSE Version 1.1](OFl.txt)