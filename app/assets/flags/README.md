# Flags

Images that symbolize the different languages

Author: Hampus Joakim Borgos
Source: https://github.com/hampusborgos/country-flags/tree/main/svg
License: public domain
