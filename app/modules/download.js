/**
 * Methods to store app contents (images, JSON, etc.) locally from the browser
 * @author Thomas Kippenberg
 * @module download
*/
import csv from './csv.js'

/**
 * Download JSON data
 * @param {string} title - filename without extension
 * @param {object} o - data object to be serialized in JSON
 */
export function downloadJson (title, o) {
  const data = JSON.stringify(o, null, '  ')
  const blob = new Blob([data], { type: 'text/plain' })
  const a = document.createElement('a')
  if (title) a.download = title + '.json'
  else if (o.title) a.download = o.title + '.json'
  else if (o.name) a.download = o.name + '.json'
  else a.download = 'download.json'
  a.href = window.URL.createObjectURL(blob)
  a.dataset.downloadurl = ['text/json', a.download, a.href].join(':')
  a.click()
}

/**
 * Download plain text
 * @param {string} title - filename without extension
 * @param {string} text - plain text to be downloaded
 */
export function downloadText (title, text) {
  const blob = new Blob([text], { type: 'text/plain' })
  const a = document.createElement('a')
  a.download = title + '.txt'
  a.href = window.URL.createObjectURL(blob)
  a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':')
  a.click()
}

/**
 * Download vcard
 * @param {string} title - filename without extension
 * @param {string} text - plain text according to vcard standard
 */
export function downloadVcard (title, text) {
  const blob = new Blob([text], { type: 'text/vcard' })
  const a = document.createElement('a')
  a.download = title + '.vcf'
  a.href = window.URL.createObjectURL(blob)
  a.dataset.downloadurl = ['text/vcard', a.download, a.href].join(':')
  a.click()
}

/**
 * Simple CSV Export according to RFC 4180
 * @param {string} title - filename without extension
 * @param {string[][]} data - twodimensional data array
 */
export function downloadCsv (title, data) {
  const text = csv.data2csv(data)
  const blob = new Blob([text], { type: 'text/csv' })
  const a = document.createElement('a')
  a.download = title + '.csv'
  a.href = window.URL.createObjectURL(blob)
  a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':')
  a.click()
}

/**
 * Download 2d canvas as Image
 * @param {string} title - filename without extension
 * @param {HTMLCanvasElement} canvas - canvas element containing image
 */
export function downloadCanvas (title, canvas) {
  const image = canvas.toDataURL('image/jpg')
  const a = document.createElement('a')
  a.download = title + '.png'
  a.href = image
  a.click()
}

/**
 * Download image form DataURL
 * @param {*} title - filename without extension
 * @param {string} image - image data as DataURL
 */
export function downloadImage (title, image) {
  const a = document.createElement('a')
  a.download = title + '.png'
  a.href = image
  a.click()
}
