/**
 * Module that contains the calculations for the mint-plot app.
 * Original Code from Timo Denk, MIT License,
 * Slightly adjusted by Thomas Kippenberg to be used as ES6 Modul
 * @see {@link https://github.com/Simsso/Online-Tools/blob/master/src/page/logic/cubic-spline-interpolation.js|Github, Original Version}
 * @author Thomas Kippenberg
 * @module mint-plot
 */

/**
 * Prepare the value pairs for calculating the spline function
 * @param {Array} points - array of value pairs { x, y} 
 * @returns Sorted and validated value pairs
 */
export function processPoints(points) {
  // sort array by x values
  points.sort(function (a, b) {
    if (a.x < b.x) return -1
    if (a.x === b.x) return 0
    return 1
  })
  for (let i = 0; i < points.length; i++) {
    if (i < points.length - 1 && points[i].x === points[i + 1].x) {
      // two points have the same x-value
      // check if the y-value is the same
      if (points[i].y === points[i + 1].y) {
        // remove the latter
        points.splice(i, 1)
        i--
      } else {
        throw Error('SameXDifferentY')
      }
    }
  }
  if (points.length < 2) {
    throw Error('NotEnoughPoints')
  }
  for (let i = points.length - 1; i >= 0; i--) {
    points[i].x = parseFloat(points[i].x)
    points[i].y = parseFloat(points[i].y)
  }
  return points
}

/**
 * Cubic spline interpolation
 * @param {*} p - points. An array of objects with x and y coordinate.
 * @param {string} boundary - interpolation boundary condition ("quadratic", "notaknot", "periodic", "natural"). "natural" is the default value.
 * @returns {functions[]} - array of interpolation functions
 */
export function cubicSplineInterpolation(p, boundary) {
  let row = 0
  const solutionIndex = (p.length - 1) * 4
  // initialize matrix
  const m = [] // rows
  for (let i = 0; i < (p.length - 1) * 4; i++) {
    // columns (rows + 1)
    m.push([])
    for (let j = 0; j <= (p.length - 1) * 4; j++) {
      m[i].push(0) // fill with zeros
    }
  }
  // splines through p equations
  for (let functionNr = 0; functionNr < p.length - 1; functionNr++, row++) {
    const p0 = p[functionNr]
    const p1 = p[functionNr + 1]
    m[row][functionNr * 4 + 0] = Math.pow(p0.x, 3)
    m[row][functionNr * 4 + 1] = Math.pow(p0.x, 2)
    m[row][functionNr * 4 + 2] = p0.x
    m[row][functionNr * 4 + 3] = 1
    m[row][solutionIndex] = p0.y
    m[++row][functionNr * 4 + 0] = Math.pow(p1.x, 3)
    m[row][functionNr * 4 + 1] = Math.pow(p1.x, 2)
    m[row][functionNr * 4 + 2] = p1.x
    m[row][functionNr * 4 + 3] = 1
    m[row][solutionIndex] = p1.y
  }
  // first derivative
  for (let functionNr = 0; functionNr < p.length - 2; functionNr++, row++) {
    const p1 = p[functionNr + 1]
    m[row][functionNr * 4 + 0] = 3 * Math.pow(p1.x, 2)
    m[row][functionNr * 4 + 1] = 2 * p1.x
    m[row][functionNr * 4 + 2] = 1
    m[row][functionNr * 4 + 4] = -3 * Math.pow(p1.x, 2)
    m[row][functionNr * 4 + 5] = -2 * p1.x
    m[row][functionNr * 4 + 6] = -1
  }
  // second derivative
  for (let functionNr = 0; functionNr < p.length - 2; functionNr++, row++) {
    const p1 = p[functionNr + 1]
    m[row][functionNr * 4 + 0] = 6 * p1.x
    m[row][functionNr * 4 + 1] = 2
    m[row][functionNr * 4 + 4] = -6 * p1.x
    m[row][functionNr * 4 + 5] = -2
  }
  // boundary conditions
  switch (boundary) {
    case 'quadratic': // first and last spline quadratic
      m[row++][0] = 1
      m[row++][solutionIndex - 4 + 0] = 1
      break
    case 'notaknot': // Not-a-knot spline
      m[row][0 + 0] = 1
      m[row++][0 + 4] = -1
      m[row][solutionIndex - 8 + 0] = 1
      m[row][solutionIndex - 4 + 0] = -1
      break
    case 'periodic': // periodic function
      // first derivative of first and last point equal
      m[row][0] = 3 * Math.pow(p[0].x, 2)
      m[row][1] = 2 * p[0].x
      m[row][2] = 1
      m[row][solutionIndex - 4 + 0] = -3 * Math.pow(p[p.length - 1].x, 2)
      m[row][solutionIndex - 4 + 1] = -2 * p[p.length - 1].x
      m[row++][solutionIndex - 4 + 2] = -1
      // second derivative of first and last point equal
      m[row][0] = 6 * p[0].x
      m[row][1] = 2
      m[row][solutionIndex - 4 + 0] = -6 * p[p.length - 1].x
      m[row][solutionIndex - 4 + 1] = -2
      break
    default: // natural spline
      m[row][0 + 0] = 6 * p[0].x
      m[row++][0 + 1] = 2
      m[row][solutionIndex - 4 + 0] = 6 * p[p.length - 1].x
      m[row][solutionIndex - 4 + 1] = 2
      break
  }
  const reducedRowEchelonForm = rref(m)
  const coefficients = []
  for (let i = 0; i < reducedRowEchelonForm.length; i++) {
    coefficients.push(
      reducedRowEchelonForm[i][reducedRowEchelonForm[i].length - 1]
    )
  }
  const functions = []
  for (let i = 0; i < coefficients.length; i += 4) {
    const a = parseFloat(coefficients[i])
    const b = parseFloat(coefficients[i + 1])
    const c = parseFloat(coefficients[i + 2])
    const d = parseFloat(coefficients[i + 3])
    functions.push({
      y: (x) => a * x * x * x + b * x * x + c * x + d,
      range: {
        xmin: parseFloat(p[i / 4].x),
        xmax: parseFloat(p[i / 4 + 1].x)
      }
    })
  }
  return functions
}

// Reduced row echelon form
// Taken from https://rosettacode.org/wiki/Reduced_row_echelon_form
function rref(mat) {
  let lead = 0
  for (let r = 0; r < mat.length; r++) {
    if (mat[0].length <= lead) {
      return
    }
    let i = r
    while (mat[i][lead] === 0) {
      i++
      if (mat.length === i) {
        i = r
        lead++
        if (mat[0].length === lead) {
          return
        }
      }
    }

    const tmp = mat[i]
    mat[i] = mat[r]
    mat[r] = tmp

    let val = mat[r][lead]
    for (let j = 0; j < mat[0].length; j++) {
      mat[r][j] = mat[r][j] / val
    }

    for (let i = 0; i < mat.length; i++) {
      if (i === r) continue
      val = mat[i][lead]
      for (let j = 0; j < mat[0].length; j++) {
        mat[i][j] = mat[i][j] - val * mat[r][j]
      }
    }
    lead++
  }
  return mat
}

/**
 * Simple spline interpolation by fitting polynomal functions of order 3
 * @param {*} p - points. An array of objects with x and y coordinate.
 * @returns {functions[]} - array of interpolation functions
 */
export function simpleSplineInterpolation(p) {
  if (p.length <= 1) return []
  // calculate gradients
  for (let i = 0; i < p.length; i++) {
    if (i === 0) {
      p[i].m = (p[i + 1].y - p[i].y) / (p[i + 1].x - p[i].x)
    } else if (i + 1 === p.length) {
      p[i].m = (p[i - 1].y - p[i].y) / (p[i - 1].x - p[i].x)
    } else {
      p[i].m = 0.5 * (p[i + 1].y - p[i].y) / (p[i + 1].x - p[i].x) + 0.5 * (p[i - 1].y - p[i].y) / (p[i - 1].x - p[i].x)
    }
  }
  // calculate polynom functions of order 3
  const functions = []
  for (let i = 0; i < p.length - 1; i++) {
    const x0 = p[i].x
    const x1 = p[i + 1].x
    const m0 = p[i].m
    const m1 = p[i + 1].m
    const y0 = p[i].y
    const y1 = p[i + 1].y
    const a = -(-2 * y1 + 2 * y0 + (m1 + m0) * x1 + (-m1 - m0) * x0) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const b = (-3 * x1 * y1 + x0 * ((m1 - m0) * x1 - 3 * y1) + (3 * x1 + 3 * x0) * y0 + (m1 + 2 * m0) * x1 ** 2 + (-2 * m1 - m0) * x0 ** 2) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const c = -(x0 * ((2 * m1 + m0) * x1 ** 2 - 6 * x1 * y1) + 6 * x0 * x1 * y0 + m0 * x1 ** 3 + (-m1 - 2 * m0) * x0 ** 2 * x1 - m1 * x0 ** 3) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const d = (x0 ** 2 * ((m1 - m0) * x1 ** 2 - 3 * x1 * y1) + x0 ** 3 * (y1 - m1 * x1) + (3 * x0 * x1 ** 2 - x1 ** 3) * y0 + m0 * x0 * x1 ** 3) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    functions.push({
      y: (x) => a * x ** 3 + b * x ** 2 + c * x + d,
      range: {
        xmin: parseFloat(p[i].x),
        xmax: parseFloat(p[i + 1].x)
      }
    })
  }
  return functions

}