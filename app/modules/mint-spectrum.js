/**
 * Module that contains the wavelengths of the line spectra of different gases and light-emitting diodes; used in the app mint-spectrum
 * @module mint-spectrum
 * @author Thomas Kippenberg
 * @see https://qudev.phys.ethz.ch/static/content/science/BuchPhysikIV/PhysikIVch8.html
 * @see https://kompendium.infotip.de/anorganische-leuchtdioden.html
 */

/**
 * Wavelengths of red, green, blue and "white" LEDs
 */
const ledLines = [
  [
    // red
    { l: 660, w: 55, i: 1 }
  ],
  [
    // green
    { l: 525, w: 35, i: 1 }
  ],
  [
    // blue
    { l: 430, w: 25, i: 1 }
  ],
  [
    // "white"
    { l: 475, w: 25, i: 1 },
    { l: 570, w: 100, i: 1 }
  ]
]

/**
 * Line spectra of various gases
 */
const gasLines = [
  [
    // H
    { l: 388, w: 1, i: 0.1 },
    { l: 397, w: 1, i: 0.2 },
    { l: 410, w: 1, i: 0.3 },
    { l: 434, w: 1, i: 0.5 },
    { l: 486.1, w: 1, i: 0.7 },
    { l: 656.3, w: 1, i: 1 }
  ],
  [
    // He
    { l: 402.5, w: 1, i: 0.2 },
    { l: 447.1, w: 1, i: 0.4 },
    { l: 471.3, w: 1, i: 0.4 },
    { l: 492.1, w: 1, i: 0.5 },
    { l: 501.5, w: 1, i: 0.7 },
    { l: 587.5, w: 1, i: 1 },
    { l: 667.8, w: 1, i: 1 }
  ],
  [
    // Na
    { l: 589.0, w: 1, i: 1 },
    { l: 589.6, w: 1, i: 1 }
  ],
  [
    // Hg
    { l: 404.7, w: 1, i: 0.1 },
    { l: 435.8, w: 1, i: 0.5 },
    { l: 502.5, w: 1, i: 0.2 },
    { l: 546.1, w: 1, i: 1 },
    { l: 577, w: 0.7, i: 0.5 },
    { l: 579, w: 0.7, i: 0.4 },
    { l: 615.2, w: 1, i: 0.4 },
    { l: 623.4, w: 1, i: 0.4 }
  ]
]
export { gasLines, ledLines }
