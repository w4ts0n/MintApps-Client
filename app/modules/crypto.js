/**
 * Module with various cryptographic functions
 * @author Thomas Kippenberg
 * @module crypto
 */

/**
 * Calculate sha1 hash
 * @param {string} text - plain text
 * @returns {string} hash (hex encoded)
 */
export function sha1 (text) {
  return sha(text, 'SHA-1')
}

/**
 * Calculate sha2 hash with 256 bit
 * @param {string} text - plain text
 * @returns {string} hash (hex encoded)
 */
export function sha2_256 (text) {
  return sha(text, 'SHA-256')
}

/**
 * Calculate sha2 hash with 384 bit
 * @param {string} text - plain text
 * @returns {string} hash (hex encoded)
 */
export function sha2_384 (text) {
  return sha(text, 'SHA-384')
}

/**
 * Calculate sha2 hash with 512 bit
 * @param {string} text - plain text
 * @returns {string} hash (hex encoded)
 */
export function sha2_512 (text) {
  return sha(text, 'SHA-512')
}

/**
 * Create SHA hash using crypto support of browser (limited to few algorithms)
 * @param {string} text - plain text
 * @param {string} algorithm - one of the supported algorithms (SHA-1, SHA-256, SHA-384, SHA-512)
 * @returns {string} hash (hex encoded)
 */
function sha (text, algorithm) {
  return new Promise((resolve, reject) => {
    const encoder = new TextEncoder()
    const data = encoder.encode(text)
    try {
      crypto.subtle.digest(algorithm, data).then((hashBuffer) => {
        const hashArray = Array.from(new Uint8Array(hashBuffer))
        const hash = hashArray
          .map((b) => b.toString(16).padStart(2, '0'))
          .join('')
        resolve(hash)
      })
    } catch (e) {
      reject(e)
    }
  })
}
