/**
 * Collection of utility functions for loading examples, local files and activities
 * @module load
 * @author Thomas Kippenberg
 */
import * as http from './http.js'
import { validateText, validateTextArea, validateDataUrl, validateInteger, validateNumber } from './validator.js'

// Maximum file size
const maxFileSize = 100e6
const parserList = {
  'pairs': parsePairs,
  'perceptron': parsePerceptron,
  'plot': parsePlot,
  'poll': parsePoll,
  'price': parsePrice,
  'quiz': parseQuiz,
  'tdchange': parseTdchange
}

/**
 * Load from local file
 * @param {string} filename - file name
 * @returns {string} file contents
 */
function loadLocalFile (filename) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsText(filename)
    reader.onload = (e) => {
      const text = e.target.result
      resolve(text)
    }
    reader.onerror = (e) => { reject(e) }
  })
}

/**
 * General function for loading examples or activities. 
 * The type and content of the "src" parameter for the source determine how the data should be loaded.
 * @param {string} [src] - json encoded string of data
 * @param {string} [src.id] - id for activities or examples
 * @param {File} [src] - file object for local file
 * @param {object} [src] - javascript data object (param type must be 'string' in this case)
 * @returns {object} imported and validated data
 */
export async function loadAndParse (src, type) {
  let data = {}
  let json = '' // required to check max size
  // read local file
  if (src instanceof File) {
    json = await loadLocalFile(src)
    data = JSON.parse(json)
  }
  // id is given
  else if(src.id) {
    // validate id parameter
    if (typeof src.id !== 'string' || !src.id.match(/^[0-9a-zA-Z-]+$/)) throw new Error('local-load-invalid-id') 
    // activity
    if (isActivityId(src.id)) data = await http.post('sync:get-activity', { id: src.id })
    // example
    else data = await http.get(`data:${type}/${src.id}.json`)
    // parse to json 
    json = JSON.stringify(data)
  }
  // src is string
  else if (typeof src === 'string') {
    json = src
    data = JSON.parse(json)
  }
  // src is general object
  else if (typeof src === 'object' && typeof src.type === 'string') {
    data = src
    json = JSON.stringify(data)
  }
  // error
  else throw new Error('local-load')
  // check length and type of data param
  if (json.length > maxFileSize) throw new Error('local-load-size')
  // check if type matches
  if (data.type && data.type !== type) throw new Error('wrong-app-type', { cause: data.type })
  // parse specific information
  const parser = parserList[type]
  const result = parser(data)
  // parse meta information (common for all files)
  if (!data.title && data.name) data.title = data.name // name backward compatibility with lerntools
  // see also GameSettings.vue
  result.title = validateText(data.title, true, 300) ? data.title : ''
  result.desc = validateTextArea(data.desc, true, 500) ? data.desc : ''
  result.license = validateText(data.license, true, 100) ? data.license : ''
  result.author = validateText(data.author, true, 100) ? data.author : ''
  result.locale = validateText(data.locale, true, 8) ? data.locale : ''
  result.subject = validateText(data.subject, true, 300) ? data.subject : ''
  result.icon = validateDataUrl(data.icon, true) ? data.icon : ''
  // choose default id
  result.id = 0
  // done
  return result
}

/**
 * pairs specific parser
 * @param {object} data 
 * @returns {object}
 */
function parsePairs (data) {
  const result = {}
  result.cards = []
  if (!data.cards || !Array.isArray(data.cards)) throw new Error('local-load')
  result.mode = data.mode === 1 ? 1 : 0
  for (const card of data.cards) {
    const image1 = validateDataUrl(card.image1, true) ? card.image1 : ''
    const image2 = validateDataUrl(card.image2, true) ? card.image2 : ''
    const text1 = validateTextArea(card.text1, true, 300) ? card.text1 : ''
    const text2 = validateTextArea(card.text2, true, 300) ? card.text2 : ''
    result.cards.push({ image1, image2, text1, text2 })
  }
  return result
}

/**
 * perceptron specific parser
 * @param {object} data 
 * @returns {object}
 */
function parsePerceptron (data) {
  // consistency checks
  if (
    (!validateInteger(data.numLayers, 2, 3)) ||
    (!data.numNodes || !Array.isArray(data.numNodes) || data.numNodes.length != data.numLayers) ||
    (!data.dataSets || !Array.isArray(data.dataSets)) ||
    (!data.thresholds || !Array.isArray(data.thresholds)) ||
    (!data.weights || !Array.isArray(data.weights))
  ) throw new Error('local-load')
  // start parsing
  const result = { numLayers: data.numLayers, numNodes: [], thresholds: [], weights: [], dataSets: [] }
  for (let i = 0; i < result.numLayers; i++) {
    result.numNodes[i] = validateInteger(data.numNodes[i], 1, 10) ? data.numNodes[i] : 1
  }
  for (let i = 0; i < data.dataSets.length; i++) {
    const ds = data.dataSets[i]
    if (!ds.inputs || !Array.isArray(ds.inputs) || !ds.targets || !Array.isArray(ds.targets)) throw new Error('local-load')
    const tmp = { id: Math.random(), inputs: [], targets: [] }
    for (let j = 0; j < result.numNodes[0]; j++) {
      tmp.inputs.push(validateNumber(ds.inputs[j], -1E100, 1E100) ? ds.inputs[j] : 0)
    }
    for (let j = 0; j < result.numNodes[result.numLayers - 1]; j++) {
      tmp.targets.push(validateNumber(ds.targets[j], -1E100, 1E100) ? ds.targets[j] : 0)
    }
    result.dataSets.push(tmp)
  }
  for (let i = 0; i + 1 < result.numLayers; i++) {
    result.weights[i] = []
    for (let j = 0; j < result.numNodes[i]; j++) {
      result.weights[i][j] = []
      for (let k = 0; k < result.numNodes[i + 1]; k++) {
        result.weights[i][j][k] = validateNumber(data.weights[i][j][k], -1E100, 1E100) ? data.weights[i][j][k] : 0
      }
    }
  }
  for (let i = 0; i < result.numLayers; i++) {
    result.thresholds[i] = []
    for (let j = 0; j < result.numNodes[i]; j++) {
      result.thresholds[i][j] = validateNumber(data.thresholds[i][j], -1E100, 1E100) ? data.thresholds[i][j] : 0
    }
  }
  return result
}

/**
 * plot specific parser
 * @param {object} data 
 * @returns {object}
 */
function parsePlot (data) {
  const result = {}
  result.titleX = validateText(data.titleX, true, 30) ? data.titleX : ''
  result.titleY = validateText(data.titleY, true, 30) ? data.titleY : ''
  result.unitX = validateText(data.unitX, true, 30) ? data.unitX : '1'
  result.unitY = validateText(data.unitY, true, 30) ? data.unitY : '1'
  result.transX = validateInteger(data.transX, 0, 11) ? data.transX : 0
  result.transY = validateInteger(data.transY, 0, 11) ? data.transY : 0
  result.fit = validateInteger(data.fit, 0, 4) ? data.fit : 0
  result.icon = validateDataUrl(data.icon, true) ? data.icon : ''
  if (!data.inputValues || !Array.isArray(data.inputValues)) throw new Error('local-load')
  result.inputValues = []
  for (const line of data.inputValues) { 
    result.inputValues.push({ 
      x: validateNumber(line.x) ? line.x : Number(line.x.replaceAll(',', '.')),
      y: validateNumber(line.y) ? line.y : Number(line.y.replaceAll(',', '.')) 
    }) 
  }
  return result
}

/**
 * poll specific parser
 * @param {object} data 
 * @returns {object}
 */

function parsePoll (data) { 
  const result = {}
  const typeSelect = 3
  const typeNumber = 4 // typeStars = 1, typeMood = 2,  typeText = 5, typeBoolean = 0,
  if (!data.questions || !Array.isArray(data.questions)) throw new Error('local-load')
  result.questions = []
  for (const question of data.questions) {
    const text = validateText(question.text, true, 300) ? question.text : ''
    const type = validateInteger(question.type, 0, 5) ? question.type : 0
    const options = validateText(question.options, true, 300) ? question.options : ''
    const image = validateDataUrl(question.image, true) ? question.image : ''
    const time = validateInteger(question.time, 0, 300) ? question.time : 0
    const min = (type === typeNumber || Number.isInteger(question.min)) ? question.min : 0
    const max = (type === typeNumber || Number.isInteger(question.max)) ? question.max : 0
    if (options.trim() === '' && type === typeSelect) throw new Error('local-load')
    result.questions.push({ text, type, options, image, time, min, max })
  }
  return result
}

/**
 * price specific parser
 * @param {object} data 
 * @returns {object}
 */
function parsePrice (data) { 
  const result = { topics: [], scores: [], questions: [] }
  if (!data.topics || !Array.isArray(data.topics)) throw new Error('local-load')
  for (const topic of data.topics) {
    result.topics.push(validateText(topic, true, 100) ? topic : '–')
  }
  if (!data.scores || !Array.isArray(data.scores)) throw new Error('local-load')
  for (const score of data.scores) {
    result.scores.push(validateInteger(score, 0, 1000) ? score : 0)
  }
  if (!data.questions || !Array.isArray(data.questions)) throw new Error('local-load')
  result.numScores = validateInteger(data.numScores, 1, 9) ? data.numScores : result.scores.length
  result.numTopics = validateInteger(data.numTopics, 1, 9) ? data.numTopics : result.scores.length
  for (const question of data.questions) {
    result.questions.push({
      pos : typeof question.pos === 'string' && question.pos.match(/^[0-9]{2}$/) ? question.pos : '00',
      text : validateTextArea(question.text, true, 500) ? question.text : '',
      answer : validateTextArea(question.text, true, 500) ? question.answer : '',
      image : validateDataUrl(question.image, true) ? question.image : '',
      time : validateInteger(question.time, 1, 300) ? question.time : 1
    })
  }
  return result
}

/**
 * quiz specific parser
 * @param {object} data 
 * @returns {object}
 */
function parseQuiz (data) { 
  const result = { questions: [] }
  if (!data.questions || !Array.isArray(data.questions)) throw new Error('local-load')
  for (const question of data.questions) {
    // validate answer options
    const options = []
    if (!question.options || !Array.isArray(question.options)) throw new Error('local-load')
    for (const option of question.options) {
      options.push(validateText(option, true, 300) ? option : '')
    }
    // validate correct answers
    const answers = new Array(options.length)
    answers.fill(false)
    if (validateInteger(question.answer, 0, answers.length - 1)) {
      answers[question.answer] = true // backward compability ABCD-Quiz, only single choice
    } else {
      if (!question.answers || !Array.isArray(question.answers) || question.answers.length !== answers.length) throw new Error('local-load')
      for (let i = 0; i < answers.length; i++) {
        answers[i] = question.answers[i]
      }
    }
    // add question
    result.questions.push({
      options,
      text: validateTextArea(question.text, true, 300) ? question.text : '',
      info: validateTextArea(question.info, true, 300) ? question.info : '',
      time: validateInteger(question.time, 0, 300) ? question.time : 30,
      answers,
      image: validateDataUrl(question.image, true) ? question.image : '',
      type: question.type === 1 ? 1 : 0
    })
  }
  return result
}

/**
 * tdchange specific parser
 * @param {object} data 
 * @returns {object}
 */
function parseTdchange (data) {
  const result = {
    units: validateInteger(data.units, 0, 1) ? data.units : 0,
    material: validateInteger(data.material, 0, 8) ? data.material : 0,
    states: [],
    energies: []
  }
  if (data.states && Array.isArray(data.states)) {
    for (const state of data.states) {
      result.states.push({
        "T": validateNumber(state.T, -300, 1E6) ? state.T : 0,
        "p": validateNumber(state.p, 1E-10, 1E10) ? state.p : 1,
        "V": validateNumber(state.V, 1E-10, 1E10) ? state.V : 1,
        "change": validateInteger(state.change, 1, 4) ? state.change : 1
      })
    }
  }
  if (data.energies && Array.isArray(data.energies)) {
    for (const energy of data.energies) {
      result.energies.push({
        "U": validateNumber(energy.U, -1E10, 1E10) ? energy.U : 0,
        "Q": validateNumber(energy.Q, -1E10, 1E10) ? energy.Q : 0,
        "W": validateNumber(energy.W, -1E10, 1E10) ? energy.W : 0
      })
    }
  }
  return result
}

/**
 * Check if the passed ID belongs to an activity (otherwise it belongs to an example) 
 * @param {string} id - id to be checked
 * @returns {boolean}
 */
export function isActivityId (id) {
  return id && id.match(/^[a-f0-9]{64}$/)
}

