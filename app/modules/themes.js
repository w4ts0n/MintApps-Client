/**
 * Information about the available MintApps themes
 * @author Thomas Kippenberg
 * @module themes
 */

import * as storage from '../modules/storage.js'

/** current theme */
let theme
/** List of listener functions (to receive notification on theme changes) */
let listeners = []

/**
 * array of objects, containing available themes;
 * the keys (light, dark, contrast) correspond to the available options;
 * each entry contains an attribute text (translation key)
 * and an attribute icon (img src for icon)
 */
export const allThemes = {
  light: { text: 'Menus.light', icon: 'theme-light.svg' },
  dark: { text: 'Menus.dark', icon: 'theme-dark.svg' },
  contrast: { text: 'Menus.contrast', icon: 'theme-contrast.svg' }
}

/**
 * Initialize the theme module by determining and loading the initial theme (based on user preferences
 */
export function setup() {
  theme = storage.get('theme')
  if (!theme && window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) theme = 'dark'
  if (!theme) theme = 'light'
  setTheme(theme)
  window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
    setTheme(e.matches ? 'dark' : 'light')
  })
}

/**
 * Set new theme
 * @param {String} newTheme - id of new theme
 * @see allThemes
 */
export function setTheme(newTheme) {
  theme = newTheme
  storage.set('theme', theme)
  window.mintapps.container.setAttribute('data-theme', theme)
  // inform listeners
  listeners.forEach(listener => listener(theme))
}

/**
 * Get current theme
 * @return {String} id of new theme
 * @see allThemes
 */
export function getTheme() {
  return theme
}

/**
 * Watch for theme changes
 * @param {Function} listener - listener function (receiving new value)
 */
export function watch(listener) {
  listeners.push(listener)
}
