/**
 * Collection of methods to draw two-dimensional graphics on a canvas, with support for coordinate axes
 * @module plot-2d
 * @author Thomas Kippenberg
*/

import * as color from './color.js'
import { useDecimalSeparatorComma } from './i18n.js'
import * as fontsizes from './fontsizes.js'

const CANVAS_LINE_STYLES = [[0, 1], [], [4, 2], [2, 4]] // line styles
const HDPI_SCALE = 2
const OFFSET = 5

// default values for style, overriden in init ()
let colors = color.getColors()
let fonts = fontsizes.getFontInfo()
let alreadyInitialized = false // avoid multiple initialization of this module

/**
 * Initialize modules, especially loading the required fonts (using the CSS variables --font-family-math and --font-family-text)
 */
export function init() {
  if (alreadyInitialized) return
  alreadyInitialized = true
  colors = color.getColors()
  fonts = fontsizes.getFontInfo()
}

/**
 * Get math font familiy (specified in CSS variable --font-family-math)
 * @returns {string}
 */
export function getFontFamilyMath() {
  return fonts.math.family
}

/**
 * Get text font familiy (specified in CSS variable --font-family-text)
 * @returns {string}
 */
export function getFontFamilyText() {
  return fonts.text.family
}

/**
 * Get text font size (specified in CSS variable --font-size-text)
 * @returns {number}
 */
export function getFontSizeText() {
  return fonts.text.size
}

/**
 * Get math font size (specified in CSS variable --font-size-math)
 * @returns {number}
 */
export function getFontSizeMath() {
  return fonts.math.size
}

/**
 * Get array of Mintapp colors as hex codes by resolving the css definitions
 * @returns {string[]} 
 * @see module:color.getColors
 */
export function getColors() {
  return colors
}

/**
 * Format number and use correct decimal separator
 * @param {number} x - number value
 * @param {number} precision - desired precision of number representation
 * @param {boolean} trim - optionally remove zeros at the end
 * @returns {string}
 */
export function formatNumber(x, precision = 5, trim = true) {
  x = Number(x)
  if (isNaN(x)) return ''
  else if (x === 0) return 0
  else if (trim && Math.abs((Math.round(x) - x) / x) < 1e-10) { return Math.round(x) }
  let result = x.toFixed(precision)
  if (trim) {
    while (result.charAt(result.length - 1) === '0') {
      result = result.substring(0, result.length - 1)
    }
  }
  if (result.endsWith('.')) result = result.slice(0, -1)
  if (useDecimalSeparatorComma()) { result = result.toString().replaceAll('.', ',') }
  return result
}

/**
 * Create empty HTML canvas
 * @param {HTMLElement} parent - HTML parent element
 * @param {number} width - width in px
 * @param {number} height - height in px
 * @param {string} id - uniqui
 * @param {boolean} hdpi - use hdpi mode (slow, but more detailed representation)
 * @param {string} title  - title of the accessibility canvas
 * @returns {HTMLCanvasElement}
 */
export function createCanvas(parent, width, height, id, hdpi, title) {
  parent.style.position = 'relative' // bug fix for safari
  parent.style.marginLeft = 'auto'
  parent.style.marginRight = 'auto'
  // use hidpi ?
  const dpiScale = hdpi === 'force' || (hdpi && isRetina()) ? HDPI_SCALE : 1 // ignore hidpi request on non retina screens
  // create canvas and append to parent
  const canvas = document.createElement('canvas')
  canvas.setAttribute('aria-label', title || id)
  canvas.style.position = 'absolute' // enable stack of canvas objects
  canvas.style.width = `${width}px`
  canvas.style.height = `${height}px`
  canvas.width = dpiScale * width
  canvas.height = dpiScale * height
  canvas.style.padding = '0px'
  canvas.id = id
  canvas.dataset.hd = !!hdpi
  parent.appendChild(canvas)
  if (dpiScale > 1) canvas.getContext('2d').scale(dpiScale, dpiScale)
  // set font
  const ctx = canvas.getContext('2d')
  ctx.font = `${fonts.text.size}px ${fonts.text.family}`
  return canvas
}

/**
 * Fill canvas mit given color
 * @param {HTMLCanvasElement} canvas - HTML Canvas Object
 * @param {number} color - index of color
 * @see module:color.getColors
 */
export function fillCanvas(canvas, color) {
  const ctx = canvas.getContext('2d')
  ctx.fillStyle = colors[color]
  ctx.fillRect(0, 0, canvas.width, canvas.height)
}

/**
 * Clear canvas
 * @param {HTMLCanvasElement} canvas - HTML Canvas Object
 */
export function clearCanvas(canvas) {
  const ctx = canvas.getContext('2d')
  ctx.clearRect(0, 0, canvas.width, canvas.height)
}

/**
 * Draw circular disc with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context
 * @param {object} disc - parameter object
 * @param {number} disc.r - radius in px
 * @param {number} disc.x - center x coordinate in px
 * @param {number} disc.y - center y coordinate in px
 * @param {number} [disr.phi0=0] - starting angle
 * @param {number} [disr.phi1=2pi] - end angle
 * @see module:plot-2d.setStyle
 */
export function drawDisc(ctx, disc) {
  ctx.beginPath()
  ctx.arc(disc.x, disc.y, disc.r, disc.phi0 || 0, disc.phi1 || 2 * Math.PI)
  setStyle(ctx, disc)
  if (disc.fill) ctx.fill()
  ctx.stroke()
}

/**
 * Draw circular disc (x,y,r,fill,color,fillColor,lineWidth)
 * @param {CanvasRenderingContext2D} ctx - rendering context
 * @param {object} coords - definition of the coordinate system
 * @param {object} disc - parameter object
 * @param {number} disc.r - radius
 * @param {number} disc.x - center x coordinate 
 * @param {number} disc.y - center y coordinate
 * @param {number} [disr.phi0=0] - starting angle
 * @param {number} [disr.phi1=2pi] - end angle
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawDiscCoords(ctx, coords, disc) {
  const pixelX = calcPixelX(coords, disc.x)
  const pixelY = calcPixelY(coords, disc.y)
  const rX = ((coords.width - 2 * OFFSET) * disc.r) / (coords.x.max - coords.x.min)
  const rY = ((coords.height - 2 * OFFSET) * disc.r) / (coords.y.max - coords.y.min)
  ctx.beginPath()
  ctx.ellipse(pixelX, pixelY, rX, rY, 0, 0, 2 * Math.PI)
  setStyle(ctx, disc)
  if (disc.fill) ctx.fill()
  ctx.stroke()
}

/**
 * Draw rect with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} rect - parameter object
 * @param {number} rect.x - upper left corner, x coordinate in px
 * @param {number} rect.y - upper left corner, y coordinate in px
 * @param {number} rect.width - widht in px
 * @param {number} rect.height - height in px
 * @see module:plot-2d.setStyle
 */
export function drawRect(ctx, rect) {
  ctx.beginPath()
  setStyle(ctx, rect)
  ctx.rect(rect.x, rect.y, rect.width, rect.height)
  if (rect.fill) ctx.fill()
  ctx.stroke()
}

/**
 * Draw rect
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} rect - parameter object
 * @param {number} rect.x - upper left corner, x coordinate
 * @param {number} rect.y - upper left corner, y coordinate
 * @param {number} rect.width - widht
 * @param {number} rect.height - height
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawRectCoords(ctx, coords, rect) {
  const pixelX0 = calcPixelX(coords, rect.x)
  const pixelY0 = calcPixelY(coords, rect.y)
  const pixelX1 = calcPixelX(coords, rect.x + rect.width)
  const pixelY1 = calcPixelY(coords, rect.y + rect.height)
  const r = structuredClone(rect)
  r.x = pixelX0
  r.y = pixelY0
  r.width = pixelX1 - pixelX0
  r.height = pixelY1 - pixelY0
  drawRect(ctx, r)
}

/**
 * Draw line with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context  
 * @param {object} line - parameter object
 * @param {number} line.x0 - start point x-coordinate in px
 * @param {number} line.y0 - start point y-coordinate in px
 * @param {number} line.x1 - end point x-coordinate in px
 * @param {number} line.y1 - end point y-coordinate in px
 * @see module:plot-2d.setStyle
 */
export function drawLine(ctx, line) {
  ctx.beginPath()
  setStyle(ctx, line)
  ctx.moveTo(line.x0, line.y0)
  ctx.lineTo(line.x1, line.y1)
  ctx.stroke()
}

/**
 * Draw line
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} line - parameter object
 * @param {number} line.x0 - start point x-coordinate
 * @param {number} line.y0 - start point y-coordinate
 * @param {number} line.x1 - end point x-coordinate
 * @param {number} line.y1 - end point y-coordinate
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawLineCoords(ctx, coords, line) {
  const l = structuredClone(line)
  l.x0 = calcPixelX(coords, line.x0)
  l.y0 = calcPixelY(coords, line.y0)
  l.x1 = calcPixelX(coords, line.x1)
  l.y1 = calcPixelY(coords, line.y1)
  drawLine(ctx, l)
}

/**
 * Draw arrow with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context  
 * @param {object} arrow - parameter object
 * @param {number} arrow.x0 - start point x-coordinate in px
 * @param {number} arrow.y0 - start point y-coordinate in px
 * @param {number} arrow.x1 - end point x-coordinate in px
 * @param {number} arrow.y1 - end point y-coordinate in px
 * @param {number} [arrow.head=10] - size of head in px
 * @see module:plot-2d.setStyle
 */
export function drawArrow(ctx, arrow) {
  const head = arrow.head ? arrow.head : 10 // size of head in px
  const angle = 30 // angle of head lines in px
  setStyle(ctx, arrow)
  // main line
  drawLine(ctx, arrow)
  // head
  ctx.beginPath()
  ctx.moveTo(arrow.x1, arrow.y1)
  const a = Math.PI * (1 + angle / 180)
  const cos = Math.cos(a)
  const sin = Math.sin(a)
  let vx = arrow.x1 - arrow.x0
  let vy = arrow.y1 - arrow.y0
  const v = Math.sqrt(vx * vx + vy * vy)
  vx = vx / v
  vy = vy / v
  const ax = vx * cos - vy * sin
  const ay = vx * sin + vy * cos
  const bx = vx * cos + vy * sin
  const by = -vx * sin + vy * cos
  ctx.lineTo(arrow.x1 + head * ax, arrow.y1 + head * ay)
  ctx.lineTo(arrow.x1 + head * bx, arrow.y1 + head * by)
  ctx.fill()
}

/**
 * Draw arrow
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} arrow - parameter object
 * @param {number} arrow.x0 - start point x-coordinate
 * @param {number} arrow.y0 - start point y-coordinate
 * @param {number} arrow.x1 - end point x-coordinate
 * @param {number} arrow.y1 - end point y-coordinate
 * @param {number} [arrow.head=10] - size of head in px
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawArrowCoords(ctx, coords, arrow) {
  const a = structuredClone(arrow)
  a.x0 = calcPixelX(coords, arrow.x0)
  a.y0 = calcPixelY(coords, arrow.y0)
  a.x1 = calcPixelX(coords, arrow.x1)
  a.y1 = calcPixelY(coords, arrow.y1)
  drawArrow(ctx, a)
}

/**
 * Draw point with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} point - parameter object
 * @param {number} point.x - x-coordinate in px
 * @param {number} point.y - y-coordinate in px
 * @see module:plot-2d.setStyle
 */
export function drawPoint(ctx, point) {
  const CROSS = 5
  setStyle(ctx, point)
  ctx.beginPath()
  ctx.moveTo(point.x - CROSS, point.y - CROSS)
  ctx.lineTo(point.x + CROSS, point.y + CROSS)
  ctx.moveTo(point.x - CROSS, point.y + CROSS)
  ctx.lineTo(point.x + CROSS, point.y - CROSS)
  ctx.stroke()
  if (point.label) {
    drawMath(ctx, {
      x: point.x - OFFSET,
      y: point.y - 10,
      size: fonts.math.size,
      text: point.label
    })
  }
}

/**
 * Draw point
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} point - parameter object
 * @param {number} point.x - x-coordinate
 * @param {number} point.y - y-coordinate
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawPointCoords(ctx, coords, point) {
  const p = structuredClone(point)
  p.x = calcPixelX(coords, p.x)
  p.y = calcPixelY(coords, p.y)
  drawPoint(ctx, p)
}

/**
 * Draw crosshair with absolute coordinates
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} point - parameter object
 * @param {number} point.x - x-coordinate in px
 * @param {number} point.y - y-coordinate in px
 * @see module:plot-2d.setStyle
 */
export function drawCrossHair(ctx, point) {
  setStyle(ctx, point)
  for (let i = 0; i < 2; i++) {
    ctx.beginPath()
    ctx.setLineDash(i === 0 ? [] : [4, 4])
    ctx.strokeStyle = i === 0 ? colors[6] : colors[4]
    ctx.moveTo(0, point.y)
    ctx.lineTo(1e4, point.y)
    ctx.moveTo(point.x, 0)
    ctx.lineTo(point.x, 1e4)
    ctx.stroke()
  }
}

/**
 * Draw crosshair
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} point - parameter object
 * @param {number} point.x - x-coordinate
 * @param {number} point.y - y-coordinate
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawCrossHairCoords(ctx, coords, point) {
  const p = structuredClone(point)
  p.x = calcPixelX(coords, p.x)
  p.y = calcPixelY(coords, p.y)
  drawCrossHair(ctx, p)
}

/**
 * Draw a trace through a given set of points
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object[]} trace.points - array of points [{ x: 0, y: 0}, {x: 1, y: 2}, ...]
 * @see module:plot-2d.setStyle
 * @see module:plot-2d.drawCoordinates
 */
export function drawTraceCoords(ctx, coords, trace) {
  setStyle(ctx, trace)
  ctx.beginPath()
  let pixelX, pixelY
  for (let i = 0; i < trace.points.length; i++) {
    pixelX = calcPixelX(coords, trace.points[i].x)
    pixelY = calcPixelY(coords, trace.points[i].y)
    if (i === 0) ctx.moveTo(pixelX, pixelY)
    else ctx.lineTo(pixelX, pixelY)
  }
  ctx.stroke()
}

/**
 * Draw coordinate system
 * @param {CanvasRenderingContext2D} ctx - rendering context
 * @param {object} coords - definition of the coordinate system
 * @param {number} coords.width - width in px
 * @param {number} coords.height - height in px
 * @param {number} coords.border - wether to draw a border (1) or not (0)
 * @param {object} coords.x - definition of x axis
 * @param {number} coords.x.min - start value x axis
 * @param {number} coords.x.max - end value x axis
 * @param {string} coords.x.text - label of x axis
 * @param {number} [coords.x.step] - step between ticks (0 means no ticks at all)
 * @param {number} [coords.x.precision] - precision of number representation
 * @param {number} [cords.grid=0] - draw grid, number of intermediate steps between two steps (0 no grid)
 * @param {object} coords.y - definition of the y-axis, corresponding to the x-axis
 */
export function drawCoordinates(ctx, coords) {
  if (coords.hide) return
  let pixelX, pixelY
  const width = coords.width
  const height = coords.height
  // color
  const col = 4
  const colGrid = 6
  // format
  ctx.strokeStyle = colors[col]
  ctx.lineWidth = 1
  const mathFont = fonts.math.size + 'px ' + fonts.math.family
  const fs = fonts.math.size
  ctx.font = mathFont
  // auto determine steps if not defined
  let xStep = coords.x.step
  if (!coords.x.hide && !xStep && xStep !== 0) {
    xStep = Math.pow(10, Math.floor(Math.log10(coords.x.max - coords.x.min)))
    const stepXPixel = (width * xStep) / (coords.x.max - coords.x.min)
    const mixPixel = coords.x.max > 1000 || coords.x.max < 0.1 ? 60 : 40
    if (stepXPixel > 10 * mixPixel) xStep /= 10
    else if (stepXPixel > 5 * mixPixel) xStep /= 5
    else if (stepXPixel > 2 * mixPixel) xStep /= 2
  }
  let yStep = coords.y.step
  if (!coords.y.hide && !yStep && yStep !== 0) {
    yStep = Math.pow(10, Math.floor(Math.log10(coords.y.max - coords.y.min)))
    const stepYPixel = (height * yStep) / (coords.y.max - coords.y.min)
    if (stepYPixel > 400) yStep /= 10
    else if (stepYPixel > 200) yStep /= 5
    else if (stepYPixel > 80) yStep /= 2
  }
  // auto set precision if not defined
  if (!coords.x.precision) coords.x.precision = 3
  if (!coords.y.precision) coords.y.precision = 3
  // grid
  if (coords.x.grid > 0 && xStep > 0) {
    const xGridStart = (Math.ceil((coords.x.min / xStep) * coords.x.grid) * xStep) / coords.x.grid
    for (let x = xGridStart; x <= coords.x.max; x += xStep / coords.x.grid) {
      pixelX = calcPixelX(coords, x)
      drawLine(ctx, {
        x0: pixelX,
        y0: OFFSET,
        x1: pixelX,
        y1: height - OFFSET,
        color: Math.abs(x) < 1e-3 * xStep ? col : colGrid
      })
    }
  }
  if (coords.y.grid > 0 && yStep > 0) {
    const yGridStart = (Math.ceil((coords.y.min / yStep) * coords.y.grid) * yStep) / coords.y.grid
    for (let y = yGridStart; y <= coords.y.max; y += yStep / coords.y.grid) {
      pixelY = calcPixelY(coords, y)
      drawLine(ctx, {
        x0: OFFSET,
        y0: pixelY,
        x1: width - OFFSET,
        y1: pixelY,
        color: Math.abs(y) < 1e-3 * yStep ? col : colGrid
      })
    }
  }
  // draw x- and y-axis
  const x0 = xStep !== 0 ? Math.ceil(coords.x.min / xStep) * xStep : coords.x.min
  const y0 = yStep !== 0 ? Math.floor(coords.y.min / yStep) * yStep : coords.y.min
  // const wideLabels = false // coords.x && (xStep < 0.1 || xStep >= 100)
  if (coords.border !== 1) {
    // x-axis
    if (!coords.x.hide) {
      // arrow
      pixelY = calcPixelY(coords, 0)
      drawArrow(ctx, {
        x0: 0,
        y0: pixelY,
        x1: width,
        y1: pixelY,
        color: col
      })
      let xTextWidth = 0
      if (coords.x.text) {
        xTextWidth = calcTextWidth(coords.x.text, fs) + fs
        drawMath(ctx, {
          x: width - OFFSET - xTextWidth,
          y: pixelY - 0.5 * fs,
          size: fs,
          text: coords.x.text
        })
      }
      ctx.font = mathFont
      // ticks
      if (xStep > 0) {
        for (let x = x0; x < coords.x.max; x += xStep) {
          if (Math.abs(x / xStep) < 0.5) x = 0
          pixelX = calcPixelX(coords, x)
          drawLine(ctx, {
            x0: pixelX,
            y0: pixelY - 3,
            x1: pixelX,
            y1: pixelY + 3,
            color: col
          })
          const xNumber = optimizeNumber(x, 4)
          const yOffset = pixelY < height - 1.2 * fs ? 1.1 * fs : -0.5 * fs
          const xNumberWidth = xNumber.toString().length * 0.25 * fs
          const skipLabel =
            pixelX < 0.5 * fs || 
            x === 0 || // skip value zero
            (yOffset < 0 && pixelX > width - xTextWidth - xNumberWidth)
          // || (wideLabels && Math.round((x - x0) / xStep) % 2 !== 0)
          if (!skipLabel) ctx.fillText(xNumber, pixelX - xNumberWidth, pixelY + yOffset)
        }
      }
    }
    // y-axis
    if (!coords.y.hide) {
      // arrow
      pixelX = calcPixelX(coords, 0)
      drawArrow(ctx, {
        x0: pixelX,
        y0: height + OFFSET,
        x1: pixelX,
        y1: 0,
        color: col
      })
      if (coords.y.text) {
        drawMath(ctx, {
          x: pixelX + 6,
          y: 1 * fs,
          size: fs,
          text: coords.y.text
        })
      }
      // ticks
      ctx.font = mathFont
      if (yStep > 0) {
        for (let y = y0; y < coords.y.max; y += yStep) {
          if (Math.abs(y / yStep) < 0.5) y = 0
          pixelY = calcPixelY(coords, y)
          drawLine(ctx, {
            x0: pixelX - 3,
            y0: pixelY,
            x1: pixelX + 3,
            y1: pixelY,
            color: col
          })
          const yNumber = optimizeNumber(y, coords.y.precision)
          const yNumberWidth = yNumber.toString().length * fs * 0.5
          if (pixelY < height - 0.5 * fs && pixelY >= 8 && y !== 0) {
            if (pixelX - 4 - yNumberWidth >= 0) {
              ctx.fillText(yNumber, pixelX - 4 - yNumberWidth, pixelY + 0.4 * fs)
            } else if (pixelY > 32) {
              ctx.fillText(yNumber, pixelX + 4, pixelY + 0.4 * fs)
            }
          }
        }
      }
    }
    // draw border with ticks and scale
  } else {
    // box
    ctx.beginPath()
    ctx.strokeStyle = colors[col]
    ctx.rect(OFFSET, OFFSET, width - 2 * OFFSET, height - 2 * OFFSET)
    ctx.stroke()
    // horizontal scale
    ctx.font = mathFont
    if (!coords.x.text) coords.x.text = ''
    const xTextWidth = calcTextWidth(coords.x.text, fs)
    if (xStep > 0) {
      for (let x = x0; x < coords.x.max; x += xStep) {
        if (Math.abs(x / xStep) < 0.5) x = 0
        pixelX = calcPixelX(coords, x)
        drawLine(ctx, {
          x0: pixelX,
          y0: height - 3 - OFFSET,
          x1: pixelX,
          y1: height + 3 - OFFSET,
          color: col
        })
        const xText = String(optimizeNumber(x, 4))
        const skipLabel =
          pixelX < 3 * fs ||
          pixelX > width - OFFSET - xTextWidth
        // || (wideLabels && Math.round((x - x0) / xStep) % 2 !== 0)
        if (!skipLabel) {
          ctx.fillText(xText, pixelX - xText.length * 0.3 * fs, height - 6 - OFFSET)
        }
      }
    }
    if (coords.x.text) {
      drawMath(ctx, {
        x: width - OFFSET - xTextWidth,
        y: height - 6 - OFFSET,
        size: fs,
        text: coords.x.text
      })
    }
    // vertical scale
    ctx.font = mathFont
    if (yStep > 0) {
      for (let y = y0; y < coords.y.max; y += yStep) {
        if (Math.abs(y / yStep) < 0.5) y = 0
        pixelY = calcPixelY(coords, y)
        drawLine(ctx, {
          x0: OFFSET - 3,
          y0: pixelY,
          x1: OFFSET + 3,
          y1: pixelY,
          color: col
        })
        const yText = optimizeNumber(y, coords.y.precision)
        if (pixelY < height - fs && pixelY >= 1.5 * fs + OFFSET) {
          ctx.fillText(yText, OFFSET + 5, pixelY + 6)
        }
      }
      if (coords.y.text) {
        drawMath(ctx, {
          x: 6 + OFFSET,
          y: 1 * fs + OFFSET,
          size: fs,
          text: coords.y.text
        })
      }
    }
  }
}

/**
 * Format number in a human readable way, use powers of ten when appropriate, for example 1.3·10⁴
 * @param {number} n - number value
 * @param {number} [precision=3] - precision of number representation
 * @param {number} [asciimath=false] - use asciimath syntax
 * @returns {string}
 */
export function optimizeNumber(n, precision = 3, asciimath = false) {
  const SUP = { 0: '⁰', 1: '¹', 2: '²', 3: '³', 4: '⁴', 5: '⁵', 6: '⁶', 7: '⁷', 8: '⁸', 9: '⁹', '-': '⁻', '+': '' }
  let result = ''
  n = Number(n)
  if (n === 0) {
    return '0'
  } else if (Math.abs(n) <= 1e-3 || Math.abs(n) >= 1e3) {
    const log10 = Math.floor(Math.log10(Math.abs(n)))
    const e = String(log10)
    if (asciimath) { 
      result = Number((n / 10 ** log10).toPrecision(precision)) + '*10^(' + e + ')'
    }
    else {
      let superE = ''
      for (let i = 0; i < e.length; i++) superE += SUP[e.charAt(i)]
      result = Number((n / 10 ** log10).toPrecision(precision)) + '·10' + superE
    }
  } else {
    result = Number(n.toPrecision(precision))
  }
  if (useDecimalSeparatorComma()) { result = result.toString().replaceAll('.', ',') }
  return result
}

/**
 * Draw mathematical function in given coordinate system
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} func - function definition
 * @param {function} func.f - mathematical function (with one paramater, for example '(x) => x^2')
 * @param {number} {func.pixelStep=1} - number of calculation steps per pixel on the screen
 * @see module:plot-2d.drawCoordinates
 */
export function drawFunction(ctx, coords, func) {
  let firstPixel = true
  setStyle(ctx, func)
  ctx.beginPath()
  let pixelX, pixelY
  const pixelStep = func.pixelStep ? func.pixelStep : 1
  for (pixelX = OFFSET; pixelX < coords.width - OFFSET; pixelX += pixelStep) {
    const x = calcX(coords, pixelX)
    const y = func.f(x)
    if (isNaN(y)) firstPixel = true
    else {
      pixelY = calcPixelY(coords, y)
      if (pixelY < -1E4 || pixelY > 1E4) {
        firstPixel = true
      } else if (typeof pixelX === 'number' && typeof pixelY === 'number') {
        if (firstPixel) {
          ctx.moveTo(pixelX, pixelY)
          firstPixel = false
        } else ctx.lineTo(pixelX, pixelY)
      }
    }
  }
  ctx.stroke()
}

/**
 * Draw legend of graph
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} legend - defintion of legend, for example [{ color: 1, lineWidth: 2, math: 'x' }
 * @see module:plot-2d.setStyle
 * @deprecated bad math support, no longer used in MintApps, see src/components/GraphLegend.vue
 */
export function drawLegend(ctx, coords, legend) {
  if (!coords || !legend || legend.length === 0) return
  const fs = fonts.text.size
  // calc dimensions
  const lineHeight = 1.3 * fonts.text.family
  const height = lineHeight * (0.5 + legend.length)
  let width = 0
  for (const entry of legend) {
    let w = 0
    if (entry.text) w += calcTextWidth(entry.text, fonts.text.size)
    if (entry.math) w += calcTextWidth(entry.math, fonts.math.size) * 0.9
    width = Math.max(width, w)
  }
  if (width === 0) return // no labels found

  width += 2.2 * fonts.text.family
  const x0 = coords.width - width - 1
  const y0 = 1
  // box
  ctx.beginPath()
  ctx.strokeStyle = colors[6]
  ctx.fillStyle = colors[5]
  ctx.lineWidth = 1
  ctx.rect(x0, y0, width, height)
  ctx.fill()
  ctx.stroke()
  // entries
  for (let i = 0; i < legend.length; i++) {
    const entry = legend[i]
    ctx.beginPath()
    setStyle(ctx, entry)
    ctx.moveTo(x0 + 0.5 * fs, y0 + 0.7 * lineHeight + i * lineHeight)
    ctx.lineTo(x0 + 1.5 * fs, y0 + 0.7 * lineHeight + i * lineHeight)
    ctx.stroke()
    let x = x0 + 2 * fs
    if (entry.text) {
      x = drawText(ctx, { x, y: y0 + 1 * lineHeight + i * lineHeight, text: entry.text })
    }
    if (entry.math) {
      drawMath(ctx, { x, y: y0 + 1 * lineHeight + i * lineHeight, text: entry.math })
    }
  }
}

/**
 * draw text on canvas, supporting markdown syntax for subscript, superscript and italic font
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} text - parameter object
 * @param {string} text.text - text
 * @param {number} text.x - x coordinate in px
 * @param {number} text.y - y coordinate in px
 * @param {number} [text.size] - font size in px
 * @param {string} [text.color] - color of text
 * @see module:plot-2d.drawCoordinates
 */
export function drawText(ctx, text) {
  if (!text.text) return
  let sub = false
  let sup = false
  let italic = false
  let y = text.y
  let x = text.x
  const t = text.text.replaceAll('``', '`') // single delimiter for asciimath mode
  const size = text.size ? text.size : fonts.text.size
  if (text.color) ctx.fillStyle = colors[text.color]
  for (let i = 0; i < t.length; i++) {
    const c = t.charAt(i)
    // markdown subscript
    if (c === '~') sub = !sub
    // markdown superscript
    else if (c === '^') sup = !sup
    // markdown emphasis
    else if (c === '*') {
      italic = !italic
      if (!italic) x += 1
    } else if (c === '`') {
      // math
      let j = t.indexOf('`', i + 1)
      if (j === -1) j = t.length
      x = drawMath(ctx, {
        text: t.substring(i + 1, j),
        x,
        y,
        size: text.size
      })
      i = j
    }
    // normal text
    else {
      // set font
      let font = ''
      if (italic) font += 'italic '
      font += sub || sup ? Math.round(size * 0.7) + 'px' : Math.round(size) + 'px'
      font += ' ' + fonts.text.family
      ctx.font = font
      // vertical position
      y = text.y + (sub ? 0.1 * size : 0) + (sup ? -0.4 * size : 0)
      ctx.fillText(c, x, y)
      // horizontal position
      x += ctx.measureText(c).width
    }
  }
  ctx.font = fonts.math.size + 'px ' + fonts.math.family
  return x
}

/**
 * draw text on canvas, supporting markdown syntax for subscript, superscript and italic font
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} text - parameter object
 * @param {string} text.text - text
 * @param {number} text.x - x coordinate
 * @param {number} text.y - y coordinate
 * @param {number} [text.size] - font size in px
 * @param {string} [text.color] - color of text
 * @see module:plot-2d.drawCoordinates
 */
export function drawTextCoords(ctx, coords, text) {
  const t = structuredClone(text)
  t.x = calcPixelX(coords, t.x)
  t.y = calcPixelY(coords, t.y)
  drawText(ctx, t)
}

/**
 * draw math expression, offering basic format options similar to asciimath
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} text - parameter object
 * @param {string} text.text - text
 * @param {number} text.x - x coordinate in px
 * @param {number} text.y - y coordinate in px
 * @param {number} [text.size] - font size in px
 * @param {string} [text.color] - color of text
 * @see module:plot-2d.drawCoordinates
 */
export function drawMath(ctx, text) {
  if (text.color) ctx.fillStyle = colors[text.color]
  const size = text.size ? text.size : fonts.math.size
  const t = text.text.replaceAll('unit', '␟')
  let y = text.y
  let x = text.x
  let isText = false
  let isUnit = false
  let isNumber = false
  let isGreek = false
  let lastC = ''
  for (let i = 0; i < t.length; i++) {
    let c = t.charAt(i)
    let spacingLeft = 0
    let spacingRight = 0
    // numbers
    if (/\d|[¹²³¼½]/.test(c)) isNumber = true
    // greek letter
    else if (c >= 'Α' && c <= 'ϖ') isGreek = true
    // operators
    else if (/[/:+*=-]/.test(c)) {
      spacingLeft = 0.1
      spacingRight = 0.1
      if (c === '*') c = '·'
    }
    // brackets
    else if (/[({})|]/.test(c)) {
      spacingLeft = 0.05
      spacingRight = 0.05
    }
    // spacing comma and semicolon
    else if (c === ',') spacingRight = 0.05
    else if (c === ';') spacingRight = 0.2
    // units
    else if (c === '␟') {
      const ama = getAsciiMathArgument(t, i)
      isUnit = true
      c = ama.text
      i = ama.pos
      if (lastC !== '/') c = ' ' + c
    }
    // text mode
    else if (c === '"') {
      let j = t.indexOf('"', i + 1)
      if (j < 0) j = t.length
      c = t.substring(i + 1, j)
      i = j
      isText = true
      spacingLeft = 0.1
    }
    // vector
    else if (c === 'v' && t.indexOf('vec', i) === i) {
      ctx.font = `${Math.round(size)}px ${fonts.math.family}`
      ctx.fillText('→', x - 0.1 * size, y - 0.7 * size)
      i += 2
      c = ''
    }
    // superscript
    else if (c === '^') {
      const ama = getAsciiMathArgument(t, i)
      x = drawMath(ctx, {
        text: ama.text,
        x: x + 0.1 * size,
        y: y - 0.4 * size,
        size: 0.7 * size
      })
      i = ama.pos
      c = ''
    }
    // subscript
    else if (c === '_') {
      const ama = getAsciiMathArgument(t, i)
      x = drawMath(ctx, {
        text: ama.text,
        x: x + 0.1 * size,
        y: y + 0.1 * size,
        size: 0.7 * size
      })
      i = ama.pos
      c = ''
    }
    // skip blanks
    if (c === ' ' && !isText) {
      c = lastC
    }
    // draw text / character
    else {
      let font = ''
      if (!isText && !isUnit && !isNumber && !isGreek && !/[(){}|=/]/.test(c)) { font += 'italic ' }
      font += Math.round(size) + 'px'
      font += ' ' + fonts.math.family
      ctx.font = font
      // vertical position
      y = text.y
      // horizontal position
      x += Math.round(size * spacingLeft)
      ctx.fillText(c, x, y)
      x += Math.round(size * spacingRight)
      x += ctx.measureText(c).width
    }
    lastC = c
    isUnit = false
    isText = false
    isNumber = false
  }
  return x
}

/**
 * draw text on canvas, supporting markdown syntax for subscript, superscript and italic font
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} coords - definition of the coordinate system
 * @param {object} text - parameter object
 * @param {string} text.text - text
 * @param {number} text.x - x coordinate
 * @param {number} text.y - y coordinate
 * @param {number} [text.size] - font size in px
 * @param {string} [text.color] - color of text
 * @see module:plot-2d.drawCoordinates
 */
export function drawMathCoords(ctx, coords, text) {
  const t = structuredClone(text)
  t.x = calcPixelX(coords, t.x)
  t.y = calcPixelY(coords, t.y)
  drawMath(ctx, t)
}

/**
 * get argument of function by parsing brackets
 * @param {string} t - text to parse
 * @param {number} startPos - start position for search
 * @returns {string} substring = argument
 */
function getAsciiMathArgument(t, startPos) {
  let pos = startPos
  // string ends - argument is missing
  if (pos + 1 >= t.length) {
    return { text: '', pos: startPos }
  }
  // check for delimiter
  if (pos + 2 < t.length && /["({"]/.test(t.charAt(pos + 1))) {
    const d0 = t.charAt(pos + 1)
    let d1 = d0
    if (d0 === '(') d1 = ')'
    else if (d0 === '{') d1 = '}'
    let level = 0
    while (pos < t.length) {
      pos++
      if (t.charAt(pos) === d0) level++
      if (t.charAt(pos) === d1) level--
      if (level === 0) break
    }
    return { text: t.substring(startPos + 2, pos), pos }
  }
  // single character without delimiter as argument
  return { text: t.substring(startPos + 1, startPos + 2), pos: pos + 1 }
}

/**
 * Generate random id.
 * @returns {string} id
 */
export function getRandomId() {
  return Math.random()
}

/**
 * Parse style information of element and set style.
 * This function is called by all routines that draw geometric objects on the canvas.
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {object} element - element to parse (line, rect, etc.)
 * @param {number} [element.lineWidth] - width of lines in px
 * @param {number} [element.lineStyle] - line style (0 = none, 1 = solid, 2 = dahsed, 3 = dotted)
 * @param {number} [element.color] - color of lines
 * @param {number} [element.fillColor] - fill color of object
 */
export function setStyle(ctx, element) {
  // line width
  if (element.lineWidth === 0) ctx.lineWidth = 1e-10
  else if (element.lineWidth) ctx.lineWidth = element.lineWidth
  // line style
  if (element.lineStyle >= 0) { ctx.setLineDash(CANVAS_LINE_STYLES[element.lineStyle]) }
  else ctx.setLineDash([])
  // stroke color
  let color
  if (Number.isInteger(element.color)) color = colors[element.color]
  else if (element.color) color = element.color
  if (color) ctx.strokeStyle = color
  // fill color
  let fillColor
  if (Number.isInteger(element.fillColor)) { fillColor = colors[element.fillColor] }
  else if (element.fillColor) fillColor = element.fillColor
  if (fillColor) {
    ctx.fillStyle = fillColor
    element.fill = true
  } else if (color) ctx.fillStyle = color
}

/**
 * Convert relative x coordinate to absolute x coordinate
 * @param {object} coords - definition of the coordinate system
 * @param {number} x - relative x coordinate
 * @returns {number} x coordinate in px
 * @see module:plot-2d.drawCoordinates
 */
export function calcPixelX(coords, x) {
  return (
    OFFSET +
    (coords.width - 2 * OFFSET) *
    ((x - coords.x.min) / (coords.x.max - coords.x.min))
  )
}

/**
 * Convert relative y coordinate to absolute y coordinate
 * @param {object} coords - definition of the coordinate system
 * @param {number} y - relative y coordinate
 * @returns {number} y coordinate in px
 * @see module:plot-2d.drawCoordinates
 */
export function calcPixelY(coords, y) {
  return (
    coords.height -
    OFFSET -
    (coords.height - 2 * OFFSET) *
    ((y - coords.y.min) / (coords.y.max - coords.y.min))
  )
}

/**
 * Convert absolute x coordinate to relative x coordinate
 * @param {object} coords - definition of the coordinate system
 * @param {number} x - coordinate in px 
 * @returns {number} relative x coordinate
 * @see module:plot-2d.drawCoordinates
 */
export function calcX(coords, pixelX) {
  return (
    ((pixelX - OFFSET) / (coords.width - 2 * OFFSET)) *
    (coords.x.max - coords.x.min) +
    coords.x.min
  )
}

/**
 * Convert absolute y coordinate to relative y coordinate
 * @param {object} coords - definition of the coordinate system
 * @param {number} y - coordinate in px 
 * @returns {number} relative y coordinate
 * @see module:plot-2d.drawCoordinates
 */
export function calcY(coords, pixelY) {
  return (
    ((pixelY - OFFSET) / (coords.height - 2 * OFFSET)) *
    (coords.y.min - coords.y.max) +
    coords.y.max
  )
}

/**
 * Estimate width of text in px
 * @param {string} text 
 * @returns {number} estimated width in px
 */
function calcTextWidth(text, fontSize) {
  if (!text) return 0
  return (
    text.replace(/[$*~^_\\{\\}\\(\\)]|unit+/g, '').length * 0.4 * fontSize +
    0.5 * fontSize
  )
}

/**
 * Get x scale between absolute and relative coords
 * @param {object} coords - definition of the coordinate system
 * @returns {number}
 * @see module:plot-2d.drawCoordinates
 */
export function getScaleX(coords) {
  return (coords.width - 2 * OFFSET) / (coords.x.max - coords.x.min)
}

/**
 * Get y scale between absolute and relative coords
 * @param {object} coords - definition of the coordinate system
 * @returns {number}
 * @see module:plot-2d.drawCoordinates
 */
export function getScaleY(coords) {
  return -(coords.height - 2 * OFFSET) / (coords.y.max - coords.y.min)
}

/**
 * Get realtive position of touch or mouse click event
 * @param {Event} event - touch or mouse event
 * @param {HTMLElement} htmlElement - element for which the coordinates are to be specified
 * @returns {object} coordinates in px {x, y}
 */
export function getEventPosition(event, htmlElement) {
  const type = event.type
  const rect = htmlElement.getBoundingClientRect()
  let x, y
  // touch?
  if (type === 'touchstart' || type === 'touchend' || type === 'touchmove') {
    const touch = event.touches[0]
    x = touch.clientX - rect.left
    y = touch.clientY - rect.top
  } else if (
    type === 'click' ||
    type === 'mousedown' ||
    type === 'mouseup' ||
    type === 'mousemove'
  ) {
    x = event.offsetX
    y = event.offsetY
  }
  return { x, y }
}

/**
 * Get absolute position of touch or mouse click event
 * @param {Event} event - touch or mouse event
 * @returns {object} coordinates in px {x, y}
 */
export function getAbsoluteEventPosition(event) {
  const type = event.type
  if (type === 'touchstart' || type === 'touchend' || type === 'touchmove') {
    return { x: event.touches[0].clientX, y: event.touches[0].clientY }
  } else if (
    type === 'click' ||
    type === 'mousedown' ||
    type === 'mouseup' ||
    type === 'mousemove'
  ) {
    return { x: event.clientX, y: event.clientY }
  }
}

/**
 * Copy canvas contents as image to clipboard
 * @param {HTMLCanvasElement} canvas 
 */
export function copyImage(canvas) {
  return new Promise((resolve, reject) => {
    canvas.toBlob((blob) => {
      try {
        navigator.clipboard.write([new ClipboardItem({ 'image/png': blob })])
        resolve()
        // eslint-disable-next-line no-unused-vars
      } catch (e) {
        reject(new Error('Errors.not-supported-browser'))
      }
    })
  })
}

/**
 * Update colors (get colors again from css variables) and fontsize
 * @returns {string[]} - MintApp colors
 * @see module:color.getColors
 */
export function updateColors() {
  fonts = fontsizes.getFontInfo()
  colors = color.getColors()
  return colors
}

/**
 * Check if display is retina display
 * @returns {boolean} true, when hidpi or retina display
 */
export function isRetina() {
  if (window.devicePixelRatio > 1) return true
  const mediaQuery =
    '(-webkit-min-device-pixel-ratio: 1.5),(min--moz-device-pixel-ratio: 1.5),(-o-min-device-pixel-ratio: 3/2),(min-resolution: 1.5dppx)'
  return window.matchMedia && window.matchMedia(mediaQuery).matches
}

