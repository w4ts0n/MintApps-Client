/**
 * Module with several utility functions for handling sessions with the sync-server
 * @author Thomas Kippenberg
 * @module html
 */


import * as storage from '../modules/storage.js'
import * as http from '../modules/http.js'

/**
 * Check if user has vald session by reading the local storage and sending an reques to the sync server
 * @returns {Promise} resolves to session or null
 */
export function isValidSession () {
  return new Promise((resolve) => {
    const storedSession = storage.get('session')
    if (storedSession?.user) {
      http.post('sync:update-session', { user: storedSession.user, })
        .then(() => {
          resolve(storedSession)
        })
        .catch(() => {
          storage.set('session', {})
          resolve(null)
        })
    } else {
      resolve(null)
    }
  })
}

/**
 * Terminate existing session
 * @returns {Promise}
 */
export function terminateSession () {
  return new Promise((resolve) => {
    if (storage.get('session')?.user) {
      http.post('sync:delete-session', { user: storage.get('session').user })
      .then(resolve)
      .catch(e => {
        console.debug(e)
        resolve()
      })
      .finally(() => {
        storage.set('session', {})
      })
    } else {
      resolve()
    }
  })
}