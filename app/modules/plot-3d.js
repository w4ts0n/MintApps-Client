/**
 * Collection of methods to draw three-dimensional graphics using three.js
 * @module plot-3d
 * @author Thomas Kippenberg
 * @see https://threejs.org/
*/

import * as THREE from 'three'
import { OrbitControls } from 'OrbitControls'
import { CSS2DRenderer, CSS2DObject } from 'CSS2DRenderer'
import * as color from './color.js'
import * as plot2d from './plot-2d.js'
import { Line2, LineGeometry, LineMaterial } from 'three-fatline'
const HDPI_SCALE = 2

let scene
let renderer // 3d renderer
let labelRenderer // renderer for 2d texts
let controls // user controls for rotating and zooming
let camera // camera
let px // size of "one pixel" in world units
let colors = color.getColors()
let width, height // width and height of canvas
let scaleX, scaleY, scaleZ

/**
 * Create scene, add lights, camera and layer for labels
 * @param {HTMLElement} parent - parent HTML elment (usually div)
 * @param {string} [hdpi] - use 'force' to force hidpi scale, otherwise the resolution will be determined automatically
 * @returns {THREE.Scene}
 */
export function createScene(parent, hdpi) {
  const dpiScale = hdpi === 'force' || (hdpi && plot2d.isRetina()) ? HDPI_SCALE : 1 // ignore hidpi request on non retina screens
  width = parent.clientWidth
  height = parent.clientHeight
  // style parent div
  parent.style.position = 'relative' // bug fix for safari
  parent.style.marginLeft = 'auto'
  parent.style.marginRight = 'auto'
  // 3d renderer
  renderer = new THREE.WebGLRenderer({ antialias: true })
  renderer.setSize(width, height)
  renderer.setPixelRatio(dpiScale)
  renderer.setClearColor(colors[5], 1)
  parent.appendChild(renderer.domElement)
  // renderer for labels
  labelRenderer = new CSS2DRenderer()
  labelRenderer.setSize(width, height)
  labelRenderer.domElement.style.position = 'absolute'
  labelRenderer.domElement.style.top = '0px'
  parent.appendChild(labelRenderer.domElement)
  // create scene
  scene = new THREE.Scene()
  const lights = []
  lights[0] = new THREE.DirectionalLight(0xffffff, 1, 1)
  lights[1] = new THREE.DirectionalLight(0xffffff, 1, 1)
  lights[2] = new THREE.DirectionalLight(0xffffff, 1, 1)
  lights[3] = new THREE.AmbientLight(0xffffff, 1)
  lights[0].position.set(0, 0, 200)
  lights[1].position.set(100, 200, 100)
  lights[2].position.set(-100, -200, -100)
  for (const light of lights) scene.add(light)
  // camera
  camera = new THREE.PerspectiveCamera(10, 1, 0.1, 2000)
  camera.aspect = width / height
  camera.position.set(40, 20, 10)
  camera.updateProjectionMatrix()
  camera.up = new THREE.Vector3(0, 0, 1)
  camera.rotation.y = Math.PI / 2
  controls = new OrbitControls(camera, labelRenderer.domElement)
  controls.addEventListener('change', () => {
    renderer.render(scene, camera)
    labelRenderer.render(scene, camera)
  })
  return scene
}

/**
 * Adjust camera position, target, direction and fov
 * @param {object} config 
 * @param {number[]} [config.position] - optional new camera position
 * @param {number[]} [config.target] - optional new camera target
 * @param {number[]} [config.up] - optional new vector in the "up"-direction
 * @param {number} [config.fov] - optional new fov
 */
export function adjustCamera(config) {
  if (config.position) {
    camera.position.set(
      config.position[0],
      config.position[1],
      config.position[2]
    )
  }
  if (config.target) {
    controls.target = new THREE.Vector3(
      config.target[0],
      config.target[1],
      config.target[2]
    )
  }
  if (config.up) { camera.up = new THREE.Vector3(config.up[0], config.up[1], config.up[2]) }
  if (config.fov) camera.fov = config.fov
  camera.updateProjectionMatrix()
  updateScene()
}

/**
 * Update (reender) the scene, for animations, this function must be called regularly
 */
export function updateScene() {
  controls.update()
  renderer.render(scene, camera)
  labelRenderer.render(scene, camera)
}

/**
 * resize scene to given width and height
 * @param {number} width - width in px
 * @param {number} height - height in px
 */
export function resize(width, height) {
  camera.aspect = width / height
  camera.updateProjectionMatrix()
  renderer.setSize(width, height)
  labelRenderer.setSize(width, height)
  updateScene()
}

/**
 * Draw coordinate system
 * @param {object} coords - definition of the coordinate system
 * @param {object} coords.x - definition of x axis
 * @param {number} coords.x.min - start value x axis
 * @param {number} coords.x.max - end value x axis
 * @param {string} coords.x.text - label of x axis
 * @param {number} [coords.x.step] - step between ticks
 * @param {number} [coords.x.scale] - scaling factor x axis
 * @param {object} coords.y - definition of the y-axis, corresponding to the x-axis
 * @param {object} coords.z - definition of the z-axis, corresponding to the x-axis
 */
export function drawCoords(coords) {
  scaleX = coords.x.scale ? coords.x.scale : 1
  scaleY = coords.y.scale ? coords.y.scale : 1
  scaleZ = coords.z.scale ? coords.z.scale : 1
  px = (coords.x.max - coords.x.min) / 400
  let lw = px * 2
  lw = 1
  const axisMaterial = new THREE.MeshStandardMaterial({ color: colors[4] })
  // draw x-axis
  drawLine({
    x0: scaleX * coords.x.min,
    y0: 0,
    z0: 0,
    x1: scaleX * coords.x.max,
    y1: 0,
    z1: 0,
    lineWidth: lw,
    color: 4
  })
  const headX = new THREE.Mesh(
    new THREE.ConeGeometry(5 * px, 10 * px, 5),
    axisMaterial
  )
  headX.position.x = coords.x.max * scaleX
  headX.rotation.z = -Math.PI / 2
  scene.add(headX)
  const x0 = Math.ceil(coords.x.min / coords.x.step) * coords.x.step
  const x1 = Math.floor(coords.x.max / coords.x.step) * coords.x.step
  // ticks
  for (let x = x0; x < x1; x += coords.x.step) {
    drawLine({
      x0: scaleX * x,
      y0: 5 * px,
      z0: 0,
      x1: scaleX * x,
      y1: -5 * px,
      z1: 0,
      lineWidth: lw,
      color: 4
    })
  }
  // label
  let div = document.createElement('div')
  div.className = 'axis-label'
  div.innerText = coords.x.text
  let label = new CSS2DObject(div)
  label.center.set(1, 1)
  headX.add(label)
  label.layers.set(0)

  // draw y-axis
  drawLine({
    x0: 0,
    y0: scaleY * coords.y.min,
    z0: 0,
    x1: 0,
    y1: scaleY * coords.y.max,
    z1: 0,
    lineWidth: lw,
    color: 4
  })
  const headY = new THREE.Mesh(
    new THREE.ConeGeometry(5 * px, 10 * px, 5),
    axisMaterial
  )
  headY.position.y = scaleY * coords.y.max
  scene.add(headY)
  // ticks
  const y0 = Math.ceil(coords.y.min / coords.y.step) * coords.y.step
  const y1 = Math.floor(coords.y.max / coords.y.step) * coords.y.step
  for (let y = y0; y < y1; y += coords.y.step) {
    drawLine({
      x0: -5 * px,
      y0: scaleY * y,
      z0: 0,
      x1: 5 * px,
      y1: scaleY * y,
      z1: 0,
      lineWidth: lw,
      color: 4
    })
  }
  // label
  div = document.createElement('div')
  div.className = 'axis-label'
  div.innerText = coords.y.text
  label = new CSS2DObject(div)
  label.center.set(1, 1)
  headY.add(label)
  label.layers.set(0)

  // draw z-axis
  drawLine({
    x0: 0,
    y0: 0,
    z0: scaleZ * coords.z.min,
    x1: 0,
    y1: 0,
    z1: scaleZ * coords.z.max,
    lineWidth: lw,
    color: 4
  })
  const headZ = new THREE.Mesh(
    new THREE.ConeGeometry(5 * px, 10 * px, 5),
    axisMaterial
  )
  headZ.position.z = scaleZ * coords.z.max
  headZ.rotation.x = +Math.PI / 2
  scene.add(headZ)
  // ticks
  const z0 = Math.ceil(coords.z.min / coords.z.step) * coords.z.step
  const z1 = Math.floor(coords.z.max / coords.z.step) * coords.z.step
  for (let z = z0; z < z1; z += coords.z.step) {
    drawLine({
      x0: 0,
      y0: -5 * px,
      z0: scaleZ * z,
      x1: 0,
      y1: 5 * px,
      z1: scaleZ * z,
      lineWidth: lw,
      color: 4
    })
  }
  // label
  div = document.createElement('div')
  div.className = 'axis-label'
  div.innerText = coords.z.text
  label = new CSS2DObject(div)
  label.center.set(1, 1)
  headZ.add(label)
  label.layers.set(0)
}

/**
 * Draw box
 * @param {object} props - parameter object
 * @param {number} props.x - x coordinate corner
 * @param {number} props.y - y coordinate corner
 * @param {number} props.z - z coordinate corner
 * @param {number} props.dx - x dimension
 * @param {number} props.dy - y dimension
 * @param {number} props.dz - z dimension
 * @param {boolean} props.center - center object
 * @param {*} add 
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 */
export function drawBox(props, add = true) {
  const geometry = new THREE.BoxGeometry(props.dx, props.dy, props.dz)
  const material = new THREE.MeshStandardMaterial(parseStyles(props))
  const mesh = new THREE.Mesh(geometry, material)
  if (props.center) {
    mesh.position.x = props.x
    mesh.position.y = props.y
    mesh.position.z = props.z
  } else {
    mesh.position.x = props.x + 0.5 * props.dx
    mesh.position.y = props.y + 0.5 * props.dy
    mesh.position.z = props.z + 0.5 * props.dz
  }
  if (add) scene.add(mesh)
  return mesh
}

/**
 * Draw line, defined by start and end position
 * @param {object} props - parameter object
 * @param {number} props.x0 - x coordinate start
 * @param {number} props.y0 - y coordinate start
 * @param {number} props.z0 - z coordinate start
 * @param {number} props.x1 - x coordinate end
 * @param {number} props.y1 - y coordinate end
 * @param {number} props.z1 - z coordinate end
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 */
export function drawLine(props) {
  const geometry = new LineGeometry()
  if (props.positions) geometry.setPositions(props.positions)
  else {
    geometry.setPositions([
      props.x0,
      props.y0,
      props.z0,
      props.x1,
      props.y1,
      props.z1
    ])
  }
  const materialDefinition = parseStyles(props)
  materialDefinition.resolution = new THREE.Vector2(width, height)
  const material = new LineMaterial(materialDefinition)
  const mesh = new Line2(geometry, material)
  mesh.computeLineDistances()
  scene.add(mesh)
  return mesh
}

/**
 * Draw cylinder
 * @param {object} props - parameter object
 * @param {number} props.r - radius
 * @param {number} props.h - height
 * @param {boolean} props.open - determines whether the frontal surfaces should also be omitted (true = yes, false = no)
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 * @see module:plot-3d.parsePositionRotation
 */
export function drawCylinder(props, add = true) {
  const geometry = new THREE.CylinderGeometry(
    props.r,
    props.r,
    props.h,
    32,
    1,
    props.open
  )
  const material = new THREE.MeshStandardMaterial(parseStyles(props))
  const mesh = new THREE.Mesh(geometry, material)
  parsePositionRotation(props, mesh)
  if (add) scene.add(mesh)
  return mesh
}

/**
 * Draw cone
 * @param {object} props - parameter object
 * @param {number} props.r - radius
 * @param {number} props.h - height
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 * @see module:plot-3d.parsePositionRotation
 */
export function drawCone(props, add = true) {
  const geometry = new THREE.ConeGeometry(props.r, props.h, 10)
  const material = new THREE.MeshStandardMaterial(parseStyles(props))
  const mesh = new THREE.Mesh(geometry, material)
  parsePositionRotation(props, mesh)
  if (add) scene.add(mesh)
  return mesh
}

/**
 * Draw sphere
 * @param {object} props - parameter object
 * @param {number} props.r - radius
 * @param {number} [props.phiStart=0] - start value phi
 * @param {number} [props.phiLength=2pi] - width of value range phi
 * @param {number} [props.thetaStart=0] - start value theta
 * @param {number} [props.phiLength=pi] - width of value value phi
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 * @see module:plot-3d.parsePositionRotation
 */
export function drawSphere(props, add = true) {
  const phiStart = isNaN(props.phiStart) ? 0 : props.phiStart
  const phiLength = isNaN(props.phiLength) ? 2 * Math.PI : props.phiLength
  const thetaStart = isNaN(props.thetaStart) ? 0 : props.thetaStart
  const thetaLength = isNaN(props.thetaLength) ? Math.PI : props.thetaLength
  const geometry = new THREE.SphereGeometry(
    props.r,
    32,
    16,
    phiStart,
    phiLength,
    thetaStart,
    thetaLength
  )
  const material = new THREE.MeshStandardMaterial(parseStyles(props))
  const mesh = new THREE.Mesh(geometry, material)
  if (add) scene.add(mesh)
  parsePositionRotation(props, mesh)
  return mesh
}

/**
 * Draw Icosahedron
 * @param {object} props - parameter object
 * @param {number} props.r - radius
 * @returns {THREE.Mesh}
 * @see module:plot-3d.parseStyles
 * @see module:plot-3d.parsePositionRotation
 */
export function drawIcosahedron(props, add = true) {
  const geometry = new THREE.IcosahedronGeometry(props.r)
  const material = new THREE.MeshStandardMaterial(parseStyles(props))
  const mesh = new THREE.Mesh(geometry, material)
  parsePositionRotation(props, mesh)
  if (add) scene.add(mesh)
  return mesh
}

/**
 * Draw grating in x1-x2 plane
 * @param {object} props - parameter object
 * @param {number} [props.n=10] - number of bars
 * @param {number} [props.size=1] - side length
 * @param {number} [props.r=1/100] -  radius of bars
 * @returns {THREE.Group}
 * @see module:plot-3d.parseStyles
 * @see module:plot-3d.parsePositionRotation
 */
export function drawGrating(props, add = true) {
  if (!props.n) props.n = 10
  if (!props.size) props.size = 1
  if (!props.r) props.r = props.size / 100
  const grating = new THREE.Group()
  const material = new THREE.MeshStandardMaterial({
    color: colors[props.color]
  })
  // vertical lines
  for (let i = -props.n / 2; i <= props.n / 2; i++) {
    const x = (props.size * i) / props.n
    const geometry = new THREE.CylinderGeometry(
      props.r,
      props.r,
      props.size,
      12
    )
    const cylinder = new THREE.Mesh(geometry, material)
    cylinder.position.x = x
    grating.add(cylinder)
  }
  // top and bottom lines
  for (let i = -1; i <= 1; i += 2) {
    const geometry = new THREE.CylinderGeometry(
      props.r,
      props.r,
      props.size,
      12
    )
    const cylinder = new THREE.Mesh(geometry, material)
    cylinder.position.y = i * 0.5 * props.size
    cylinder.rotation.z = Math.PI / 2
    grating.add(cylinder)
  }
  if (add) scene.add(grating)
  parsePositionRotation(props, grating)
  return grating
}

/**
 * Generate plane for plotting a 3D-function
 * @param {object} props - configuration object
 * @param {number} props.size - grid size
 * @param {boolean} add - wether plane should be added to scene
 * @returns {THREE.BufferGeometry}
 * @see module:plot-3d.parseStyles
 */
export function init3DFunction(props, add = true) {
  const geometry = new THREE.BufferGeometry()
  const vertices = new Float32Array(props.size * 2 * 9) // two triangles per square
  geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3))
  geometry.computeVertexNormals()
  const params = parseStyles(props)
  params.side = THREE.DoubleSide
  delete params.color
  const material = new THREE.MeshNormalMaterial(params)
  const mesh = new THREE.Mesh(geometry, material)
  mesh.frustumCulled = false
  if (add) scene.add(mesh)
  return geometry
}

/**
 * Draw 3D-Function in x-y plane
 * @param {THREE.BufferGeometry()} geometry, see {@link module:plot-3d.init3DFunction}
 * @param {number[]} values - one-dimensional array (params.size * params.size) that contains the z-values
 * @param {object} params - configuration object
 * @param {number} params.size - grid size
 * @param {number} params.x.min - start value x-coordinate
 * @param {number} params.x.max - end value x-coordinate
 * @param {number} params.y.min - start value y-coordinate
 * @param {number} params.y.max - end value y-coordinate
 */
export function draw3DFunction(geometry, values, params) {
  const sizeX = params.x.size
  const sizeY = params.y.size
  const scaleX = (params.x.max - params.x.min) / sizeX
  const scaleY = (params.y.max - params.y.min) / sizeY
  const x0 = params.x.min
  const y0 = params.y.min
  const vertices = new Float32Array(sizeX * sizeY * 2 * 9) // two triangles per square
  for (let pixelX = 0; pixelX < sizeX - 1; pixelX++) {
    for (let pixelY = 0; pixelY < sizeY - 1; pixelY++) {
      const j = pixelY * sizeY + pixelX // index for values array
      const i = j * 18 // index for vertices array
      const x = pixelX * scaleX + x0 // real x value
      const y = pixelY * scaleY + y0 // real y value

      vertices[i + 0] = x
      vertices[i + 1] = y
      vertices[i + 2] = values[j]

      vertices[i + 3] = x + scaleX
      vertices[i + 4] = y
      vertices[i + 5] = values[j + 1]

      vertices[i + 6] = x
      vertices[i + 7] = y + scaleY
      vertices[i + 8] = values[j + sizeY]

      vertices[i + 9] = x + scaleX
      vertices[i + 10] = y
      vertices[i + 11] = values[j + 1]

      vertices[i + 12] = x + scaleX
      vertices[i + 13] = y + scaleY
      vertices[i + 14] = values[j + sizeY + 1]

      vertices[i + 15] = x
      vertices[i + 16] = y + scaleY
      vertices[i + 17] = values[j + sizeY]
    }
  }
  geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3))
  geometry.computeVertexNormals()
}

/**
 * Draw 3D-Function in polar coords
 * @param {THREE.BufferGeometry()} geometry, see {@link module:plot-3d.init3DFunction}
 * @param {function} rFunction - radius function r(phi, theta)
 * @param {object} params - configuration object
 * @param {number} params.steps - number of steps when going through the angle ranges
 */
export function draw3DFunctionPolar(geometry, rFunction, params) {
  const steps = params.steps
  const scalePhi = 2 * Math.PI / steps
  const scaleTheta = Math.PI / steps
  const vertices = new Float32Array(steps * steps * 2 * 9) // two triangles per square
  for (let k = 0; k < steps; k++) {
    const phi = k * scalePhi
    for (let j = 0; j < steps; j++) {
      const theta = j * scalePhi / 2
      const cosTheta0 = Math.cos(theta)
      const sinTheta0 = Math.sin(theta)
      const cosPhi0 = Math.cos(phi)
      const sinPhi0 = Math.sin(phi)
      const cosTheta1 = Math.cos(theta + scaleTheta)
      const sinTheta1 = Math.sin(theta + scaleTheta)
      const cosPhi1 = Math.cos(phi + scalePhi)
      const sinPhi1 = Math.sin(phi + scalePhi)
      const i = (k * steps + j) * 18 // index for vertices array

      const r0 = rFunction(theta, phi)
      vertices[i + 0] = r0 * sinTheta0 * cosPhi0
      vertices[i + 1] = r0 * sinTheta0 * sinPhi0
      vertices[i + 2] = r0 * cosTheta0

      const r1 = rFunction(theta + scaleTheta, phi)
      vertices[i + 3] = r1 * sinTheta1 * cosPhi0
      vertices[i + 4] = r1 * sinTheta1 * sinPhi0
      vertices[i + 5] = r1 * cosTheta1

      const r2 = rFunction(theta, phi + scalePhi)
      vertices[i + 6] = r2 * sinTheta0 * cosPhi1
      vertices[i + 7] = r2 * sinTheta0 * sinPhi1
      vertices[i + 8] = r2 * cosTheta0

      vertices[i + 9] = vertices[i + 3]
      vertices[i + 10] = vertices[i + 4]
      vertices[i + 11] = vertices[i + 5]

      vertices[i + 12] = vertices[i + 6]
      vertices[i + 13] = vertices[i + 7]
      vertices[i + 14] = vertices[i + 8]

      const r3 = rFunction(theta + scaleTheta, phi + scalePhi)
      vertices[i + 15] = r3 * sinTheta1 * cosPhi1
      vertices[i + 16] = r3 * sinTheta1 * sinPhi1
      vertices[i + 17] = r3 * cosTheta1
    }
  }
  geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3))
  geometry.computeVertexNormals()
}

/**
 * Parse style information of element and return material props.
 * This function is called by all routines that draw geometric objects on the canvas.
 * @param {object} props - element to parse (box, line, sphere, etc.)
 * @returns {object} object, containing the attributes color, transparent, opacity, side and lineWidth
 */
function parseStyles(props) {
  // color
  if (Number.isInteger(props.color)) props.color = colors[props.color]
  else if (!props.color) props.color = colors[4]
  // opacity
  if (!props.opacity) {
    props.opacity = 1
    props.transparent = false
  } else {
    props.transparent = true
  }
  // side
  props.side = props.twoSide ? THREE.DoubleSide : THREE.FrontSide
  // lineWidth
  if (!props.lineWidth) props.lineWidth = 0
  // return config object for Material
  return {
    color: props.color,
    transparent: props.transparent,
    opacity: props.opacity,
    side: props.side,
    lineWidth: props.lineWidth
  }
}

/**
 * Parse position and rotation definition and apply to Mesh-element or group
 * @param {object} def - parameter object
 * @param {THREE.Mesh} elment - elment to modify
 * @param {number} [def.x] - x coordinate
 * @param {number} [def.y] - y coordinate
 * @param {number} [def.z] - z coordinate
 * @param {number} [def.rotx] - rotation x axis
 * @param {number} [def.roty] - rotation y axis
 * @param {number} [def.rotz] - rotation z axis
 * @param {THREE.Mesh} modified element 
 */
export function parsePositionRotation(def, element) {
  if (def.x) element.position.x = def.x
  if (def.y) element.position.y = def.y
  if (def.z) element.position.z = def.z
  if (def.rotx) element.rotation.x = def.rotx
  if (def.roty) element.rotation.y = def.roty
  if (def.rotz) element.rotation.z = def.rotz
}

/**
 * Generates a random 3D unit vector with a uniform spherical distribution
 * @see http://stackoverflow.com/questions/5408276/python-uniform-spherical-distribution
 * @returns {number[]} array with coordinates [x, y, z]
 */
export function generateRandomUnitVector() {
  const phi = Math.random() * Math.PI * 2
  const cosTheta = -1 + Math.random() * 2
  const theta = Math.acos(cosTheta)
  const x = Math.sin(theta) * Math.cos(phi)
  const y = Math.sin(theta) * Math.sin(phi)
  const z = Math.cos(theta)
  return [x, y, z]
}

/**
 * Update colors (get colors again from css variables)
 * @returns {string[]} - MintApp colors
 * @see module:color.getColors
 */
export function updateColors() {
  colors = color.getColors()
  return colors
}

/**
 * Get screenshot
 * @returns [string] image encoded as dataURL
 */
export function getScreenshot() {
  renderer.render(scene, camera)
  return renderer.domElement.toDataURL()
}

