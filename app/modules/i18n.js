/**
 * Information about the available languages and helper functions
 * @author Thomas Kippenberg
 * @module i18n
 * @see {@link https://codeberg.org/MintApps/weblate|Mintapps weblate repository}
 */

import config from '../config.js'
import * as storage from '../modules/storage.js'
import { renderBlock, renderInline } from './markdown.js'
import { getUrlParam, deleteUrlParam } from './utils.js'

/** Current language, in two letter code */
let locale = ''
/** Messages for current language, empty during load or on error */
let messages = {}
/** Translation queue, elements that will be translated after messages have been loaded */
let queue = []
/** List of listener functions (to receive notification on locale change) */
let listeners = []

/**
 * List of all locales, must be a subset of locales in weblate/tools/config.js
 * @see {@link https://de.wikipedia.org/wiki/Liste_der_ISO-639-1-Codes|two letter codes for languages}
 * @see {@link https://github.com/hampusborgos/country-flags/tree/main/svg|source of the flags used}
 */
export const allLocales = {
  cs: { text: 'Čeština', icon: '../flags/cz.svg', comma: true, auto: true },
  da: { text: 'Dansk', icon: '../flags/dk.svg', comma: true, auto: true },
  de: { text: 'Deutsch', icon: '../flags/de.svg', comma: true },
  en: { text: 'English', icon: '../flags/gb.svg', comma: false },
  es: { text: 'Español', icon: '../flags/es.svg', comma: false, auto: true },
  et: { text: 'Eesti', icon: '../flags/ee.svg', comma: true, auto: true },
  fi: { text: 'Suomi', icon: '../flags/fi.svg', comma: true, auto: true },
  fr: { text: 'Français', icon: '../flags/fr.svg', comma: true, auto: true },
  hu: { text: 'Magyar', icon: '../flags/hu.svg', comma: true, auto: true },
  it: { text: 'Italiano', icon: '../flags/it.svg', comma: true, auto: true },
  nl: { text: 'Nederlands', icon: '../flags/nl.svg', comma: true, auto: true },
  pl: { text: 'Polski', icon: '../flags/pl.svg', comma: true, auto: true },
  pt: { text: 'Português', icon: '../flags/pt.svg', comma: true, auto: true },
  sv: { text: 'Svenska', icon: '../flags/se.svg', comma: true, auto: true },
  uk: { text: 'українська', icon: '../flags/ua.svg', comma: true },
  zh_Hans: { text: '中国人', icon: '../flags/cn.svg', comma: true, auto: true }
}

/**
 * Initialize the i18n module by determining the initial language (either from the useragent or the url)
 * and loading the corresponding language files.
 */
export function setup() {
  // 1. try: receive from url
  locale = getQueryLocale()
  // 2. use default for search engines
  if (navigator.webdriver) locale = config.defaultLocale
  // 3. try: receive from sessionStorage
  if (!locale) locale = storage.get('locale')
  // 4. try: receive from user agent (browser)
  if (!locale) locale = getUserLocale()
  // fallback to default locale if not supported
  if (!isSupportedLocale(locale)) locale = config.defaultLocale
  // finally load
  loadLocale()
}

/**
 * try to get locale from url parameter "lang"
 * @returns locale of user or empty string if user locale unavailabe
 */
function getQueryLocale () {
  return getUrlParam('lang')
}

/**
 * try to get locale from user agent
 * @returns locale of user or empty string if user locale unavailabe
 */
function getUserLocale() {
  let locale = ''
  const nl = navigator.language
  if (nl && nl.length > 1) locale = nl.substring(0, 2)
  if (locale === 'zh') locale = 'zh_Hans'
  return locale
}

/**
 * Check if locale is supported
 * @param {string} locale - id of locale
 * @returns {boolean} true if supported
 */
export function isSupportedLocale(locale) {
  return !!allLocales[locale]
}

/**
 * Load messages for specified locale
 * @param {string} locale - id of locale
 */
function loadLocale() {
  messages = {}
  fetch(`../assets/weblate/main-${locale}.json`).then(response => {
    if (!response.ok) {
      console.error(`i18n: can't load messages for locale ${locale}`)
    }
    else {
      response.json().then(data => {
        messages = data
        storage.set('messages', JSON.stringify(messages))
        // setUrlParam('lang', locale)
        deleteUrlParam('lang')
        console.debug(`i18n: successfully loaded messages for locale ${locale}`)
        // set meta infos for SEO optimization
        if (window.mintapps?.website) {
          document.documentElement.setAttribute('lang', locale)
          document.title = translate(window.mintapps.title)
          let meta = document.createElement('meta')
          meta.name = 'description'
          meta.setAttribute('content', translate(window.mintapps.text))
          document.head.append(meta)
          meta = document.createElement('meta')
          meta.name = 'author'
          meta.setAttribute('content', 'MintApps Project')
          storage.set('locale', locale)
        }
        // translate elements in queue
        queue.forEach(element => translateChildren(element))
        queue = []
        // inform listeners
        listeners.forEach(listener => listener(locale))
      })
    }
  })
}

/**
 * Translate given key
 * @param {String} key - Translation key, for example "General.button"
 * @param {HTMLElement} [element] - HTML-Element, optional required to resolve variables
 */
export function translate(key, element = null) {
  // Don't translate empty or missing keys
  if (!key || key === 'null') return ''
  // Don't translate strings not matching the key format
  if (!key.match(/^[A-Za-z]+\.[A-Za-z0-9-]+$/)) return key
  // Try to resolve key in messages
  let text = key.split('.').reduce((obj, i) => (obj ? obj[i] : null), messages)
  if (!text) {
    console.debug(`i18n: missing key ${key} in locale ${locale}`)
    text = key
  }
  // Handle variables enclosed by curly brackets
  const variables = text.match(/{(.*?)}/g);
  if (variables && element) {
    variables.forEach((variable) => {
      // Find matching key in data-* attributes
      Object.entries(element.dataset).filter(([key, value]) => {
        if (`{${key}}` === variable) text = text.replace(`${variable}`, value)
      })
    });
  }
  return text
}

/**
 * Translate elment with data-i18n(-xxx) attribute(s)
 * @param {HTMLElement} element
 */
export function translateElement(element) {
  const dataset = element.dataset
  // inner text
  if (dataset.i18n) element.innerText = translate(dataset.i18n, element)
  // alt text
  if (dataset.i18nAlt) element.alt = translate(dataset.i18nAlt, element)
  // title text
  if (dataset.i18nTitle) element.title = translate(dataset.i18nTitle, element)
  // placeholder
  if (dataset.i18nPlaceholder) element.placeholder = translate(dataset.i18nPlaceholder, element)
  // inline markdown
  if (dataset.i18nMdInline) element.innerHTML = renderInline(translate(dataset.i18nMdInline, element))
  // block markdown
  if (dataset.i18nMdBlock) element.innerHTML = renderBlock(translate(dataset.i18nMdBlock, element))
}

/**
 * Translate all children with data-i18n(-xxx) attributes
 * @param {HTMLElement} element - root element
 */
export function translateChildren(element) {
  // Queue translation if messages missing
  if (Object.keys(messages).length === 0) {
    queue.push(element)
    return
  }
  // Translate all elements with class i18n
  const elements = element.querySelectorAll('[data-i18n], [data-i18n-alt], [data-i18n-title], [data-i18n-placeholder], [data-i18n-md-inline], [data-i18n-md-block]')
  elements.forEach(translateElement)
}

/**
 * Check if locale uses comma as decimal separator
 * @param {string} locale - id of locale
 * @returns {boolean} true if comma is used, false if point is used
 * @see {@link https://upload.wikimedia.org/wikipedia/commons/a/a8/DecimalSeparator.svg|information about the decimal separator}
 */
export function useDecimalSeparatorComma(locale) {
  if (!locale) locale = getCurrentLocale()
  return allLocales[locale] && allLocales[locale].comma
}

/**
 * Set current locale and load messages and retranslate complete page
 * @param {String} newLocale - new locale
 */
export function setLocale(newLocale) {
  if (newLocale === locale) return
  // translate without reload
  locale = newLocale
  loadLocale()
  translateChildren(window.mintapps.container)
  // setHrefLangAttribute(window.mintapps.container)
}

/**
 * Get current locale from DOM tree (because we can't use vue inject here)
 * @returns {String} id of locale
 */
export function getCurrentLocale() {
  //const locale = document.documentElement.getAttribute('lang')
  return locale
}

/**
 * Watch for locale changes
 * @param {Function} listener - listener function (receiving new value)
 */
export function watch(listener) {
  listeners.push(listener)
}

/**
 * Set the lang attribute in all child elements that are a link and contain this
 *
function setHrefLangAttribute(root) {
  root.querySelectorAll('a').forEach(a => {
    try {
      const url = new URL(a.href)
      if (url.searchParams.get('lang')) {
        url.searchParams.set('lang', locale)
        a.href = url.toString()
      }
    } catch  {
      // ignore invalid links
    }
  })
}
*/
