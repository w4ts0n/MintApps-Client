/**
 * Simple storage module, using the browser's session storage
 * @author Thomas Kippenberg
 * @module storage
 */

const listeners = {}

/**
 * Store key value pair
 * @param {String} key
 * @param {Object} value
 */
export function set(key, value) {
  sessionStorage.setItem(key, JSON.stringify(value))
  if (listeners[key]) listeners[key].forEach(listener => listener())
}

/**
 * Get previsosley stored value (by key)
 * @param {String} key 
 * @returns Object
 */
export function get(key) {
  try {
    return JSON.parse(sessionStorage.getItem(key))
  } catch {
    return null
  }
}

/**
 * Watch specified key for change
 * @param {String} key
 * @param {Function} listener - callback function change
 */
export function watch(key, listener) {
  if (!listeners[key]) listeners[key] = []
  listeners[key].push(listener)
}