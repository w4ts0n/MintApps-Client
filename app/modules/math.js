/**
 * Collection of various mathematical functions used in the MintApps.
 * @author Thomas Kippenberg
 * @module math
 */

/**
 * Generate Gauss distributed random number
 * @param {number} mu - expectation value
 * @param {number} sigma - standard deviation
 * @returns {number} gauss distributed random number
 * @see https://de.wikipedia.org/wiki/Box-Muller-Methode
 */
export function gaussRandom(mu, sigma) {
  const u1 = Math.random()
  const u2 = Math.random()
  const z0 = Math.sqrt(-2 * Math.log(u1)) * Math.cos(2 * Math.PI * u2)
  // dont calc z1 in order to save time
  return mu + sigma * z0
}


/**
 * Runge-Kutta integration for equation of first order
 * @param {number} t - time
 * @param {number} x - equals position x for motion
 * @param {number} h - numeric integration step
 * @param {function} f - function f(t, x) that equals velocity v for motion
 * @returns {number[]} - new values [t, x]
 */
export function rungeKutta1(t, x, h, f) {
  const k1 = f(t, x)
  const k2 = f(t + 0.5 * h, x + 0.5 * h * k1)
  const k3 = f(t + 0.5 * h, x + 0.5 * h * k2)
  const k4 = f(t + h, x + h * k3)
  // integrate Kutta
  x += (h / 6) * (k1 + 2 * k2 + 2 * k3 + k4)
  t += h
  // return
  return [t, x]
}

/**
 * Runge-Kutta integration for equation of second order, one dimensional
 * @param {number} t - time
 * @param {number} x0 - equals position x for motion
 * @param {number} x1 - equals velocity v for motion
 * @param {number} h - numeric integration step
 * @param {function} f - function f(t, x, v) that equals acceleration a for motion
 * @returns {number[]} new values [t, x0, x1]
 */
export function rungeKutta2(t, x0, x1, h, f) {
  const k10 = x1
  const k11 = f(t, x0, x1)
  const k20 = x1 + 0.5 * h * k11
  const k21 = f(t + 0.5 * h, x0 + 0.5 * h * k10, x1 + 0.5 * h * k11)
  const k30 = x1 + 0.5 * h * k21
  const k31 = f(t + 0.5 * h, x0 + 0.5 * h * k20, x1 + 0.5 * h * k21)
  const k40 = x1 + h * k31
  const k41 = f(t + h, x0 + h * k30, x1 + h * k31)
  // integrate Kutta
  x0 += (h / 6) * (k10 + 2 * k20 + 2 * k30 + k40)
  x1 += (h / 6) * (k11 + 2 * k21 + 2 * k31 + k41)
  t += h
  // return
  return [t, x0, x1]
}

/**
 * Runge-Kutta integration for equation of second order for twodimensional vectors
 * @param {number} t - time
 * @param {number[]} x0 - equals position [x, y] for motion
 * @param {number[]} x1 - equals velocity [vx, vy] for motion
 * @param {number} h - numeric integration step
 * @param {function} f - function f(t, x, v) that equals acceleration [ax, ay] for motion
 * @returns {number[]} new values [t, x0, x1]
 */
export function rungeKutta2Vector(t, x0, x1, h, f) {
  const k10 = [x1[0], x1[1]]
  const k11 = f(t, x0, x1)
  const k20 = [x1[0] + 0.5 * h * k11[0], x1[1] + 0.5 * h * k11[1]]
  const k21 = f(
    t + 0.5 * h,
    [x0[0] + 0.5 * h * k10[0], x0[1] + 0.5 * h * k10[1]],
    [x1[0] + 0.5 * h * k11[0], x1[1] + 0.5 * h * k11[1]]
  )
  const k30 = [x1[0] + 0.5 * h * k21[0], x1[1] + 0.5 * h * k21[1]]
  const k31 = f(
    t + 0.5 * h,
    [x0[0] + 0.5 * h * k20[0], x0[1] + 0.5 * h * k20[1]],
    [x1[0] + 0.5 * h * k21[0], x1[1] + 0.5 * h * k21[1]]
  )
  const k40 = [x1[0] + h * k31[0], x1[1] + h * k31[1]]
  const k41 = f(
    t + h,
    [x0[0] + h * k30[0], x0[1] + h * k30[1]],
    [x1[0] + h * k31[0], x1[1] + h * k31[1]]
  )
  // integrate Kutta
  x0[0] += (h / 6) * (k10[0] + 2 * k20[0] + 2 * k30[0] + k40[0])
  x0[1] += (h / 6) * (k10[1] + 2 * k20[1] + 2 * k30[1] + k40[1])
  x1[0] += (h / 6) * (k11[0] + 2 * k21[0] + 2 * k31[0] + k41[0])
  x1[1] += (h / 6) * (k11[1] + 2 * k21[1] + 2 * k31[1] + k41[1])
  t += h
  // return
  return [t, x0, x1]
}

/**
 * Prepare the value pairs for calculating fit functions
 * @param {Array} points - array of value pairs { x, y} 
 * @param {Number} minPoints - required minimum number of different points
 * @param {Boolean} [allowSameXwithDifferentY=true] - allow duplicate x values with different y values
 * @returns Sorted and validated value pairs
 * @throws 
 */
export function processPoints(givenPoints, minPoints, allowSameXwithDifferentY = false) {
  // check for type
  const points = []
  for (const point of givenPoints) {
    if (!isNaN(point.x) && !isNaN(point.y)) points.push(point)
  }
  // sort array by x values
  points.sort((a, b) => a.x - b.x)
  // remove duplicates
    for (let i = 0; i < points.length; i++) {
      // check if to points have the same x-value
      if (i < points.length - 1 && points[i].x === points[i + 1].x) {
        // remove duplicats (y-value is the same)
        if (points[i].y === points[i + 1].y) {
          points.splice(i, 1)
          i--
        } else if (!allowSameXwithDifferentY) {
          throw Error('Maths.interpolate-duplicate-x-value')
        }
      }
  }
  // at least two points are necessary to calc fit functions
  if (points.length < minPoints) {
    throw Error('Maths.not-enough-points')
  }
  return points
}

/**
 * Linear function regression
 * @param {object[]} givenPoints - Array of point objects {x, y}
 * @param {number} [prop=3] - type of regression (1 = constant function, 2 = straight of origin, 3 = general straight line)
 * @returns {object} with m = gradient, t = y axis section, f = function
 */
export function fitLinear(givenPoints, prop = 3) {
  const points = processPoints(givenPoints, 0, true)
  const numPoints = points.length
  if (numPoints === 0) return { m: 0, y0: 0, f: (x) => 0 * x, label: 'f(x)=0' }
  // average values – only for normal line or constant
  let averageX = 0
  let averageY = 0
  if (prop === 1 || prop === 3) {
    for (const point of points) {
      averageX += point.x / numPoints
      averageY += point.y / numPoints
    }
  }
  // constant function ?
  if (prop === 1) return { m: 0, y0: averageY, f: () => averageY }
  // else determine slope
  let sum1 = 0
  let sum2 = 0
  for (const point of points) {
    sum1 += (point.x - averageX) * (point.y - averageY)
    sum2 += Math.pow(point.x - averageX, 2)
  }
  // build label
  const m = sum1 / sum2
  const y0 = averageY - m * averageX
  return { m, y0, f: x => m * x + y0 }
}

/**
 * Exponential function regression
 * @param {object[]} givenPoints - Array of point objects {x, y}
 * @returns {object} with parameters a, b of function a*e^(b*x)
 */
export function fitExponential(givenPoints) {
  const points = processPoints(givenPoints, 2, true)
  // check signs
  let sign = 0
  points.forEach(p => {
    if (p.y < 0 && sign === 0) sign = -1
    if (p.y > 0 && sign === 0) sign = +1
    if (p.y < 0 && sign > 0) throw Error('Maths.exponential-fit-different-signs')
    if (p.y > 0 && sign < 0) throw Error('Maths.exponential-fit-different-signs')
    if (p.y === 0) throw Error('Maths.exponential-fit-zero')
  })
  // transform to linear function using log
  const linPoints = points.map(p => {
    return { x: p.x, y: Math.log(sign * p.y) }
  })
  // fit linear
  const linear = fitLinear(linPoints, 3)
  // transform back to exp function
  const a = Math.exp(linear.y0) * sign
  const b = linear.m
  return { a, b, f: x => a * Math.exp(b * x) }
}

/**
 * Power function regression
 * @param {object[]} givenPoints - Array of point objects {x, y}
 * @returns {object} with parameters a, b of function a*b^x
 */
export function fitPower(givenPoints) {
  const points = processPoints(givenPoints, 2, true)
  // check signs
  let sign = 0
  points.forEach(p => {
    if (p.x < 0) throw Error('Maths.power-fit-negative-abcissa')
    if (p.y === 0 && p.x !== 0) throw Error('Maths.power-fit-zero')
    if (p.x === 0 && p.y !== 0) throw Error('Maths.power-fit-zero')
    if (p.y < 0 && sign === 0) sign = -1
    if (p.y > 0 && sign === 0) sign = +1
    if (p.y < 0 && sign > 0) throw Error('Maths.power-fit-different-signs')
    if (p.y > 0 && sign < 0) throw Error('Maths.power-fit-different-signs')
  })
  // remove first point when x=0 and y=0
  if (points[0].x === 0 && points[0].y === 0) points.splice(0, 1)
  // transform to linear function using log
  const linPoints = points.map(p => {
    return { x: Math.log(p.x), y: Math.log(sign * p.y) }
  })
  // fit linear
  const linear = fitLinear(linPoints, 3)
  // transform back to exp function
  const a = Math.exp(linear.y0) * sign
  const b = linear.m
  return { a, b, f: x => a * Math.pow(x, b) }
}

/**
 * Simple spline interpolation by fitting polynomal functions of order 3
 * @param {*} givenPoints - points. An array of objects with x and y coordinate.
 * @returns {functions[]} - array of interpolation functions
 */
export function interpolateSpline(givenPoints) {
  const p = processPoints(givenPoints, 2, false)
  // calculate gradients
  for (let i = 0; i < p.length; i++) {
    if (i === 0) {
      p[i].m = (p[i + 1].y - p[i].y) / (p[i + 1].x - p[i].x)
    } else if (i + 1 === p.length) {
      p[i].m = (p[i - 1].y - p[i].y) / (p[i - 1].x - p[i].x)
    } else {
      p[i].m = 0.5 * (p[i + 1].y - p[i].y) / (p[i + 1].x - p[i].x) + 0.5 * (p[i - 1].y - p[i].y) / (p[i - 1].x - p[i].x)
    }
  }
  // calculate polynom functions of order 3
  const functions = []
  for (let i = 0; i < p.length - 1; i++) {
    const x0 = p[i].x
    const x1 = p[i + 1].x
    const m0 = p[i].m
    const m1 = p[i + 1].m
    const y0 = p[i].y
    const y1 = p[i + 1].y
    const a = -(-2 * y1 + 2 * y0 + (m1 + m0) * x1 + (-m1 - m0) * x0) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const b = (-3 * x1 * y1 + x0 * ((m1 - m0) * x1 - 3 * y1) + (3 * x1 + 3 * x0) * y0 + (m1 + 2 * m0) * x1 ** 2 + (-2 * m1 - m0) * x0 ** 2) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const c = -(x0 * ((2 * m1 + m0) * x1 ** 2 - 6 * x1 * y1) + 6 * x0 * x1 * y0 + m0 * x1 ** 3 + (-m1 - 2 * m0) * x0 ** 2 * x1 - m1 * x0 ** 3) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    const d = (x0 ** 2 * ((m1 - m0) * x1 ** 2 - 3 * x1 * y1) + x0 ** 3 * (y1 - m1 * x1) + (3 * x0 * x1 ** 2 - x1 ** 3) * y0 + m0 * x0 * x1 ** 3) / (-(x1 ** 3) + 3 * x0 * x1 ** 2 - 3 * x0 ** 2 * x1 + x0 ** 3)
    functions.push({
      y: (x) => a * x ** 3 + b * x ** 2 + c * x + d,
      range: {
        xmin: parseFloat(p[i].x),
        xmax: parseFloat(p[i + 1].x)
      }
    })
  }
  return functions
}

/**
 * Tests if number x is within specified range x0 ... x1
 * @param {number} x - number
 * @param {number} x0 - lower limit
 * @param {number} x1 - upper limit
 * @returns {boolean}
 */
export function isInRange(x, x0, x1) {
  return (x >= x0 && x <= x1) || (x >= x1 && x <= x0)
}

/**
 * Sum of to fractions
 * @param {number[]} a - first fraction
 * @param {number[]} b - second fraction
 * @returns {number[]}
 */
export function sumQ(a, b) {
  const c = [a[0] * b[1] + b[0] * a[1], a[1] * b[1]]
  return reduceQ(c)
}

/**
 * Difference of to fractions
 * @param {number[]} a - first fraction
 * @param {number[]} b - second fraction
 * @returns {number[]}
 */
export function differenceQ(a, b) {
  const c = [a[0] * b[1] - b[0] * a[1], a[1] * b[1]]
  return reduceQ(c)
}

/**
 * Product of to fractions
 * @param {number[]} a - first fraction
 * @param {number[]} b - second fraction
 * @returns {number[]}
 */
export function productQ(a, b) {
  const c = [a[0] * b[0], a[1] * b[1]]
  return reduceQ(c)
}

/**
 * Quotient of to fractions
 * @param {number[]} a - first fraction
 * @param {number[]} b - second fraction
 * @returns {number[]}
 */
export function quotientQ(a, b) {
  const c = [a[0] * b[1], a[1] * b[0]]
  return reduceQ(c)
}

/**
 * Reduce a fraction as much as possible
 * @param {number[]} a - fraction
 * @returns  {number[]}
 */
export function reduceQ(a) {
  const limit = Math.min(a[0], a[1])
  for (let i = limit; i > 2; i--) {
    if (a[0] % i === 0 && a[1] % i === 0) {
      a[0] /= i
      a[1] /= i
    }
  }
  return a
}

/**
 * Perform simple arithmetic operation for two fractions
 * @param {number[]} a - first fraction
 * @param {number[]} b - second fraction
 * @param {string} op - operator (+, - , *, /)
 * @returns {number[]}
 */
export function calcInQ(a, b, op) {
  switch (op) {
    case '+':
      return sumQ(a, b)
    case '-':
      return differenceQ(a, b)
    case '/':
      return quotientQ(a, b)
    case '*':
      return productQ(a, b)
  }
}

/**
 * Perform simple arithmetic operations for real numbers
 * @param {number} a - first number
 * @param {number} b - second number
 * @param {string} op - operator (+, - , *, /)
 * @returns {number}
 */
export function calcInR(a, b, op) {
  switch (op) {
    case '+':
      return a + b
    case '-':
      return a - b
    case '/':
      return a / b
    case '*':
      return a * b
  }
}
