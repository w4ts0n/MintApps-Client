/**
 * Functions for using colors depending on the selected color scheme
 * @author Thomas Kippenberg
 * @module color
 */

const COLOR_PALLETES = {
  paletteMintapps: [
    { x: 0.0, r: 0.102, g: 0.113, b: 0.125 },
    { x: 0.15, r: 0.0, g: 0.375, b: 0.625 },
    { x: 0.3, r: 0.0, g: 0.625, b: 0.375 },
    { x: 0.4, r: 0.625, g: 0.625, b: 0 },
    { x: 0.7, r: 0.625, g: 0.625, b: 0.625 },
    { x: 1.0, r: 0.102, g: 0.113, b: 0.125 }
  ],
  paletteRainbow: [
    { x: 0.0, r: 1, g: 0, b: 1 },
    { x: 0.15, r: 0, g: 0, b: 1 },
    { x: 0.275, r: 0, g: 1, b: 1 },
    { x: 0.325, r: 0, g: 1, b: 0 },
    { x: 0.5, r: 1, g: 1, b: 0 },
    { x: 0.663, r: 1, g: 0, b: 0 },
    { x: 0.9, r: 0.1, g: 0.1, b: 0.1 },
    { x: 1.0, r: 1, g: 0, b: 1 }
  ],
  paletteBlueyellow: [
    { x: 0.0, r: 0.0, g: 0.027, b: 0.391 },
    { x: 0.16, r: 0.124, g: 0.418, b: 0.793 },
    { x: 0.42, r: 0.926, g: 1.0, b: 1.0 },
    { x: 0.6425, r: 1.0, g: 0.664, b: 0.0 },
    { x: 0.8575, r: 0.0, g: 0.008, b: 0.0 },
    { x: 1.0, r: 0.0, g: 0.027, b: 0.391 }
  ],
  paletteGrayscale: [
    { x: 0.0, r: 0.2, g: 0.2, b: 0.2 },
    { x: 0.5, r: 0.8, g: 0.8, b: 0.8 },
    { x: 1.0, r: 0.2, g: 0.2, b: 0.2 }
  ],
  paletteBlack: [
    { x: 0.0, r: 0.0, g: 0.0, b: 0.0 },
    { x: 1.0, r: 0.0, g: 0.0, b: 0.0 }
  ]
}

/**
 * Get array of Mintapp colors as hex codes by resolving the css definitions
 * @returns {string[]} Array with the following color values :
 * 0 transparent,
 * 1 primary paint color (blue),
 * 2 accent color (green),
 * 3 secondary text color (gray),
 * 4 primary text color (black for light, white for dark),
 * 5 background color (white for light, black for dark),
 * 6 dividor color (light gray for light, dark gray for dark),
 * 7 error color (red),
 * 8 icon text color
 */
export function getColors () {
  let element = document.querySelector('main') ?? document.querySelector('.mintapp')
  if (element) {
    const style = getComputedStyle(element)
    return [
      style.getPropertyValue('--background-color'),
      style.getPropertyValue('--primary-color'),
      style.getPropertyValue('--secondary-color'),
      style.getPropertyValue('--ternary-color'),
      style.getPropertyValue('--primary-text-color'),
      style.getPropertyValue('--background-color'),
      style.getPropertyValue('--dividor-color'),
      style.getPropertyValue('--error-color'),
      style.getPropertyValue('--background-color-1'),
      style.getPropertyValue('--icon-text-color')
    ]
  } else {
    return [
      '#ffffff',
      '#000080',
      '#008000',
      '#808080',
      '#000000',
      '#ffffff',
      '#c0c0c0',
      '#800000',
      '#ffffff',
      '#000000'
    ]
  }
}

/**
 * Get array of Mintapp colors containing the css definitions (which will be not resolved)
 * @returns {string[]} Array with the following values:
 * 'transparent',
 * 'var(--primary-color)',
 * 'var(--secondary-color)',
 * 'var(--ternary-color)',
 * 'var(--primary-text-color)',
 * 'var(--background-color)',
 * 'var(--dividor-color)',
 * 'var(--error-color)',
 * 'var(--background-color-1)'
 */
export function getCssColors () {
  return [
    'transparent',
    'var(--primary-color)',
    'var(--secondary-color)',
    'var(--ternary-color)',
    'var(--primary-text-color)',
    'var(--background-color)',
    'var(--dividor-color)',
    'var(--error-color)',
    'var(--background-color-1)'
  ]
}

/**
 * Create a random color
 * @returns {string} hex-color code, for example #4050a0
 */
export function randomColor () {
  const r = Math.floor(150 * Math.random())
  const b = Math.floor(150 * Math.random())
  const g = Math.floor(150 * Math.random())
  return rgbToHex(r, g, b)
}

/**
 * Creates a palette (matching the theme) with total entries and specifies the color tone with index;
 * Returns complete style text
 * @param {number} index - number between 0 and total
 * @param {number} total - size of palette (number of different colors)
 * @returns {string} css-String for coloring the background, for example 'background-color: #ff0000'
 */
export function getBgVarStyle (index, total) {
  const p1 = Math.round(100 * index/(total - 0.999))
  const p2 = 100 - p1
  return `background-color:color-mix(in oklab, var(--primary-color) ${p1}%, var(--secondary-color) ${p2}%)`
}

/**
 * Creates a palette (matching the theme) with total entries and specifies the color tone with index;
 * Returns css color string
 * @param {number} index - number between 0 and total
 * @param {number} total - size of palette (number of different colors)
 * @returns {string} css-String for coloring the background, for example 'background-color: #ff0000'
 */
export function getBgVarColor (index, total) {
  const p1 = Math.round(100 * index/(total - 0.999))
  const p2 = 100 - p1
  return `color-mix(in oklab, var(--primary-color) ${p1}%, var(--secondary-color) ${p2}%)`
}

/**
 * Get list of all available color palettes
 * @returns { String[] } Array, containing all available palette names
 */
export function getColorPaletteNames () {
  return Object.keys(COLOR_PALLETES)
}

/**
 * Create a list of colors for a specific color palette
 * @param {number} length - number of entries in color palette
 * @param {*} name - name of the color palette see {@link module:color.getColorPaletteNames}
 * @returns {String[]} Array, containing rgb entries, for example [{r: 128, g: 17, b: 255}]
 */
export function getColorPalette (length, name) {
  if (!name || !COLOR_PALLETES[name]) name = 'paletteMintapps'
  const cp = COLOR_PALLETES[name]
  const palette = []
  let r, g, b
  for (let i = 0; i < length; i++) {
    const x = i / length
    let pos = 0
    while (x > cp[pos + 1].x) pos++
    const frac = (x - cp[pos].x) / (cp[pos + 1].x - cp[pos].x)
    r = 255 * (cp[pos].r + (cp[pos + 1].r - cp[pos].r) * frac)
    g = 255 * (cp[pos].g + (cp[pos + 1].g - cp[pos].g) * frac)
    b = 255 * (cp[pos].b + (cp[pos + 1].b - cp[pos].b) * frac)
    palette.push([r, g, b, 255])
  }
  return palette
}

/**
 * Convert rgb color to hex color code
 * @param {number} r - red value (0-255)
 * @param {number} g - green value (0-255)
 * @param {number} b - blue value (0-255)
 * @returns {string} hex color code
 */
export function rgbToHex (r, g, b) {
  return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b)
}

/**
 * Convert hex color to rgb color code
 * @param {string} hex - hex color code (for example '#ff0000')
 * @returns {object} rgb colors (for example { r: 255, b: 128, g: 77 })
 */
export function hexToRgb (hex) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      }
    : null
}

/**
 * check, if given color is dark (or light)
 * @param {string} color - either rgb (for example 'rgb(100,200,10)') or hex-color (for example '#77ff00')
 * @see https://awik.io/determine-color-bright-dark-using-javascript/
 * @returns {boolean} true when dark color, false else
 */
export function isDarkColor (color) {
  let r, g, b
  if (color.match(/^rgb/)) {
    [r, g, b] = color.match(
      /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
    )
  } else {
    color = +('0x' + color.slice(1).replace(color.length < 5 && /./g, '$&$&'))
    r = color >> 16
    g = (color >> 8) & 255
    b = color & 255
  }
  const test = Math.sqrt(0.3 * r * r + 0.6 * g * g + 0.1 * b * b)
  return test < 128
}

/**
 * Convert decimal number to hex code
 * @param {number} c - decimal number (0...255)
 * @returns {string} hex code with to letters (00 .. ff)
 */
function componentToHex (c) {
  c = Math.floor(c)
  const hex = c.toString(16)
  return hex.length === 1 ? '0' + hex : hex
}
