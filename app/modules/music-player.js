/**
 * Simple music player that can play the tracks stored in the /src/assets/music directory.
 * @author Thomas Kippenberg
 * @module music-player
 */
import index from '../assets/music/index.js'
import { escapeHTML } from '../modules/text.js'

const audios = [] // array to buffer fetched audios
const ctx = new AudioContext() // audio context
let playSound // sound buffer source

/**
 * Play specified music file (completely)
 * @param {string} id - id of music file, see /app/assets/music/index.js
 */
export async function play (id) {
  if (playSound) playSound.stop()
  if (!index.items[id]) {
    console.error(`Unknown music with id ${id}`)
    return
  }
  // get mp3 file if not already loaded
  if (!audios[id]) {
    const data = await fetch(`../assets/music/${index.items[id].file}`)
    const arrayBuffer = await data.arrayBuffer()
    audios[id] = await ctx.decodeAudioData(arrayBuffer)
  }
  // play
  playSound = ctx.createBufferSource()
  playSound.loop = true
  playSound.buffer = audios[id]
  playSound.connect(ctx.destination)
  playSound.start(ctx.currentTime)
}


/**
 * Stop music player
*/
export async function stop () {
  playSound?.stop()
}

/**
 * Get credits for list of music files in HTML syntax
 * @param {string[]} idList - list of song ids, see /src/assets/music/index.js
 * @returns {string} HTML text
 */
export function credits (idList) {
  let result = ''
  for (let i = 0; i < idList.length; i++) {
    const id = idList[i]
    if (index.items[id]) {
      if (i > 0) result += ', '
      result += `<em>${escapeHTML(index.items[id].title)}</em>`
    }
  }
  result += ` by <a data-target="external" target="_blank" href="${escapeHTML(index.url)}">${escapeHTML(index.author)}</a>`
  return result
}

/**
 * Feadeout effect;
 * @see https://developer.apple.com/library/archive/documentation/AudioVideo/Conceptual/Using_HTML5_Audio_Video/Device-SpecificConsiderations/Device-SpecificConsiderations.html
 * @param {number} ms - duration of fadeout in milliseconds
async function fadeout (ms) {
  for (let i = 0; i < 10; i++) {
    await sleep(ms / 10)
    audio.volume = (10 - i) / 10
  }
} */

/**
 * Custom sleep function, internal
 * @param {*} ms - duration in milliseconds
 * @returns {Promise} - resolved after timeout
 
function sleep (ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}*/


