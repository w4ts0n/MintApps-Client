/**
 * Module containing various auxiliary routines for physical simulations
 * @author Thomas Kippenberg
 * @module physics
 */
import * as color from './color.js'
const sizeB = 4 // size of B-field markers in px
const sizeE = 4 // size of E-field markers in px
const minDistance = 25 // minimum distance of markers E- and B-field

/**
 * Convert wavelength to RGB code.
 * Code & algoritm from John D. Cook, many thanks for permission to use and modify it.
 * @param {number} w - wavelength i nm
 * @returns {object} object containing color values {r, g, b}
 * @see https://www.johndcook.com/wavelength_to_RGB.html
 */
export function wavelength2rgb (w) {
  w = Math.round(w * 1e9)
  let red, green, blue, factor
  if (w >= 380 && w < 440) {
    red = -(w - 440) / (440 - 380)
    green = 0.0
    blue = 1.0
  } else if (w >= 440 && w < 490) {
    red = 0.0
    green = (w - 440) / (490 - 440)
    blue = 1.0
  } else if (w >= 490 && w < 510) {
    red = 0.0
    green = 1.0
    blue = -(w - 510) / (510 - 490)
  } else if (w >= 510 && w < 580) {
    red = (w - 510) / (580 - 510)
    green = 1.0
    blue = 0.0
  } else if (w >= 580 && w < 645) {
    red = 1.0
    green = -(w - 645) / (645 - 580)
    blue = 0.0
  } else if (w >= 645 && w < 781) {
    red = 1.0
    green = 0.0
    blue = 0.0
  } else {
    red = 0.0
    green = 0.0
    blue = 0.0
  }
  // Let the intensity fall off near the vision limits
  if (w >= 380 && w < 420) {
    factor = 0.3 + (0.7 * (w - 380)) / (420 - 380)
  } else if (w >= 420 && w < 701) {
    factor = 1.0
  } else if (w >= 701 && w < 781) {
    factor = 0.3 + (0.7 * (780 - w)) / (780 - 700)
  } else {
    factor = 0.0
  }
  const gamma = 0.8
  const R = red > 0 ? 255 * Math.pow(red * factor, gamma) : 0
  const G = green > 0 ? 255 * Math.pow(green * factor, gamma) : 0
  const B = blue > 0 ? 255 * Math.pow(blue * factor, gamma) : 0
  return { r: R, g: G, b: B }
  // return {r:red*255, g:green*255, b:blue*255}
}

/**
 * Visualize a magnetic B field in a certain area using x or o signs
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {number} x - x coordinate in px
 * @param {number} y - y coordinate in px
 * @param {number} width - width in px
 * @param {number} height - height in px
 * @param {number} B - strength in arbitrary unit
 * @param {number} maxB - maximum amount of field strength, used for normalization 
 * @param {boolean} [circular=false] - true = circular region, false = rectangual region
 */
export function drawBfield (ctx, x, y, width, height, B, maxB, circular = false) {
  const colors = color.getColors()
  ctx.save()
  ctx.fillStyle = colors[2] + '20'
  ctx.beginPath()
  if (circular) { ctx.arc(x + 0.5 * width, y + 0.5 * height, width * 0.5, 0, 2 * Math.PI) } else ctx.rect(x, y, width, height)
  ctx.fill()
  ctx.clip()
  ctx.lineWidth = 1
  ctx.strokeStyle = colors[2]
  const step = minDistance * Math.sqrt(Math.abs(maxB / B))
  const stepsX = Math.floor(width / step)
  const stepsY = Math.floor(height / step)
  for (let i = -stepsX; i <= stepsX; i++) {
    const pixelX = x + 0.5 * (width + i * step)
    for (let j = -stepsY; j <= stepsY; j++) {
      const pixelY = y + 0.5 * (height + j * step)
      if (B > 0) {
        ctx.beginPath()
        ctx.moveTo(pixelX - sizeB, pixelY - sizeB)
        ctx.lineTo(pixelX + sizeB, pixelY + sizeB)
        ctx.moveTo(pixelX + sizeB, pixelY - sizeB)
        ctx.lineTo(pixelX - sizeB, pixelY + sizeB)
        ctx.stroke()
      } else {
        ctx.lineWidth = 1
        ctx.beginPath()
        ctx.arc(pixelX, pixelY, 5, 0, 2 * Math.PI)
        ctx.stroke()
        ctx.lineWidth = 2
        ctx.beginPath()
        ctx.arc(pixelX, pixelY, 1, 0, 2 * Math.PI)
        ctx.stroke()
      }
    }
  }
  ctx.restore()
}

/**
 * Visualize an electric E field in a certain area using arrows
 * @param {CanvasRenderingContext2D} ctx - rendering context 
 * @param {number} x - x coordinate in px
 * @param {number} y - y coordinate in px
 * @param {number} width - width in px
 * @param {number} height - height in px
 * @param {number} E - strength in arbitrary unit
 * @param {number} maxE - maximum amount of field strength, used for normalization 
 */
export function drawEfield (ctx, x, y, width, height, E, maxE) {
  const colors = color.getColors()
  ctx.fillStyle = colors[1] + '20'
  ctx.beginPath()
  ctx.rect(x, y, width, height)
  ctx.fill()
  ctx.lineWidth = 1
  ctx.strokeStyle = colors[1]
  ctx.fillStyle = colors[1]
  const step = minDistance * Math.sqrt(Math.abs(maxE / E))
  const steps = Math.floor(width / step)
  for (let i = -steps; i <= steps; i++) {
    const pixelX = x + 0.5 * (width + i * step)
    ctx.beginPath()
    ctx.moveTo(pixelX, y)
    ctx.lineTo(pixelX, y + height)
    ctx.stroke()
    ctx.beginPath()
    if (E > 0) {
      ctx.moveTo(pixelX - sizeE, y + height - sizeE)
      ctx.lineTo(pixelX + sizeE, y + height - sizeE)
      ctx.lineTo(pixelX, y + height)
      ctx.lineTo(pixelX - sizeE, y + height - sizeE)
    } else {
      ctx.moveTo(pixelX - sizeE, y + sizeE)
      ctx.lineTo(pixelX + sizeE, y + sizeE)
      ctx.lineTo(pixelX, y)
      ctx.lineTo(pixelX - sizeE, y + sizeE)
    }
    ctx.fill()
  }
}
