/**
 * List of all developers, advisors, ideators etc. of the MintApps
 * @author Thomas Kippenberg
 * @module contributors
 */

/**
* Object that contains all information about the contributors of the Mintapps
*/
export const CONTRIBUTOR_LIST = {
  tk100: {
    fn: 'Thomas Kippenberg',
    href: 'https://kippenbergs.de'
  },
  mabo: {
    fn: 'Matthias Borchard',
    href: 'https://www.mabo-physik.de'
  },
  stri: {
    fn: 'Stefan Richtberg',
    href: 'https://www.richtberg.org/'
  },
  chsc: {
    fn: 'Christian Schiller',
    href: 'https://codeberg.org/schilchris'
  },
  misp: {
    fn: 'Michael Speckert',
    href: 'https://speckerts.de'
  },
  rode: {
    fn: 'Roland Debl'
  },
  w4ts0n: {
    fn: 'Lukas Schieren',
    href: 'https://lukas-schieren.de'
  },
  bech: {
    fn: 'Benjamin Chalupar'
  },
  mr: {
    fn: 'Michael Rode'
  },
  hgu: {
    fn: 'Hans-Georg Unckell',
    href: 'https://codeberg.org/hgu'
  },
  stbe: {
    fn: 'Stephan Beutlhauser'
  }
}

/**
 * Convert array of contributor-ids to human readable list with commas, "and" and hyperlinks
 * @param {string} list - list of contributor ids, separeted by spaces
 * @returns {string} human readable list in markdown syntax
 */
export function listToReadableList(list = '') {
  const array = list.trim().split(',').map((entry => entry.trim()))
  let result = ''
  for (let i = 0; i + 1 < array.length; i++) {
    result += resolveContributor(array[i])
    if (i + 2 < array.length) result += ', '
  }
  if (array.length > 1) result += ' & '
  result += resolveContributor(array[array.length - 1])
  return result
}

/**
 * Resolve name id and corresponding href
 * @param {string} id - contributor id, for example 'tk100'
 * @returns {string} Full name and, if applicable, internet address as a markdown link
 */
export function resolveContributor(id) {
  const c = CONTRIBUTOR_LIST[id]
  if (c && c.href) return '[' + c.fn + '](' + c.href + ')'
  else if (c) return c.fn
  else return id
}
