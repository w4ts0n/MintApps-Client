# Installation

You are welcome to run the MintApps on your own server. To do this, you have to download this repository on your local system, configure the app and deploy it to your server.

## Prerequisites

You don't need to install any extra software like node.js or npm on your local systems. These tools are only required for development.

## Download sources

Download and unzip the client sources:

~~~
wget https://codeberg.org/MintApps/client/archive/main.zip
unzip main.zip
rm main.zip
~~~

Enter the client/app directory:

~~~
cd client
cd app
~~~

Download and unzip weblate repository:

~~~
cd assets/
wget https://codeberg.org/MintApps/weblate/archive/main.zip
unzip main.zip
rm main.zip
cd ..
~~~

If you'd also like to install the example quiz games (MINT Quiz, Pairs, and The Big Quiz) and other examples, additionally run:

~~~
cd assets/
wget https://codeberg.org/MintApps/data/archive/main.zip
unzip main.zip
rm main.zip
cd ..
~~~

## Configure application

When deploying the app on your own web server, you usually have to adjust the configuration according to the legal requirements in your country. First, copy `config.example.js` to `config.js`:

~~~
cp config.example.js config.js
~~~

Then use your favorite text editor to adjust the values. For more information, see the comments.

## Deploy

Copy all files from the `app/` directory to your web server, a standard webhosting is sufficient.

## Notes

- Some selected apps, such as quiz games, optionally support synchronization between multiple guests and saving your own activities. In order to use these features, an additional server component is required. See [Server](https://codeberg.org/MintApps/server) for more details.
- If you want to create individual, downloadable packages (SCORM) for single apps, you will find detailed information in [../extra/scorm/README.md](https://codeberg.org/MintApps/client/src/branch/main/tools/scorm/README.md).

